# Tutoriel d'utilisation du logiciel "Base de données"

## Installation

### Récupération du programme

Pour récupérer le programme, deux possibilité s'offre à vous :
* Télécharger tous le projet en cliquant [ici](https://developpix.gitlab.io/base-de-donnees/base-de-donnees-all.zip) ou sur le lien "Télécharger ALL" dans le fichier README.md
* Si vous possédez déjà les ressources utilisées par le programme,
 vous pouvez télécharger uniquement le jar en cliquant [ici](https://developpix.gitlab.io/base-de-donnees/base-de-donnees.jar) ou en cliquant sur le lien "Télécharger JAR" dans le fichier README.md

### Installation du programme

#### Dépendances

Pour exécutez ce programme, il est nécessaire que vous ayez installé une version de Java >= 1.8

#### Installation à proprement parler

Si vous avez télécharger tous le programme, vous avez uniquement à extraire le ZIP dans un sous-dossier de votre choix.

|![Image absente](tutos/img/screenshot_apres_extraction.png)|
|:-:|
|*Screenshot après extraction*|

Si vous avez télécharger uniquement le JAR, remplacer le JAR déjà existant par le nouveau.

### Débuter la correction

Lorsque vous lancez l'application vous arriver sur une page de bienvenue.
A la fin de l'exercice vous reviendrez sur cette page.

![Image absente](tutos/img/screenshot_vue_bienvenue.png)|
|:-:|
|*Screenshot de la page de bienvenue*|

Cliquer sur le bouton `Commencer` pour commencer la correction de l'exercice.

#### Saisie de la relation

|![Image absente](tutos/img/screenshot_saisie_relation1.png)|
|:-:|
|*Screenshot de la page de saisie de la relation*|

Tous d'abord saisissons les attributs de la relation. Pour saisir les attributs vous devez entrer la liste des attributs séparés par des virgule et entourés de `<` et `>`.
Les espaces, tabulation et retour à la ligne n'ont pas d'importance dans la saisie, vous pouvez donc en entré n'importe où.
Attention toutefois les attributs composé de plusieurs mots tiendront compte des espaces, tabulations et retour à la ligne saisi entre les mots.

|![Image absente](tutos/img/screenshot_saisie_relation2.png)|
|:-:|
|*Screenshot de la saisie d'un ensemble d'attributs d'une relation*|

Pour saisir les dépendances même principe, les attributs sont séparés par des virgules et les dépendances par des point-virgules. Cependant ici, l'ensemble est entouré d'accolades, `{` et `}`.

|![Image absente](tutos/img/screenshot_saisie_relation3.png)|
|:-:|
|*Screenshot de la saisie des dépendances d'une relation*|

Une fois terminer cliquer sur recherches préliminaires. Vous pourrez revenir à tous moment en arrière, ne vous inquiéter donc pas :stuck_out_tongue_winking_eye:.

### Les recherches préliminaires

La saisie reste la même que pour la saisie des données de la relation initiale. Comme vous pouvez le voir, en haut se situe une barre de progression pour vous présicez où vous en êtes dans la correction, ainsi que la relation sur laquelle vous travaillé.
Attention, toutefois ici tous les ensemble (attributs et dépendances) sont entourés d'accolades.
Pour vérifier votre saisie cliquer sur le bouton `vérifier`. Si vous voulez la réponse directement, cliquer dessus sans rien saisir.
**It's magic !!! :joy:**

|![Image absente](tutos/img/screenshot_recherches1.png)|
|:-:|
|*Screenshot de la page des recherches préliminaires*|

Pour la saisie de la clé, vous avez deux listes :

* Celle du haut est la liste des attributs de la relation, cliquer sur les attributs pour les rajouter à la clé
* Celle du bas contient la liste des attributs de la clé, cliquer sur un attribut pour le retire de la clé

|![Image absente](tutos/img/screenshot_recherches2.png)|
|:-:|
|*Screenshot de la saisie de la clé*|

Une fois terminer cliquer sur le bouton `Algorithmes` pour passer à la suite.

### Les algorithmes

Pour la décomposition en suivant l'algorithme de décomposition, vous devez commencé par sélectionné une dépendance dans la liste déroulante en haut.
Ensuite à droite saisissez la relation R1 contenant la dépendance sélectionné précédemment.
A droite saisissez la deuxième relation R2.
Une fois terminer cliquer sur vérifier, il vérifiera que vous avez bien décomposé puis il ajoutera les relations dans la liste des relations représentant le résultat.

|![Image absente](tutos/img/screenshot_decompo1.png)|
|:-:|
|*Screenshot de la décomposition*|

Si les relations saisie sont correct il ajoutera la/les vôtre(s) sinon il ajoutera la/les relation(s) correct.

|![Image absente](tutos/img/screenshot_decompo2.png)|
|:-:|
|*Screenshot d'une décomposition incorrect*|

Lorsqu'il n'est plus possible de décomposé les zones de saisies se grisent vous ne pourrez donc plus continuer.

Passons donc à la décomposition en suivant l'algorithme de synthèse.

Pour l'algorithme de synthèse, il vous suffit de rentrer un nom à la relation à rajouter dans la liste, ses attributs et son ensemble de dépendances.
Pour modifier une relation saisie, il suffit seulement de rentrer le même nom.
Pour supprimer une relation cliquer sur la relation à supprimer dans la relation puis cliquer sur le bouton supprimer.

|![Image absente](tutos/img/screenshot_synthese1.png)|
|:-:|
|*Screenshot de l'algorithme de synthèse*|

Pour vérifier vôtre décomposition suivant l'algorithme de synthèse cliquer sur le bouton vérifier à côté.

Vous avez terminé, vous pouvez cliquer sur le bouton `Ullman` pour passer à la partie utilisant le théorème d'Ullman.

### Ullman

|![Image absente](tutos/img/screenshot_ullman1.png)|
|:-:|
|*Screenshot de la vue d'Ullman*|

Commençons par saisir les informations de la relation initiale. Saisissez les attributs à droites et les dépendances à gauche, comme sur l'image ci-dessous.

|![Image absente](tutos/img/screenshot_ullman2.png)|
|:-:|
|*Screenshot Ullman : Saisie de la relation initiale *|

Saisissons ensuite les informations de la première relation (R1) puis de la deuxième relation (R) qui remplaceront la relation initiale.

|![Image absente](tutos/img/screenshot_ullman3.png)|![Image absente](tutos/img/screenshot_ullman4.png)|
|:-:|:-:|
|*Screenshot saisie de R1*|*Screenshot saisie de R2*|

Ensuite sélectionné en dessous si vous pensez qu'il y a des pertes de données puis cliquer sur vérifier à gauche (ou cliquer directement sur vérifier pour avoir la bonne réponse). Faites de même pour la vérification de pertes de dépendances.

|![Image absente](tutos/img/screenshot_ullman5.png)|![Image absente](tutos/img/screenshot_ullman6.png)|
|:-:|:-:|
|*Screenshot vérification pertes de données*|*Screenshot vérification pertes de dépendances*|

### Vous avez terminé

Vous avez terminé, à présent vous pouvez cliquer sur le bouton `Accueil`, cela vous ramenera à la page de bienvenue depuis laquelle vous pourrez recommencer avec la même relation où une nouvelle.