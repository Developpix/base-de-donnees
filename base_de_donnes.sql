-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost
-- Généré le :  jeu. 14 fév. 2019 à 16:12
-- Version du serveur :  10.3.12-MariaDB
-- Version de PHP :  7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `base_de_donnees`
--

-- --------------------------------------------------------

--
-- Structure de la table `comptes`
--

CREATE TABLE `comptes` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `comptes`
--

INSERT INTO `comptes` (`id`, `username`, `password`) VALUES
(1, 'a', '1f40fc92da241694750979ee6cf582f2d5d7d28e18335de05abc54d0560e0f5302860c652bf08d560252aa5e74210546f369fbbbce8c12cfc7957b2652fe9a75');

-- --------------------------------------------------------

--
-- Structure de la table `exercices`
--

CREATE TABLE `exercices` (
  `id` int(11) NOT NULL,
  `relation` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `tuples` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `exercices`
--

INSERT INTO `exercices` (`id`, `relation`, `tuples`) VALUES
(1, '{\"graphe\":{\"listeAdjacence\":[[[{\"nom\":\"a\"}],[[{\"nom\":\"b\"}]]],[[{\"nom\":\"b\"}],[[{\"nom\":\"c\"}]]]],\"ensembleAttributs\":[{\"nom\":\"a\"},{\"nom\":\"b\"},{\"nom\":\"c\"}]},\"attributs\":[{\"nom\":\"a\"},{\"nom\":\"b\"},{\"nom\":\"c\"}]}', ''),
(2, '{\"graphe\":{\"listeAdjacence\":[[[{\"nom\":\"a\"}],[[{\"nom\":\"b\"}]]],[[{\"nom\":\"a\"},{\"nom\":\"b\"}],[[{\"nom\":\"c\"}]]],[[{\"nom\":\"a\"},{\"nom\":\"b\"},{\"nom\":\"c\"}],[[{\"nom\":\"a\"},{\"nom\":\"c\"},{\"nom\":\"d\"}]]]],\"ensembleAttributs\":[{\"nom\":\"a\"},{\"nom\":\"b\"},{\"nom\":\"c\"},{\"nom\":\"d\"}]},\"attributs\":[{\"nom\":\"a\"},{\"nom\":\"b\"},{\"nom\":\"c\"},{\"nom\":\"d\"}]}', ''),
(3, '{\"graphe\":{\"listeAdjacence\":{},\"ensembleAttributs\":[{\"nom\":\"a\"},{\"nom\":\"b\"},{\"nom\":\"c\"}]},\"attributs\":[{\"nom\":\"a\"},{\"nom\":\"b\"},{\"nom\":\"c\"}]}', ''),
(4, '{\"graphe\":{\"listeAdjacence\":{},\"ensembleAttributs\":[{\"nom\":\"a\"},{\"nom\":\"b\"},{\"nom\":\"c\"}]},\"attributs\":[{\"nom\":\"a\"},{\"nom\":\"b\"},{\"nom\":\"c\"}]}', ''),
(5, '{\"graphe\":{\"listeAdjacence\":{},\"ensembleAttributs\":[{\"nom\":\"a\"},{\"nom\":\"b\"},{\"nom\":\"c\"}]},\"attributs\":[{\"nom\":\"a\"},{\"nom\":\"b\"},{\"nom\":\"c\"}]}', ''),
(6, '{\"graphe\":{\"listeAdjacence\":{},\"ensembleAttributs\":[{\"nom\":\"a\"},{\"nom\":\"b\"},{\"nom\":\"c\"}]},\"attributs\":[{\"nom\":\"a\"},{\"nom\":\"b\"},{\"nom\":\"c\"}]}', ''),
(7, '{\"graphe\":{\"listeAdjacence\":[[[{\"nom\":\"t\"}],[[{\"nom\":\"H\"},{\"nom\":\"b\"},{\"nom\":\"x\"}]]],[[{\"nom\":\"P\"},{\"nom\":\"u\"}],[[{\"nom\":\"H\"},{\"nom\":\"P\"}]]],[[{\"nom\":\"H\"}],[[{\"nom\":\"Q\"}]]],[[{\"nom\":\"C\"},{\"nom\":\"w\"}],[[{\"nom\":\"N\"},{\"nom\":\"P\"},{\"nom\":\"t\"},{\"nom\":\"x\"}]]],[[{\"nom\":\"H\"},{\"nom\":\"b\"}],[[{\"nom\":\"b\"}]]],[[{\"nom\":\"C\"},{\"nom\":\"x\"}],[[{\"nom\":\"w\"}],[{\"nom\":\"t\"},{\"nom\":\"w\"}]]],[[{\"nom\":\"C\"},{\"nom\":\"H\"}],[[{\"nom\":\"x\"}]]],[[{\"nom\":\"P\"},{\"nom\":\"u\"},{\"nom\":\"x\"}],[[{\"nom\":\"H\"},{\"nom\":\"x\"}]]]],\"ensembleAttributs\":[{\"nom\":\"C\"},{\"nom\":\"H\"},{\"nom\":\"N\"},{\"nom\":\"P\"},{\"nom\":\"Q\"},{\"nom\":\"b\"},{\"nom\":\"t\"},{\"nom\":\"u\"},{\"nom\":\"w\"},{\"nom\":\"x\"}]},\"attributs\":[{\"nom\":\"C\"},{\"nom\":\"H\"},{\"nom\":\"N\"},{\"nom\":\"P\"},{\"nom\":\"Q\"},{\"nom\":\"b\"},{\"nom\":\"t\"},{\"nom\":\"u\"},{\"nom\":\"w\"},{\"nom\":\"x\"}]}', ''),
(8, '{\"graphe\":{\"listeAdjacence\":{},\"ensembleAttributs\":[{\"nom\":\"F\"}]},\"attributs\":[{\"nom\":\"F\"}]}', ''),
(9, '{\"graphe\":{\"listeAdjacence\":{},\"ensembleAttributs\":[{\"nom\":\"F\"}]},\"attributs\":[{\"nom\":\"F\"}]}', ''),
(10, '{\"graphe\":{\"listeAdjacence\":{},\"ensembleAttributs\":[{\"nom\":\"F\"}]},\"attributs\":[{\"nom\":\"F\"}]}', ''),
(11, '{\"graphe\":{\"listeAdjacence\":{},\"ensembleAttributs\":[{\"nom\":\"F\"}]},\"attributs\":[{\"nom\":\"F\"}]}', ''),
(12, '{\"graphe\":{\"listeAdjacence\":[[[{\"nom\":\"q\"}],[[{\"nom\":\"B\"}]]],[[{\"nom\":\"A\"}],[[{\"nom\":\"B\"}]]],[[{\"nom\":\"B\"}],[[{\"nom\":\"A\"},{\"nom\":\"W\"},{\"nom\":\"q\"}]]],[[{\"nom\":\"B\"},{\"nom\":\"q\"}],[[{\"nom\":\"O\"}]]],[[{\"nom\":\"B\"},{\"nom\":\"c\"}],[[{\"nom\":\"W\"},{\"nom\":\"q\"}]]]],\"ensembleAttributs\":[{\"nom\":\"A\"},{\"nom\":\"B\"},{\"nom\":\"O\"},{\"nom\":\"W\"},{\"nom\":\"c\"},{\"nom\":\"q\"}]},\"attributs\":[{\"nom\":\"A\"},{\"nom\":\"B\"},{\"nom\":\"O\"},{\"nom\":\"W\"},{\"nom\":\"c\"},{\"nom\":\"q\"}]}', '');

-- --------------------------------------------------------

--
-- Structure de la table `travail`
--

CREATE TABLE `travail` (
  `id_exercice` int(11) NOT NULL,
  `id_compte` int(11) NOT NULL,
  `relation` int(11) NOT NULL,
  `couvMin` int(11) NOT NULL,
  `fermTran` int(11) NOT NULL,
  `fermEns` int(11) NOT NULL,
  `cle` int(11) NOT NULL,
  `formeNormale` int(11) NOT NULL,
  `decomposition` int(11) NOT NULL,
  `synthese` int(11) NOT NULL,
  `perteDonnees` int(11) NOT NULL,
  `perteDependances` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `travail`
--

INSERT INTO `travail` (`id_exercice`, `id_compte`, `relation`, `couvMin`, `fermTran`, `fermEns`, `cle`, `formeNormale`, `decomposition`, `synthese`, `perteDonnees`, `perteDependances`) VALUES
(4, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `comptes`
--
ALTER TABLE `comptes`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `exercices`
--
ALTER TABLE `exercices`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `travail`
--
ALTER TABLE `travail`
  ADD KEY `FK_Compte` (`id_compte`),
  ADD KEY `FK_Exercice` (`id_exercice`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `comptes`
--
ALTER TABLE `comptes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `exercices`
--
ALTER TABLE `exercices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `travail`
--
ALTER TABLE `travail`
  ADD CONSTRAINT `FK_Compte` FOREIGN KEY (`id_compte`) REFERENCES `comptes` (`id`),
  ADD CONSTRAINT `FK_Exercice` FOREIGN KEY (`id_exercice`) REFERENCES `exercices` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
