# Projet "Base de données" version 2.1.0

[![pipeline status](https://gitlab.com/Developpix/base-de-donnees/badges/master/pipeline.svg)](https://gitlab.com/Developpix/base-de-donnees/commits/master)

[![build status](https://developpix.gitlab.io/base-de-donnees/build.svg)](https://gitlab.com/Developpix/base-de-donnees/commits/master)
[![tests attribut status](https://developpix.gitlab.io/base-de-donnees/testsAttribut.svg)](https://gitlab.com/Developpix/base-de-donnees/commits/master)
[![tests graphe status](https://developpix.gitlab.io/base-de-donnees/testsGraphe.svg)](https://gitlab.com/Developpix/base-de-donnees/commits/master)
[![tests relation status](https://developpix.gitlab.io/base-de-donnees/testsRelation.svg)](https://gitlab.com/Developpix/base-de-donnees/commits/master)
[![tests table status](https://developpix.gitlab.io/base-de-donnees/testsTable.svg)](https://gitlab.com/Developpix/base-de-donnees/commits/master)
[![tests compte status](https://developpix.gitlab.io/base-de-donnees/testsCompte.svg)](https://gitlab.com/Developpix/base-de-donnees/commits/master)
[![tests chargeur config status](https://developpix.gitlab.io/base-de-donnees/testsConfigLoader.svg)](https://gitlab.com/Developpix/base-de-donnees/commits/master)
[![tests générateur exos status](https://developpix.gitlab.io/base-de-donnees/testsGenerateurExos.svg)](https://gitlab.com/Developpix/base-de-donnees/commits/master)

## Présentation
Le projet consiste à réaliser une application avec une interface graphique qui demande à l’utilisateur de réaliser des tâches sur les relations, pour une base de donnée relationnelle, et qui vérifie les données qu’il a saisie, ou bien qui lui donne les données, si il ne sait pas.

Si vous avez besoin de la documentation -> [Doc](https://developpix.gitlab.io/base-de-donnees/apidocs/index.html)

* Télécharger [JAR](https://developpix.gitlab.io/base-de-donnees/base-de-donnees.jar)
* Télécharger [ALL](https://developpix.gitlab.io/base-de-donnees/base-de-donnees-all.zip)

Un tutoriel est disponible [ici](https://gitlab.com/Developpix/base-de-donnees/blob/master/tutos/index.md), nous vous conseillons de le lire avant de commencer à utiliser l'application.

## Utiliser Git

### En ligne de commande

```bash
    git status          # Status du dépôt, voir si des modifications sont
                        # répertoriées (indexées) et/ou si elles doivent
                        # être commit

    git add . ou --all  # Prend en compte (index tous) tous pour le
                        # prochain commit

            nom_fichier # Index le fichier "nom_fichier" pour qu'il soit pris
                        # en compte lors du prochain commit

    git commit -s -m "message" # Commit avec le message "message" sans l'option
                            # -m il ouvrir un editeur de texte pour saisir
                            # le message

               -a           # Commit toutes les modifs sauf les fichiers
                            # rajoutées (créer non indexer) qui doivent d'abord
                            # être indexés

    git pull            # Télécharge les modifs présent sur le serveur
                        # Si il y a des modifications

    git push            # Envoi les modifs sur le serveur

    git log             # Affiche tous les commits (historique)
            --oneline   # Chaque commit est affiché en une seule ligne

    git branch          # Voir la liste des branches disponible

    git checkout nom_branch # Passer sur la branche "nom_branch"
```
[Toutes les commandes sur cette page](https://github.com/joshnh/Git-Commands/blob/master/README.md)

### Git en GUI

#### GitG
[Lien vers le site](https://wiki.gnome.org/action/show/Apps/Gitg)

#### GitKraken
[Lien vers le site](https://www.gitkraken.com)

Nécessite d'avoir un compte GitHub pour l'utiliser gratuitement.

#### Autres
Regarder sur le [site](https://git-scm.com/downloads/guis/)

### Le fichier .gitignore
Mettez dans ce fichier tous les fichiers à exclure des commits.
Tous ce qui est après un **#** est un commentaire. Les regex
peuvent être utiliser regarder, pour la syntaxe du fichier
[regarder ici](https://git-scm.com/docs/gitignore).

### Git flow, une extension sympa de Git
Permet de travailler avec un système de branche avec :
* Une branche principale **master**
* Une branche de développement **develop**
* Une pour les versions **release**
* Des branches pour les modifications
    * **feature** pour travailler sur des nouveautés
    * **hotfix** pour corriger des bugs
