package ihm.modele.utilisateur;

import static org.junit.Assert.assertEquals;

import java.util.Set;

import org.junit.Test;

import ihm.modele.bd.Attribut;
import ihm.modele.bd.Relation;
import ihm.modele.bd.Table;

public class GenerateurExerciceTest {

	@Test(expected = IllegalArgumentException.class)
	public void testGenererRelationCT1() throws IllegalArgumentException {
		GenerateurExercice.genererRelation(0, 10);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testGenererRelationCT2() throws IllegalArgumentException {
		GenerateurExercice.genererRelation(51, 10);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testGenererRelationCT3() throws IllegalArgumentException {
		GenerateurExercice.genererRelation(1, 1);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testGenererRelationCT4() throws IllegalArgumentException {
		GenerateurExercice.genererRelation(10, -1);
	}

	@Test
	public void testGenererRelationCT5() throws IllegalArgumentException {
		Relation r = GenerateurExercice.genererRelation(1, 0);

		assertEquals("Vérification 1 attribut généré", 1, r.getAttributs().size());

		int nbDep = 0;
		for (Set<Attribut> sg : r.getGraphe().donneCles()) {
			nbDep += r.getGraphe().successeurs(sg).size();
		}
		assertEquals("Vérification une dépendance créée", 0, nbDep);
	}

	@Test
	public void testGenererRelationCT6() throws IllegalArgumentException {
		Relation r = GenerateurExercice.genererRelation(50, 10);

		assertEquals("Vérification 50 attributs générés", 50, r.getAttributs().size());

		int nbDep = 0;
		for (Set<Attribut> sg : r.getGraphe().donneCles()) {
			nbDep += r.getGraphe().successeurs(sg).size();
		}
		assertEquals("Vérification 10 dépendance créée", 10, nbDep);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testGenererTuplesCT1() throws IllegalArgumentException {
		GenerateurExercice.genererTuples(0, 10);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testGenererTuplesCT2() throws IllegalArgumentException {
		GenerateurExercice.genererTuples(1, -10);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testGenererTuplesCT3() throws IllegalArgumentException {
		GenerateurExercice.genererTuples(1, -1);
	}

	@Test
	public void testGenererTuplesCT4() throws IllegalArgumentException {
		Table tuples = GenerateurExercice.genererTuples(42, 0);

		assertEquals("Vérification 5 attributs dans la table", 42, tuples.getAttributs().size());

		assertEquals("Vérification 0 tuples dans la table", 0, tuples.nombreTuples());
	}

	@Test
	public void testGenererTuplesCT5() throws IllegalArgumentException {
		Table tuples = GenerateurExercice.genererTuples(5, 10);

		assertEquals("Vérification 5 attributs dans la table", 5, tuples.getAttributs().size());

		assertEquals("Vérification 10 tuples dans la table", 10, tuples.nombreTuples());
	}

	@Test
	public void testNombreDependancesPossiblesCT1() {
		assertEquals("Test 1 attributs donne 0 dépendances possible", 0,
				GenerateurExercice.nombreDependancesPossibles(1));
	}

	@Test
	public void testNombreDependancesPossiblesCT2() {
		assertEquals("Test 2 attributs donne 2 dépendances possible", 2,
				GenerateurExercice.nombreDependancesPossibles(2));
	}

	@Test
	public void testNombreDependancesPossiblesCT3() {
		assertEquals("Test 15 attributs donne 14 283 372 dépendances possible", 14283372, GenerateurExercice.nombreDependancesPossibles(15));
	}

	@Test
	public void testNombreDependancesPossiblesCT4() {
		assertEquals("Test 16 attributs donne 42 915 650 dépendances possible", 42915650,
				GenerateurExercice.nombreDependancesPossibles(16));
	}
}
