package ihm.modele.utilisateur;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Map;

import org.junit.Test;

import com.google.gson.JsonParseException;

import ihm.modele.database.configuration.TableConfiguration;

public class ConfigurationLoaderTest {

	@Test(expected = JsonParseException.class)
	public void testChargerSessionDatabaseConfigurationCT1() throws JsonParseException, IOException, SQLException {
		ConfigurationLoader
				.chargerSessionDatabaseConfiguration("src/test/java/ihm/modele/utilisateur/conf_invalide_port.json");
	}

	@Test(expected = IOException.class)
	public void testChargerSessionDatabaseConfigurationCT2() throws JsonParseException, IOException, SQLException {
		ConfigurationLoader
				.chargerSessionDatabaseConfiguration("src/test/java/ihm/modele/utilisateur/conf_inexistant.json");
	}

	@Test(expected = JsonParseException.class)
	public void testChargerConfigTablesCT1() throws JsonParseException, IOException {
		ConfigurationLoader.chargerConfigTables("src/test/java/ihm/modele/utilisateur/conf_invalide_json.json");
	}

	@Test(expected = IOException.class)
	public void testChargerConfigTablesCT2() throws JsonParseException, IOException {
		ConfigurationLoader.chargerConfigTables("src/test/java/ihm/modele/utilisateur/conf_inexistant.json");
	}

	@Test(expected = JsonParseException.class)
	public void testChargerConfigTablesCT3() throws JsonParseException, IOException {
		ConfigurationLoader
				.chargerConfigTables("src/test/java/ihm/modele/utilisateur/conf_invalide_travail_manquant.json");
	}

	@Test(expected = JsonParseException.class)
	public void testChargerConfigTablesCT4() throws JsonParseException, IOException {
		ConfigurationLoader
				.chargerConfigTables("src/test/java/ihm/modele/utilisateur/conf_invalide_relation_exos_manquant.json");
	}

	@Test
	public void testChargerConfigTablesCT5() throws JsonParseException, IOException {
		Map<String, TableConfiguration> confTables = ConfigurationLoader
				.chargerConfigTables("src/test/java/ihm/modele/utilisateur/conf_valide.json");

		// Vérification de la configuration de la table Comptes
		TableConfiguration confComptes = confTables.get("Comptes");
		assertEquals("Test nom de la table 'Comptes'", "comptes", confComptes.getTable());
		assertEquals("Test id de la table 'Comptes'", "id", confComptes.getAttribut("id"));
		assertEquals("Test identifiant de la table 'Comptes'", "username", confComptes.getAttribut("identifiant"));
		assertEquals("Test motDePasse de la table 'Comptes'", "password", confComptes.getAttribut("motDePasse"));

		// Vérification de la configuration de la table Travail
		TableConfiguration confTravail = confTables.get("Travail");
		assertEquals("Test nom de la table 'Travail'", "travail", confTravail.getTable());
		assertEquals("Test id de la table 'Travail'", "id", confTravail.getAttribut("id"));
		assertEquals("Test id_compte de la table 'Travail'", "id_compte", confTravail.getAttribut("id_compte"));
		assertEquals("Test id_exercice de la table 'Travail'", "id_exercice", confTravail.getAttribut("id_exercice"));
		assertEquals("Test relationSaisie de la table 'Travail'", "relationSaisie", confTravail.getAttribut("relationSaisie"));
		assertEquals("Test relation de la table 'Travail'", "relation", confTravail.getAttribut("relation"));
		assertEquals("Test couvMin de la table 'Travail'", "couvMin", confTravail.getAttribut("couvMin"));
		assertEquals("Test fermTran de la table 'Travail'", "fermTran", confTravail.getAttribut("fermTran"));
		assertEquals("Test fermEns de la table 'Travail'", "fermEns", confTravail.getAttribut("fermEns"));
		assertEquals("Test cle de la table 'Travail'", "cle", confTravail.getAttribut("cle"));
		assertEquals("Test formeNormale de la table 'Travail'", "formeNormale", confTravail.getAttribut("formeNormale"));
		assertEquals("Test decomposition de la table 'Travail'", "decomposition", confTravail.getAttribut("decomposition"));
		assertEquals("Test synthese de la table 'Travail'", "synthese", confTravail.getAttribut("synthese"));
		assertEquals("Test perteDonnees de la table 'Travail'", "perteDonnees", confTravail.getAttribut("perteDonnees"));
		assertEquals("Test perteDependances de la table 'Travail'", "perteDependances", confTravail.getAttribut("perteDependances"));

		// Vérification de la configuration de la table Exercices
		TableConfiguration confExercices = confTables.get("Exercices");
		assertEquals("Test nom de la table 'Exercices'", "exercices", confExercices.getTable());
		assertEquals("Test id de la table 'Exercices'", "id", confExercices.getAttribut("id"));
		assertEquals("Test relation de la table 'Exercices'", "relation", confExercices.getAttribut("relation"));
		assertEquals("Test tuples de la table 'Exercices'", "tuples", confExercices.getAttribut("tuples"));
	}

}
