package ihm.modele.utilisateur;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import org.junit.Before;
import org.junit.Test;

public class CompteTest {

	private Compte compte;

	@Before
	public void setUp() {
		this.compte = new Compte(1, "login", "passwd");
	}

	@Test
	public void testCompteCT1() {
		assertEquals("CT1 constructeur test id", 1, this.compte.getID());
		assertEquals("CT1 constructeur test identifiant", "login", this.compte.getIdentifiant());
		assertNotEquals("CT1 constructeur test mot de passe", "passwd", this.compte.getMotDePasse());
	}

	@Test(expected = IllegalArgumentException.class)
	public void testCompteCT2() {
		new Compte(-1, "login", "passwd");
	}

	@Test(expected = IllegalArgumentException.class)
	public void testCompteCT3() {
		new Compte(Integer.MIN_VALUE, "login", "passwd");
	}

	@Test(expected = IllegalArgumentException.class)
	public void testCompteCT4() {
		new Compte(1, null, "passwd");
	}

	@Test(expected = IllegalArgumentException.class)
	public void testCompteCT5() {
		new Compte(1, "", "passwd");
	}

	@Test(expected = IllegalArgumentException.class)
	public void testCompteCT6() {
		new Compte(1, "login", null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testCompteCT7() {
		new Compte(1, "login", "");
	}

	@Test(expected = IllegalArgumentException.class)
	public void testCompteCT8() {
		this.compte.setIdentifiant(null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testCompteCT9() {
		this.compte.setIdentifiant("");
	}

	@Test
	public void testCompteCT10() {
		this.compte.setIdentifiant("login2");
		assertEquals("CT2 changement identifiant test id", 1, this.compte.getID());
		assertEquals("CT2 changement identifiant test identifiant", "login2", this.compte.getIdentifiant());
		assertNotEquals("CT2 changement identifiant test mot de passe", "passwd", this.compte.getMotDePasse());
	}

	@Test(expected = IllegalArgumentException.class)
	public void testCompteCT11() {
		this.compte.setMotDePasse(null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testCompteCT12() {
		this.compte.setMotDePasse("");
	}

	@Test
	public void testCompteCT13() {
		this.compte.setMotDePasse("aaaaaaaa");
		assertEquals("CT2 changement mot de passe test id", 1, this.compte.getID());
		assertEquals("CT2 changement mot de passe test identifiant", "login", this.compte.getIdentifiant());
		assertNotEquals("CT2 changement mot de passe test mot de passe", "passwd", this.compte.getMotDePasse());
		assertNotEquals("CT2 changement mot de passe test mot de passe", "aaaaaaaa", this.compte.getMotDePasse());
	}
}
