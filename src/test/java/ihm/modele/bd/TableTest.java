package ihm.modele.bd;

import static org.junit.Assert.assertEquals;

import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import org.junit.Test;

import ihm.modele.exceptions.AttributNonPresentException;
import ihm.modele.exceptions.EnsembleAttributsVideException;
import ihm.modele.exceptions.NomVideException;

public class TableTest {

	@Test
	public void testGetRelationCT1() throws NomVideException {
		Set<Attribut> atts = new TreeSet<>();
		atts.add(new Attribut("Q"));
		atts.add(new Attribut("T"));
		
		Table t = new Table(atts);
		
		Map<Attribut, String> map = new TreeMap<>();
		map.put(new Attribut("Q"), "Q3");
		map.put(new Attribut("T"), "T2");
		t.ajouterTuple(map);
		
		map.clear();
		map.put(new Attribut("Q"), "Q4");
		map.put(new Attribut("T"), "T4");
		t.ajouterTuple(map);
		
		map.clear();
		map.put(new Attribut("Q"), "Q2");
		map.put(new Attribut("T"), "T2");
		t.ajouterTuple(map);
		
		map.clear();
		map.put(new Attribut("Q"), "Q1");
		map.put(new Attribut("T"), "T2");
		t.ajouterTuple(map);
		
		map.clear();
		map.put(new Attribut("Q"), "Q3");
		map.put(new Attribut("T"), "T4");
		t.ajouterTuple(map);
		
		Relation r = new Relation(new Graphe(atts), atts);
		
		assertEquals("Test table avec 2 attributs et 5 tuples donnant aucune dépendances", r, t.getRelation());
	}

	@Test
	public void testGetRelationCT2() throws NomVideException, IllegalArgumentException, AttributNonPresentException, EnsembleAttributsVideException {
		Set<Attribut> atts = new TreeSet<>();
		atts.add(new Attribut("q"));
		atts.add(new Attribut("e"));
		
		Table t = new Table(atts);
		
		Map<Attribut, String> map = new TreeMap<>();
		map.put(new Attribut("q"), "q2");
		map.put(new Attribut("e"), "e2");
		t.ajouterTuple(map);
		
		map.clear();
		map.put(new Attribut("q"), "q2");
		map.put(new Attribut("e"), "e2");
		t.ajouterTuple(map);
		
		map.clear();
		map.put(new Attribut("q"), "q2");
		map.put(new Attribut("e"), "e1");
		t.ajouterTuple(map);
		
		Graphe g = new Graphe(atts);
		Set<Attribut> dep1 = new TreeSet<>();
		dep1.add(new Attribut("e"));
		Set<Attribut> dep2 = new TreeSet<>();
		dep2.add(new Attribut("q"));
		g.ajouteArc(dep1, dep2);
		
		Relation r = new Relation(g, atts);
		
		assertEquals("Test table avec 2 attributs et 3 tuples donnant 1 dépendance", r, t.getRelation());
	}

	@Test
	public void testGetRelationCT3() throws NomVideException, IllegalArgumentException, AttributNonPresentException, EnsembleAttributsVideException {
		Set<Attribut> atts = new TreeSet<>();
		atts.add(new Attribut("t"));
		atts.add(new Attribut("z"));
		atts.add(new Attribut("l"));
		
		Table t = new Table(atts);
		
		Map<Attribut, String> map = new TreeMap<>();
		map.put(new Attribut("t"), "t4");
		map.put(new Attribut("z"), "z3");
		map.put(new Attribut("l"), "l4");
		t.ajouterTuple(map);
		
		map.clear();
		map.put(new Attribut("t"), "t1");
		map.put(new Attribut("z"), "z2");
		map.put(new Attribut("l"), "l1");
		t.ajouterTuple(map);
		
		map.clear();
		map.put(new Attribut("t"), "t4");
		map.put(new Attribut("z"), "z4");
		map.put(new Attribut("l"), "l1");
		t.ajouterTuple(map);
		
		map.clear();
		map.put(new Attribut("t"), "t2");
		map.put(new Attribut("z"), "z1");
		map.put(new Attribut("l"), "l1");
		t.ajouterTuple(map);
		
		map.clear();
		map.put(new Attribut("t"), "t4");
		map.put(new Attribut("z"), "z1");
		map.put(new Attribut("l"), "l2");
		t.ajouterTuple(map);
		
		Graphe g = new Graphe(atts);
		Set<Attribut> dep1 = new TreeSet<>();
		dep1.add(new Attribut("t"));
		dep1.add(new Attribut("z"));
		Set<Attribut> dep2 = new TreeSet<>();
		dep2.add(new Attribut("l"));
		g.ajouteArc(dep1, dep2);
		
		dep1.clear();
		dep1.add(new Attribut("t"));
		dep1.add(new Attribut("l"));
		dep2.clear();
		dep2.add(new Attribut("z"));
		g.ajouteArc(dep1, dep2);
		
		dep1.clear();
		dep1.add(new Attribut("z"));
		dep1.add(new Attribut("l"));
		dep2.clear();
		dep2.add(new Attribut("t"));
		g.ajouteArc(dep1, dep2);
		
		Relation r = new Relation(g, atts);
		
		assertEquals("Test table avec 2 attributs et 3 tuples donnant 1 dépendance", r, t.getRelation());
	}
}
