package ihm.modele.bd;

import static org.junit.Assert.assertTrue;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.junit.Before;
import org.junit.Test;

import ihm.modele.exceptions.AttributNonPresentException;
import ihm.modele.exceptions.EnsembleAttributsVideException;
import ihm.modele.exceptions.EnsembleDependancesInvalideException;
import ihm.modele.exceptions.NomVideException;
import ihm.modele.utilisateur.Parseur;

public class RelationTest {

	private Relation r1;
	private Graphe graphe, grapheDep1;
	private Set<Attribut> ensembleInitial;
	private Attribut att1, att2, att3, att4, att5;

	@Before
	public void setUp() {
		this.ensembleInitial = new HashSet<>();

		// Remplissage de l'ensemble initial
		try {
			this.att1 = new Attribut("Attribut 1");
			this.att2 = new Attribut("Attribut 2");
			this.att3 = new Attribut("Attribut 3");
			this.att4 = new Attribut("Attribut 4");
			this.att5 = new Attribut("Attribut 5");

			this.ensembleInitial.add(this.att1);
			this.ensembleInitial.add(this.att2);
			this.ensembleInitial.add(this.att3);
			this.ensembleInitial.add(this.att4);
			this.ensembleInitial.add(this.att5);

			this.graphe = new Graphe(this.ensembleInitial);
			this.grapheDep1 = new Graphe(this.graphe);

			Set<Attribut> dep1 = new HashSet<>();
			dep1.add(this.att1);
			dep1.add(this.att2);
			Set<Attribut> dep2 = new HashSet<>();
			dep2.add(this.att3);
			dep2.add(this.att4);
			this.grapheDep1.ajouteArc(dep1, dep2);

			dep1 = new HashSet<>();
			dep1.add(this.att1);
			dep2 = new HashSet<>();
			dep2.add(this.att4);
			dep2.add(this.att5);
			this.grapheDep1.ajouteArc(dep1, dep2);

			dep1 = new HashSet<>();
			dep1.add(this.att1);
			dep1.add(this.att2);
			dep2 = new HashSet<>();
			dep2.add(this.att5);
			this.grapheDep1.ajouteArc(dep1, dep2);

			dep1 = new HashSet<>();
			dep1.add(this.att5);
			dep2 = new HashSet<>();
			dep2.add(this.att4);
			dep2.add(this.att2);
			this.grapheDep1.ajouteArc(dep1, dep2);

			dep1 = new HashSet<>();
			dep1.add(this.att2);
			dep2 = new HashSet<>();
			dep2.add(this.att5);
			this.grapheDep1.ajouteArc(dep1, dep2);
			
			this.r1 = new Relation(this.grapheDep1, this.ensembleInitial);
		} catch (Exception e) {
			System.out.println("Erreur création des données de test");
			e.printStackTrace();
		}

	}

	/*
	 * Tests sur l'algorithme de synthèse
	 */
	@Test
	public void testAlgorithmeSynthèse1()
			throws IllegalArgumentException, EnsembleAttributsVideException, AttributNonPresentException {

		List<Relation> resultats = this.r1.algorithmeSynthese(this.r1.rechercheCle());

		assertTrue("Test algo synthèse 1 : vérification 4 relations", resultats.size() == 4);

		Set<Attribut> att1 = new HashSet<>();
		att1.add(this.att1);
		att1.add(this.att2);
		att1.add(this.att3);
		Graphe g1 = new Graphe(att1);
		Set<Attribut> dep1 = new HashSet<>();
		dep1.add(this.att1);
		dep1.add(this.att2);
		Set<Attribut> dep2 = new HashSet<>();
		dep2.add(this.att3);
		g1.ajouteArc(dep1, dep2);
		Relation rBis1 = new Relation(g1, att1);
		assertTrue("Test algo synthèse 1 : vérification que R1 est dedans", resultats.contains(rBis1));

		Set<Attribut> att2 = new HashSet<>();
		att2.add(this.att2);
		att2.add(this.att5);
		Graphe g2 = new Graphe(att2);
		dep1 = new HashSet<>();
		dep1.add(this.att2);
		dep2 = new HashSet<>();
		dep2.add(this.att5);
		g2.ajouteArc(dep1, dep2);
		Relation rBis2 = new Relation(g2, att2);
		assertTrue("Test algo synthèse 1 : vérification que R2 est dedans", resultats.contains(rBis2));

		Set<Attribut> att3 = new HashSet<>();
		att3.add(this.att2);
		att3.add(this.att4);
		att3.add(this.att5);
		Graphe g3 = new Graphe(att3);
		dep1 = new HashSet<>();
		dep1.add(this.att5);
		dep2 = new HashSet<>();
		dep2.add(this.att2);
		g3.ajouteArc(dep1, dep2);
		dep2 = new HashSet<>();
		dep2.add(this.att4);
		g3.ajouteArc(dep1, dep2);
		Relation rBis3 = new Relation(g3, att3);
		assertTrue("Test algo synthèse 1 : vérification que R3 est dedans", resultats.contains(rBis3));

		Set<Attribut> att4 = new HashSet<>();
		att4.add(this.att1);
		att4.add(this.att5);
		Graphe g4 = new Graphe(att4);
		dep1 = new HashSet<>();
		dep1.add(this.att1);
		dep2 = new HashSet<>();
		dep2.add(this.att5);
		g4.ajouteArc(dep1, dep2);
		Relation rBis4 = new Relation(g4, att4);
		assertTrue("Test algo synthèse 1 : vérification que R4 est dedans", resultats.contains(rBis4));

	}

	// Test recherché Clé

	@Test
	public void testRechercheCle1()
			throws IllegalArgumentException, EnsembleAttributsVideException, AttributNonPresentException {
		Set<Attribut> atts = new HashSet<>();
		atts.add(att1);
		atts.add(att2);
		atts.add(att3);

		Graphe g = new Graphe(atts);
		Set<Attribut> dep1 = new TreeSet<>();
		dep1.add(this.att1);
		Set<Attribut> dep2 = new TreeSet<>();
		dep2.add(this.att2);
		g.ajouteArc(dep1, dep2);
		dep2 = new TreeSet<>();
		dep2.add(this.att3);
		g.ajouteArc(dep1, dep2);

		Set<Attribut> cle = new TreeSet<>();
		cle.add(this.att1);

		Relation r = new Relation(g, atts);
		assertTrue("Test recherche d'une clé avec un seul attribut", r.rechercheCle().equals(cle));
	}

	@Test
	public void testRechercheCle2()
			throws IllegalArgumentException, EnsembleAttributsVideException, AttributNonPresentException {
		Set<Attribut> atts = new HashSet<>();
		atts.add(att1);
		atts.add(att2);
		atts.add(att3);
		atts.add(att4);

		Graphe g = new Graphe(atts);
		Set<Attribut> dep1 = new TreeSet<>();
		dep1.add(this.att1);
		Set<Attribut> dep2 = new TreeSet<>();
		dep2.add(this.att2);
		g.ajouteArc(dep1, dep2);
		dep1 = new TreeSet<>();
		dep1.add(this.att3);
		dep2 = new TreeSet<>();
		dep2.add(this.att4);
		g.ajouteArc(dep1, dep2);

		Set<Attribut> cle = new TreeSet<>();
		cle.add(this.att1);
		cle.add(this.att3);

		Relation r = new Relation(g, atts);
		assertTrue("Test recherche d'une clé avec deux attributs", r.rechercheCle().equals(cle));
	}

	@Test
	public void testRechercheCle3()
			throws IllegalArgumentException, EnsembleAttributsVideException, AttributNonPresentException {
		Set<Attribut> atts = new TreeSet<>();
		atts.add(att1);
		atts.add(att2);

		Graphe g = new Graphe(atts);
		Set<Attribut> dep1 = new TreeSet<>();
		dep1.add(this.att1);
		Set<Attribut> dep2 = new TreeSet<>();
		dep2.add(this.att2);
		g.ajouteArc(dep1, dep2);
		g.ajouteArc(dep2, dep1);

		Set<Attribut> cle1 = new TreeSet<>();
		cle1.add(this.att1);
		Set<Attribut> cle2 = new TreeSet<>();
		cle2.add(this.att2);

		Relation r = new Relation(g, atts);

		assertTrue("Test recherche d'une clé dépendance en boucle",
				r.rechercheCle().equals(cle1) || r.rechercheCle().equals(cle2));

	}

	@Test
	public void testRechercheCle4() throws IllegalArgumentException, EnsembleAttributsVideException,
			AttributNonPresentException, NomVideException, EnsembleDependancesInvalideException {
		Set<Attribut> atts = new TreeSet<>();
		atts.add(new Attribut("F"));
		atts.add(new Attribut("h"));
		atts.add(new Attribut("j"));
		atts.add(new Attribut("z"));

		Graphe g = Parseur.convertirEnGraphe(atts,
				"{F,j->F; h,z->F,j,z; h,j->F,j,z; h,j->h,j,z; j->z; h,j,z->F,h; F,h->h}");

		Set<Attribut> cle1 = new TreeSet<>();
		cle1.add(new Attribut("h"));
		cle1.add(new Attribut("j"));
		Set<Attribut> cle2 = new TreeSet<>();
		cle2.add(new Attribut("h"));
		cle2.add(new Attribut("z"));

		Relation r = new Relation(g, atts);

		assertTrue("Test recherche d'une clé qui posait problème",
				r.rechercheCle().equals(cle1) || r.rechercheCle().equals(cle2));
	}

	@Test
	public void testRechercheFormeNormal()
			throws IllegalArgumentException, AttributNonPresentException, EnsembleAttributsVideException {
		Set<Attribut> atts = new HashSet<>();
		atts.add(att1);
		atts.add(att2);

		Graphe g = new Graphe(atts);
		Set<Attribut> dep1 = new TreeSet<>();
		dep1.add(this.att1);
		Set<Attribut> dep2 = new TreeSet<>();
		dep2.add(this.att2);
		g.ajouteArc(dep1, dep2);
		g.ajouteArc(dep2, dep1);

		Set<Attribut> cle1 = new TreeSet<>();
		cle1.add(this.att1);
		Set<Attribut> cle2 = new TreeSet<>();
		cle2.add(this.att2);

		Relation r = new Relation(g, atts);
		assertTrue("Test recherche forme normal", r.equals(r));
	}
}
