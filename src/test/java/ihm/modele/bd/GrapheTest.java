package ihm.modele.bd;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

import org.junit.Before;
import org.junit.Test;

import ihm.modele.exceptions.AttributNonPresentException;
import ihm.modele.exceptions.EnsembleAttributsVideException;
import ihm.modele.exceptions.EnsembleDependancesInvalideException;
import ihm.modele.exceptions.NomVideException;
import ihm.modele.utilisateur.Parseur;

public class GrapheTest {

	private Graphe graphe, grapheDep;
	private Set<Attribut> ensembleInitial;
	private Attribut att1, att2, att3, att4, att5;

	@Before
	public void setUp() {
		this.ensembleInitial = new HashSet<>();

		// Remplissage de l'ensemble initial
		try {
			this.att1 = new Attribut("Attribut 1");
			this.att2 = new Attribut("Attribut 2");
			this.att3 = new Attribut("Attribut 3");
			this.att4 = new Attribut("Attribut 4");
			this.att5 = new Attribut("Attribut 5");

			this.ensembleInitial.add(this.att1);
			this.ensembleInitial.add(this.att2);
			this.ensembleInitial.add(this.att3);
			this.ensembleInitial.add(this.att4);
			this.ensembleInitial.add(this.att5);

			this.graphe = new Graphe(this.ensembleInitial);
			this.grapheDep = new Graphe(this.graphe);

			Set<Attribut> dep1 = new HashSet<>();
			dep1.add(this.att1);
			dep1.add(this.att2);
			Set<Attribut> dep2 = new HashSet<>();
			dep2.add(this.att3);
			dep2.add(this.att4);
			this.grapheDep.ajouteArc(dep1, dep2);

			dep1 = new HashSet<>();
			dep1.add(this.att1);
			dep2 = new HashSet<>();
			dep2.add(this.att4);
			dep2.add(this.att5);
			this.grapheDep.ajouteArc(dep1, dep2);

			dep1 = new HashSet<>();
			dep1.add(this.att1);
			dep1.add(this.att2);
			dep2 = new HashSet<>();
			dep2.add(this.att5);
			this.grapheDep.ajouteArc(dep1, dep2);

			dep1 = new HashSet<>();
			dep1.add(this.att5);
			dep2 = new HashSet<>();
			dep2.add(this.att4);
			dep2.add(this.att2);
			this.grapheDep.ajouteArc(dep1, dep2);

			dep1 = new HashSet<>();
			dep1.add(this.att2);
			dep2 = new HashSet<>();
			dep2.add(this.att5);
			this.grapheDep.ajouteArc(dep1, dep2);
		} catch (Exception e) {
			System.out.println("Erreur création des données de test");
			e.printStackTrace();
		}

	}

	@Test
	public void testConstructeur1() throws IllegalArgumentException {
		Graphe g = new Graphe(this.ensembleInitial);
		assertEquals("Test création d'un graphe avec un ensemble d'attributs", this.ensembleInitial,
				g.getEnsembleAttributs());
	}

	@Test(expected = IllegalArgumentException.class)
	public void testConstructeur2() throws IllegalArgumentException {
		new Graphe((Set<Attribut>) null);
	}

	@Test
	public void testConstructeurClonage1() throws IllegalArgumentException {
		Graphe g = new Graphe(this.graphe);
		assertEquals("Test création d'un graphe clone du graphe de base", this.ensembleInitial,
				g.getEnsembleAttributs());
	}

	@Test(expected = IllegalArgumentException.class)
	public void testConstructeurClonage2() throws IllegalArgumentException {
		new Graphe((Graphe) null);
	}

	/*
	 * Test de l'ajout d'arcs (dépendances)
	 */
	@Test
	public void testAjoutArc1()
			throws AttributNonPresentException, IllegalArgumentException, EnsembleAttributsVideException {
		Set<Attribut> dep1 = new TreeSet<>();
		dep1.add(this.att1);
		Set<Attribut> dep2 = new TreeSet<>();
		dep2.add(this.att2);
		this.graphe.ajouteArc(dep1, dep2);
		if (this.graphe.getListeAdjacence().containsKey(dep1))
			assertTrue("Test ajout d'un arc avec att1 contenant 1 attribut présent dans le graphe",
					this.graphe.getListeAdjacence().get(dep1).contains(dep2));
		else
			fail("Test ajout d'un arc avec att1 contenant 1 attribut présent dans le graphe");
	}

	@Test
	public void testAjoutArc2()
			throws AttributNonPresentException, IllegalArgumentException, EnsembleAttributsVideException {
		Set<Attribut> dep1 = new TreeSet<>();
		dep1.add(this.att1);
		dep1.add(this.att3);
		dep1.add(this.att4);
		Set<Attribut> dep2 = new TreeSet<>();
		dep2.add(this.att2);
		this.graphe.ajouteArc(dep1, dep2);
		if (this.graphe.getListeAdjacence().containsKey(dep1))
			assertTrue("Test ajout d'un arc avec att1 contenant 3 attributs présent dans le graphe",
					this.graphe.getListeAdjacence().get(dep1).contains(dep2));
		else
			fail("Test ajout d'un arc avec att1 contenant 3 attributs présent dans le graphe");
	}

	@Test(expected = AttributNonPresentException.class)
	public void testAjoutArc3() throws AttributNonPresentException, NomVideException, IllegalArgumentException,
			EnsembleAttributsVideException {
		Set<Attribut> dep1 = new TreeSet<>();
		dep1.add(this.att1);
		Set<Attribut> dep2 = new TreeSet<>();
		dep2.add(new Attribut("Coucou"));
		this.graphe.ajouteArc(dep1, dep2);
	}

	@Test(expected = AttributNonPresentException.class)
	public void testAjoutArc4() throws AttributNonPresentException, NomVideException, IllegalArgumentException,
			EnsembleAttributsVideException {
		Set<Attribut> dep1 = new TreeSet<>();
		dep1.add(this.att1);
		dep1.add(this.att2);
		dep1.add(this.att4);
		dep1.add(this.att5);
		Set<Attribut> dep2 = new TreeSet<>();
		dep2.add(new Attribut("Coucou"));
		this.graphe.ajouteArc(dep1, dep2);
	}

	@Test(expected = AttributNonPresentException.class)
	public void testAjoutArc5() throws AttributNonPresentException, NomVideException, IllegalArgumentException,
			EnsembleAttributsVideException {
		Set<Attribut> dep1 = new TreeSet<>();
		dep1.add(this.att1);
		dep1.add(new Attribut("Coucou"));
		dep1.add(this.att4);
		dep1.add(this.att5);
		Set<Attribut> dep2 = new TreeSet<>();
		dep2.add(this.att2);
		this.graphe.ajouteArc(dep1, dep2);
	}

	@Test(expected = AttributNonPresentException.class)
	public void testAjoutArc6() throws AttributNonPresentException, NomVideException, IllegalArgumentException,
			EnsembleAttributsVideException {
		Set<Attribut> dep1 = new TreeSet<>();
		dep1.add(new Attribut("Coucou"));
		dep1.add(new Attribut("Coucou2"));
		Set<Attribut> dep2 = new TreeSet<>();
		dep2.add(this.att2);
		this.graphe.ajouteArc(dep1, dep2);
	}

	@Test(expected = EnsembleAttributsVideException.class)
	public void testAjoutArc7() throws AttributNonPresentException, NomVideException, IllegalArgumentException,
			EnsembleAttributsVideException {
		Set<Attribut> dep1 = new TreeSet<>();
		Set<Attribut> dep2 = new TreeSet<>();
		dep2.add(this.att2);
		this.graphe.ajouteArc(dep1, dep2);
	}

	@Test(expected = EnsembleAttributsVideException.class)
	public void testAjoutArc8() throws AttributNonPresentException, NomVideException, IllegalArgumentException,
			EnsembleAttributsVideException {
		Set<Attribut> dep1 = new TreeSet<>();
		dep1.add(this.att2);
		Set<Attribut> dep2 = new TreeSet<>();
		this.graphe.ajouteArc(dep1, dep2);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testAjoutArc9() throws AttributNonPresentException, NomVideException, IllegalArgumentException,
			EnsembleAttributsVideException {
		Set<Attribut> dep1 = null;
		Set<Attribut> dep2 = new TreeSet<>();
		dep2.add(this.att2);
		this.graphe.ajouteArc(dep1, dep2);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testAjoutArc10() throws AttributNonPresentException, NomVideException, IllegalArgumentException,
			EnsembleAttributsVideException {
		Set<Attribut> dep1 = new TreeSet<>();
		dep1.add(this.att1);
		dep1.add(this.att2);
		this.graphe.ajouteArc(dep1, null);
	}

	/*
	 * Test de présence d'arcs (dépendances)
	 */
	@Test
	public void testEstArc1()
			throws AttributNonPresentException, IllegalArgumentException, EnsembleAttributsVideException {
		Set<Attribut> dep1 = new TreeSet<>();
		dep1.add(this.att1);
		Set<Attribut> dep2 = new TreeSet<>();
		dep2.add(this.att2);
		this.graphe.ajouteArc(dep1, dep2);
		assertTrue("Test présence d'un arc présent avec att1 contenant 1 attribut", this.graphe.estArc(dep1, dep2));
	}

	@Test
	public void testEstArc2()
			throws AttributNonPresentException, IllegalArgumentException, EnsembleAttributsVideException {
		Set<Attribut> dep1 = new TreeSet<>();
		dep1.add(this.att1);
		dep1.add(this.att5);
		Set<Attribut> dep2 = new TreeSet<>();
		dep2.add(this.att2);
		this.graphe.ajouteArc(dep1, dep2);
		assertTrue("Test présence d'un arc présent avec att1 contenant 2 attributs", this.graphe.estArc(dep1, dep2));
	}

	@Test
	public void testEstArc3()
			throws AttributNonPresentException, IllegalArgumentException, EnsembleAttributsVideException {
		Set<Attribut> dep1 = new TreeSet<>();
		dep1.add(this.att1);
		Set<Attribut> dep2 = new TreeSet<>();
		dep2.add(this.att2);
		assertFalse("Test présence d'un arc non présent avec att1 contenant 1 attributs",
				this.graphe.estArc(dep1, dep2));
	}

	@Test
	public void testEstArc4()
			throws AttributNonPresentException, IllegalArgumentException, EnsembleAttributsVideException {
		Set<Attribut> dep1 = new TreeSet<>();
		dep1.add(this.att1);
		dep1.add(this.att2);
		Set<Attribut> dep2 = new TreeSet<>();
		dep2.add(this.att2);
		assertFalse("Test présence d'un arc non présent avec att1 contenant 2 attributs",
				this.graphe.estArc(dep1, dep2));
	}

	@Test(expected = AttributNonPresentException.class)
	public void testEstArc5() throws AttributNonPresentException, NomVideException, IllegalArgumentException,
			EnsembleAttributsVideException {
		Set<Attribut> dep1 = new TreeSet<>();
		dep1.add(new Attribut("salut aaa"));
		Set<Attribut> dep2 = new TreeSet<>();
		dep2.add(this.att2);
		assertFalse("Test présence d'un arc avec attribut non présent (att1 contient 1/1 attribut non présent)",
				this.graphe.estArc(dep1, dep2));
	}

	@Test(expected = AttributNonPresentException.class)
	public void testEstArc6() throws AttributNonPresentException, NomVideException, IllegalArgumentException,
			EnsembleAttributsVideException {
		Set<Attribut> dep1 = new TreeSet<>();
		dep1.add(this.att1);
		dep1.add(new Attribut("salut aaa"));
		Set<Attribut> dep2 = new TreeSet<>();
		dep2.add(this.att2);
		assertFalse("Test présence d'un arc avec attribut non présent (att1 contient 1/2 attribut non présent)",
				this.graphe.estArc(dep1, dep2));
	}

	@Test(expected = AttributNonPresentException.class)
	public void testEstArc7() throws AttributNonPresentException, NomVideException, IllegalArgumentException,
			EnsembleAttributsVideException {
		Set<Attribut> dep1 = new TreeSet<>();
		dep1.add(new Attribut("salut aadfgh"));
		dep1.add(new Attribut("salut aaa"));
		Set<Attribut> dep2 = new TreeSet<>();
		dep2.add(this.att2);
		assertFalse("Test présence d'un arc avec attribut non présent (att1 contient 2/2 attribut non présent)",
				this.graphe.estArc(dep1, dep2));
	}

	@Test(expected = AttributNonPresentException.class)
	public void testEstArc8() throws AttributNonPresentException, NomVideException, IllegalArgumentException,
			EnsembleAttributsVideException {
		Set<Attribut> dep1 = new TreeSet<>();
		dep1.add(this.att1);
		Set<Attribut> dep2 = new TreeSet<>();
		dep2.add(new Attribut("salut aaa"));
		assertFalse("Test présence d'un arc avec attribut non présent (att2)", this.graphe.estArc(dep1, dep2));
	}

	@Test(expected = EnsembleAttributsVideException.class)
	public void testEstArc9() throws AttributNonPresentException, NomVideException, IllegalArgumentException,
			EnsembleAttributsVideException {
		Set<Attribut> dep1 = new TreeSet<>();
		Set<Attribut> dep2 = new TreeSet<>();
		dep2.add(this.att1);
		assertFalse("Test présence d'un arc avec att1 vide", this.graphe.estArc(dep1, dep2));
	}

	@Test(expected = EnsembleAttributsVideException.class)
	public void testEstArc10() throws AttributNonPresentException, NomVideException, IllegalArgumentException,
			EnsembleAttributsVideException {
		Set<Attribut> dep1 = new TreeSet<>();
		dep1.add(this.att1);
		Set<Attribut> dep2 = new TreeSet<>();
		assertFalse("Test présence d'un arc avec att1 vide", this.graphe.estArc(dep1, dep2));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testEstArc11() throws AttributNonPresentException, NomVideException, IllegalArgumentException,
			EnsembleAttributsVideException {
		Set<Attribut> dep1 = null;
		Set<Attribut> dep2 = new TreeSet<>();
		dep2.add(this.att1);
		assertFalse("Test présence d'un arc avec att1 nulle", this.graphe.estArc(dep1, dep2));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testEstArc12() throws AttributNonPresentException, NomVideException, IllegalArgumentException,
			EnsembleAttributsVideException {
		Set<Attribut> dep1 = new TreeSet<>();
		dep1.add(this.att1);
		assertFalse("Test présence d'un arc avec att2 nulle", this.graphe.estArc(dep1, null));
	}

	/*
	 * Test suppression d'un arc
	 */
	@Test
	public void testSupprimerArc1()
			throws AttributNonPresentException, IllegalArgumentException, EnsembleAttributsVideException {
		Set<Attribut> dep1 = new TreeSet<>();
		dep1.add(this.att1);
		Set<Attribut> dep2 = new TreeSet<>();
		dep2.add(this.att2);
		this.graphe.ajouteArc(dep1, dep2);
		this.graphe.supprimerArc(dep1, dep2);
		assertTrue("Test suppression d'un arc présent avec att1 contenant 1 attribut",
				this.graphe.getListeAdjacence().keySet().size() == 0);
	}

	@Test
	public void testSupprimerArc2()
			throws AttributNonPresentException, IllegalArgumentException, EnsembleAttributsVideException {
		Set<Attribut> dep1 = new TreeSet<>();
		dep1.add(this.att1);
		dep1.add(this.att5);
		Set<Attribut> dep2 = new TreeSet<>();
		dep2.add(this.att2);
		this.graphe.ajouteArc(dep1, dep2);
		this.graphe.supprimerArc(dep1, dep2);
		assertTrue("Test suppression d'un arc présent avec att1 contenant 2 attribut",
				this.graphe.getListeAdjacence().keySet().size() == 0);
	}

	@Test
	public void testSupprimerArc3()
			throws AttributNonPresentException, IllegalArgumentException, EnsembleAttributsVideException {
		Set<Attribut> dep1 = new TreeSet<>();
		dep1.add(this.att1);
		Set<Attribut> dep2 = new TreeSet<>();
		dep2.add(this.att2);
		this.graphe.supprimerArc(dep1, dep2);
		if (this.graphe.getListeAdjacence().containsKey(dep1))
			assertFalse("Test suppression d'un arc non présent avec att1 contenant 1 attribut",
					this.graphe.getListeAdjacence().get(dep1).contains(dep2));
		else
			assertTrue("Test suppression d'un arc non présent avec att1 contenant 1 attribut", true);
	}

	@Test
	public void testSupprimerArc4()
			throws AttributNonPresentException, IllegalArgumentException, EnsembleAttributsVideException {
		Set<Attribut> dep1 = new TreeSet<>();
		dep1.add(this.att1);
		dep1.add(this.att2);
		Set<Attribut> dep2 = new TreeSet<>();
		dep2.add(this.att2);
		this.graphe.supprimerArc(dep1, dep2);
		if (this.graphe.getListeAdjacence().containsKey(dep1))
			assertFalse("Test suppression d'un arc non présent avec att1 contenant 2 attributs",
					this.graphe.getListeAdjacence().get(dep1).contains(dep2));
		else
			assertTrue("Test suppression d'un arc non présent avec att1 contenant 2 attributs", true);
	}

	@Test(expected = AttributNonPresentException.class)
	public void testSupprimerArc5() throws AttributNonPresentException, NomVideException, IllegalArgumentException,
			EnsembleAttributsVideException {
		Set<Attribut> dep1 = new TreeSet<>();
		dep1.add(new Attribut("salut aaa"));
		Set<Attribut> dep2 = new TreeSet<>();
		dep2.add(this.att2);
		this.graphe.supprimerArc(dep1, dep2);
	}

	@Test(expected = AttributNonPresentException.class)
	public void testSupprimerArc6() throws AttributNonPresentException, NomVideException, IllegalArgumentException,
			EnsembleAttributsVideException {
		Set<Attribut> dep1 = new TreeSet<>();
		dep1.add(this.att1);
		dep1.add(new Attribut("salut aaa"));
		Set<Attribut> dep2 = new TreeSet<>();
		dep2.add(this.att2);
		this.graphe.supprimerArc(dep1, dep2);
	}

	@Test(expected = AttributNonPresentException.class)
	public void testSupprimerArc7() throws AttributNonPresentException, NomVideException, IllegalArgumentException,
			EnsembleAttributsVideException {
		Set<Attribut> dep1 = new TreeSet<>();
		dep1.add(new Attribut("salut aadfgh"));
		dep1.add(new Attribut("salut aaa"));
		Set<Attribut> dep2 = new TreeSet<>();
		dep2.add(this.att2);
		this.graphe.supprimerArc(dep1, dep2);
	}

	@Test(expected = AttributNonPresentException.class)
	public void testSupprimerArc8() throws AttributNonPresentException, NomVideException, IllegalArgumentException,
			EnsembleAttributsVideException {
		Set<Attribut> dep1 = new TreeSet<>();
		dep1.add(this.att1);
		Set<Attribut> dep2 = new TreeSet<>();
		dep2.add(new Attribut("Tests finis pour le moment"));
		this.graphe.supprimerArc(dep1, dep2);
	}

	@Test(expected = EnsembleAttributsVideException.class)
	public void testSupprimerArc9() throws AttributNonPresentException, NomVideException, IllegalArgumentException,
			EnsembleAttributsVideException {
		Set<Attribut> dep1 = new TreeSet<>();
		Set<Attribut> dep2 = new TreeSet<>();
		dep2.add(this.att2);
		this.graphe.supprimerArc(dep1, dep2);
	}

	@Test(expected = EnsembleAttributsVideException.class)
	public void testSupprimerArc10() throws AttributNonPresentException, NomVideException, IllegalArgumentException,
			EnsembleAttributsVideException {
		Set<Attribut> dep1 = new TreeSet<>();
		dep1.add(this.att2);
		Set<Attribut> dep2 = new TreeSet<>();
		this.graphe.supprimerArc(dep1, dep2);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testSupprimerArc11() throws AttributNonPresentException, NomVideException, IllegalArgumentException,
			EnsembleAttributsVideException {
		Set<Attribut> dep1 = null;
		Set<Attribut> dep2 = new TreeSet<>();
		dep2.add(this.att2);
		this.graphe.supprimerArc(dep1, dep2);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testSupprimerArc12() throws AttributNonPresentException, NomVideException, IllegalArgumentException,
			EnsembleAttributsVideException {
		Set<Attribut> dep1 = new TreeSet<>();
		dep1.add(this.att1);
		this.graphe.supprimerArc(dep1, null);
	}

	/*
	 * Test de la liste des successeurs
	 */
	@Test
	public void testSucesseurs1() throws IllegalArgumentException, EnsembleAttributsVideException {
		Set<Attribut> attributs = new HashSet<>();
		attributs.add(this.att1);
		attributs.add(this.att2);
		Set<Attribut> attributs2 = new HashSet<>();
		attributs2.add(this.att3);
		attributs2.add(this.att4);
		Set<Attribut> attributs3 = new HashSet<>();
		attributs3.add(this.att5);
		Set<Set<Attribut>> attendus = new HashSet<>();
		attendus.add(attributs2);
		attendus.add(attributs3);
		assertEquals("Test sucesseurs d'un sommet contenant deux successeurs", attendus,
				this.grapheDep.successeurs(attributs));
	}

	@Test
	public void testSucesseurs2() throws IllegalArgumentException, EnsembleAttributsVideException {
		Set<Attribut> attributs = new HashSet<>();
		attributs.add(this.att2);
		Set<Attribut> attributs2 = new HashSet<>();
		attributs2.add(this.att5);
		Set<Set<Attribut>> attendus = new HashSet<>();
		attendus.add(attributs2);
		assertEquals("Test sucesseurs d'un sommet contenant deux successeurs", attendus,
				this.grapheDep.successeurs(attributs));
	}

	/*
	 * Test du equals
	 */
	@Test
	public void testEquals1() {
		assertTrue("Test Graphe égal à lui même", this.graphe.equals(this.graphe));
	}

	@Test
	public void testEquals2() {
		assertFalse("Test Graphe sans dépendances n'est pas égal à un Graphe avec des dépendances",
				this.graphe.equals(this.grapheDep));
	}

	@Test
	public void testEquals3() {
		Graphe g2 = new Graphe(this.grapheDep);
		assertTrue("Test Graphe est égal à un Graphe identique", this.grapheDep.equals(g2));
	}

	@Test
	public void testEquals4()
			throws IllegalArgumentException, AttributNonPresentException, EnsembleAttributsVideException {
		Graphe g2 = new Graphe(this.grapheDep);

		Set<Attribut> dep1 = new HashSet<>();
		dep1.add(this.att2);
		Set<Attribut> dep2 = new HashSet<>();
		dep2.add(this.att5);
		g2.supprimerArc(dep1, dep2);

		assertFalse("Test Graphe n'est pas égal à un Graphe avec 1 dépendance en moins", this.grapheDep.equals(g2));
	}

	@Test
	public void testEquals5()
			throws IllegalArgumentException, AttributNonPresentException, EnsembleAttributsVideException {
		Graphe g2 = new Graphe(this.grapheDep);

		Set<Attribut> dep1 = new HashSet<>();
		dep1.add(this.att1);
		dep1.add(this.att2);
		Set<Attribut> dep2 = new HashSet<>();
		dep2.add(this.att3);
		dep2.add(this.att4);
		g2.supprimerArc(dep1, dep2);

		dep1 = new HashSet<>();
		dep1.add(this.att1);
		dep2 = new HashSet<>();
		dep2.add(this.att4);
		dep2.add(this.att5);
		g2.supprimerArc(dep1, dep2);

		assertFalse("Test Graphe n'est pas égal à un Graphe avec 2 dépendances en moins", this.grapheDep.equals(g2));
	}

	/*
	 * Test de la fermeture transitive
	 */
	@Test
	public void testFermetureTransitive()
			throws IllegalArgumentException, AttributNonPresentException, EnsembleAttributsVideException {
		Graphe fermeture = this.grapheDep.fermetureTransitive();
		Graphe attendus = new Graphe(this.grapheDep);

		Set<Attribut> dep1 = new HashSet<>();
		dep1.add(this.att1);
		dep1.add(this.att2);
		Set<Attribut> dep2 = new HashSet<>();
		dep2.add(this.att4);
		attendus.ajouteArc(dep1, dep2);

		dep1 = new HashSet<>();
		dep1.add(this.att2);
		dep2 = new HashSet<>();
		dep2.add(this.att4);
		attendus.ajouteArc(dep1, dep2);

		dep1 = new HashSet<>();
		dep1.add(this.att1);
		dep2 = new HashSet<>();
		dep2.add(this.att2);
		attendus.ajouteArc(dep1, dep2);
		dep2 = new HashSet<>();
		dep2.add(this.att3);
		attendus.ajouteArc(dep1, dep2);

		dep1 = new HashSet<>();
		dep1.add(this.att1);
		dep2 = new HashSet<>();
		dep2.add(this.att5);
		attendus.ajouteArc(dep1, dep2);

		assertEquals("Test fermeture transitive 1", Graphe.dependancesSimple(attendus),
				Graphe.dependancesSimple(fermeture));
	}

	/*
	 * Test du sous-graphe induit
	 */
	@Test
	public void testGrapheInduit1()
			throws IllegalArgumentException, EnsembleAttributsVideException, AttributNonPresentException {
		Set<Attribut> atts2 = new TreeSet<>();
		atts2.add(this.att1);
		atts2.add(this.att2);
		atts2.add(this.att3);
		atts2.add(this.att4);
		atts2.add(this.att5);

		Graphe grapheAttendus = new Graphe(atts2);

		Set<Attribut> dep1 = new HashSet<>();
		dep1.add(this.att1);
		dep1.add(this.att2);
		Set<Attribut> dep2 = new HashSet<>();
		dep2.add(this.att3);
		dep2.add(this.att4);
		grapheAttendus.ajouteArc(dep1, dep2);

		dep1 = new HashSet<>();
		dep1.add(this.att1);
		dep2 = new HashSet<>();
		dep2.add(this.att4);
		dep2.add(this.att5);
		grapheAttendus.ajouteArc(dep1, dep2);

		dep1 = new HashSet<>();
		dep1.add(this.att1);
		dep1.add(this.att2);
		dep2 = new HashSet<>();
		dep2.add(this.att5);
		grapheAttendus.ajouteArc(dep1, dep2);

		dep1 = new HashSet<>();
		dep1.add(this.att5);
		dep2 = new HashSet<>();
		dep2.add(this.att4);
		dep2.add(this.att2);
		grapheAttendus.ajouteArc(dep1, dep2);

		dep1 = new HashSet<>();
		dep1.add(this.att2);
		dep2 = new HashSet<>();
		dep2.add(this.att5);
		grapheAttendus.ajouteArc(dep1, dep2);

		assertEquals("Test graphe égale au sous-graphe induit contenant tous les attributs", this.grapheDep,
				this.grapheDep.grapheInduit(atts2));
	}

	@Test
	public void testGrapheInduit2()
			throws IllegalArgumentException, EnsembleAttributsVideException, AttributNonPresentException {
		Set<Attribut> atts2 = new TreeSet<>();
		atts2.add(this.att1);
		atts2.add(this.att2);
		atts2.add(this.att3);
		atts2.add(this.att5);

		Graphe grapheAttendus = new Graphe(atts2);

		Set<Attribut> dep1 = new HashSet<>();
		dep1.add(this.att1);
		dep1.add(this.att2);
		Set<Attribut> dep2 = new HashSet<>();
		dep2.add(this.att3);
		grapheAttendus.ajouteArc(dep1, dep2);

		dep1 = new HashSet<>();
		dep1.add(this.att1);
		dep2 = new HashSet<>();
		dep2.add(this.att5);
		grapheAttendus.ajouteArc(dep1, dep2);

		dep1 = new HashSet<>();
		dep1.add(this.att1);
		dep1.add(this.att2);
		dep2 = new HashSet<>();
		dep2.add(this.att5);
		grapheAttendus.ajouteArc(dep1, dep2);

		dep1 = new HashSet<>();
		dep1.add(this.att5);
		dep2 = new HashSet<>();
		dep2.add(this.att2);
		grapheAttendus.ajouteArc(dep1, dep2);

		dep1 = new HashSet<>();
		dep1.add(this.att2);
		dep2 = new HashSet<>();
		dep2.add(this.att5);
		grapheAttendus.ajouteArc(dep1, dep2);

		assertEquals("Test sous-graphe induit avec 1 attribut en moins", grapheAttendus,
				this.grapheDep.grapheInduit(atts2));
	}

	@Test
	public void testGrapheInduit3()
			throws IllegalArgumentException, EnsembleAttributsVideException, AttributNonPresentException {
		Set<Attribut> atts2 = new TreeSet<>();
		atts2.add(this.att1);
		atts2.add(this.att2);
		atts2.add(this.att5);

		Graphe grapheAttendus = new Graphe(atts2);

		Set<Attribut> dep1 = new HashSet<>();
		dep1.add(this.att1);
		Set<Attribut> dep2 = new HashSet<>();
		dep2.add(this.att5);
		grapheAttendus.ajouteArc(dep1, dep2);

		dep1 = new HashSet<>();
		dep1.add(this.att1);
		dep1.add(this.att2);
		dep2 = new HashSet<>();
		dep2.add(this.att5);
		grapheAttendus.ajouteArc(dep1, dep2);

		dep1 = new HashSet<>();
		dep1.add(this.att5);
		dep2 = new HashSet<>();
		dep2.add(this.att2);
		grapheAttendus.ajouteArc(dep1, dep2);

		dep1 = new HashSet<>();
		dep1.add(this.att2);
		dep2 = new HashSet<>();
		dep2.add(this.att5);
		grapheAttendus.ajouteArc(dep1, dep2);

		assertEquals("Test sous-graphe induit avec 2 attributs en moins", grapheAttendus,
				this.grapheDep.grapheInduit(atts2));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testGrapheInduit4()
			throws IllegalArgumentException, EnsembleAttributsVideException, AttributNonPresentException {
		this.grapheDep.grapheInduit(null);
		fail("Test sous-graphe induit avec ensemble d'attributs null");
	}

	@Test(expected = EnsembleAttributsVideException.class)
	public void testGrapheInduit5()
			throws IllegalArgumentException, EnsembleAttributsVideException, AttributNonPresentException {
		this.grapheDep.grapheInduit(new TreeSet<>());
		fail("Test sous-graphe induit avec ensemble d'attributs vide");
	}

	@Test(expected = AttributNonPresentException.class)
	public void testGrapheInduit6() throws IllegalArgumentException, EnsembleAttributsVideException,
			AttributNonPresentException, NomVideException {
		Set<Attribut> atts2 = new TreeSet<>(this.ensembleInitial);
		atts2.add(new Attribut("Borderlands"));

		this.grapheDep.grapheInduit(atts2);
		fail("Test sous-graphe induit avec ensemble d'attributs contenant 1 attribut non présent");
	}

	@Test(expected = AttributNonPresentException.class)
	public void testGrapheInduit7() throws IllegalArgumentException, EnsembleAttributsVideException,
			AttributNonPresentException, NomVideException {
		Set<Attribut> atts2 = new TreeSet<>(this.ensembleInitial);
		atts2.add(new Attribut("Borderlands"));
		atts2.add(new Attribut("Borderlands 2"));

		this.grapheDep.grapheInduit(atts2);
		fail("Test sous-graphe induit avec ensemble d'attributs contenant 2 attributs non présent");
	}

	// Test Couverture minimale

	@Test
	public void testCouvertureMinimale1()
			throws IllegalArgumentException, EnsembleAttributsVideException, AttributNonPresentException {
		Graphe g = new Graphe(this.ensembleInitial);
		assertTrue("Test couverture minimale d'un ensemble vide donne un ensemble vide",
				g.couvertureMinimale().equals(g));
	}

	@Test
	public void testCouvertureMinimale2()
			throws IllegalArgumentException, EnsembleAttributsVideException, AttributNonPresentException {
		Graphe g = new Graphe(this.ensembleInitial);
		Set<Attribut> dep1 = new TreeSet<>();
		dep1.add(this.att1);
		dep1.add(this.att3);
		dep1.add(this.att4);
		Set<Attribut> dep2 = new TreeSet<>();
		dep2.add(this.att2);
		g.ajouteArc(dep1, dep2);
		assertTrue("Test couverture minimale d'un ensemble avec une seule dépendance",
				g.couvertureMinimale().equals(g));
	}

	@Test
	public void testCouvertureMinimale3()
			throws IllegalArgumentException, EnsembleAttributsVideException, AttributNonPresentException {
		Graphe g = new Graphe(this.ensembleInitial);
		Set<Attribut> dep1 = new TreeSet<>();
		dep1.add(this.att1);
		dep1.add(this.att3);
		dep1.add(this.att4);
		Set<Attribut> dep2 = new TreeSet<>();
		dep2.add(this.att2);
		g.ajouteArc(dep1, dep2);
		dep2 = new TreeSet<>();
		dep2.add(this.att5);
		g.ajouteArc(dep1, dep2);
		assertTrue("Test couverture minimale d'un ensemble avec deux dépendances", g.couvertureMinimale().equals(g));
	}

	@Test
	public void testCouvertureMinimale4()
			throws IllegalArgumentException, EnsembleAttributsVideException, AttributNonPresentException {
		Graphe g = new Graphe(this.ensembleInitial);
		Set<Attribut> dep1 = new TreeSet<>();
		dep1.add(this.att1);
		dep1.add(this.att3);
		Set<Attribut> dep2 = new TreeSet<>();
		dep2.add(this.att2);
		g.ajouteArc(dep1, dep2);
		dep2 = new TreeSet<>();
		dep2.add(this.att5);
		g.ajouteArc(dep1, dep2);

		Graphe g2 = new Graphe(g);
		dep1 = new TreeSet<>();
		dep1.add(this.att1);
		dep1.add(this.att3);
		dep1.add(this.att4);
		dep2 = new TreeSet<>();
		dep2.add(this.att2);
		g2.ajouteArc(dep1, dep2);

		assertTrue("Test couverture minimale d'un ensemble avec un attribut superflu",
				g.couvertureMinimale().equals(g));
	}

	@Test
	public void testCouvertureMinimale5()
			throws IllegalArgumentException, EnsembleAttributsVideException, AttributNonPresentException {
		Graphe g = new Graphe(this.ensembleInitial);
		Set<Attribut> dep1 = new TreeSet<>();
		dep1.add(this.att1);
		Set<Attribut> dep2 = new TreeSet<>();
		dep2.add(this.att2);
		g.ajouteArc(dep1, dep2);
		dep2 = new TreeSet<>();
		dep2.add(this.att3);
		g.ajouteArc(dep1, dep2);

		Graphe g2 = new Graphe(g);
		dep1 = new TreeSet<>();
		dep1.add(this.att1);
		dep2 = new TreeSet<>();
		dep2.add(this.att2);
		dep2.add(this.att3);
		g2.ajouteArc(dep1, dep2);

		assertTrue("Test couverture minimale d'un ensemble avec une dépendance non simple",
				g.couvertureMinimale().equals(g));
	}

	@Test
	public void testCouvertureMinimale6()
			throws IllegalArgumentException, EnsembleAttributsVideException, AttributNonPresentException {
		Graphe g = new Graphe(this.ensembleInitial);
		Set<Attribut> dep1 = new TreeSet<>();
		dep1.add(this.att1);
		Set<Attribut> dep2 = new TreeSet<>();
		dep2.add(this.att2);
		g.ajouteArc(dep1, dep2);
		dep2 = new TreeSet<>();
		dep2.add(this.att3);
		g.ajouteArc(dep1, dep2);
		dep1 = new TreeSet<>();
		dep1.add(this.att2);
		g.ajouteArc(dep1, dep2);

		Graphe g2 = new Graphe(this.ensembleInitial);
		dep1 = new TreeSet<>();
		dep1.add(this.att1);
		dep2 = new TreeSet<>();
		dep2.add(this.att2);
		g2.ajouteArc(dep1, dep2);
		dep1 = new TreeSet<>();
		dep1.add(this.att2);
		dep2 = new TreeSet<>();
		dep2.add(this.att3);
		g2.ajouteArc(dep1, dep2);

		assertTrue("Test couverture minimale d'un ensemble avec une dépendance redondante",
				g.couvertureMinimale().equals(g2));
	}

	@Test
	public void testCouvertureMinimale7()
			throws IllegalArgumentException, EnsembleAttributsVideException, AttributNonPresentException, NomVideException, EnsembleDependancesInvalideException {
		Set<Attribut> atts = new TreeSet<>();
		atts.add(new Attribut("F"));
		atts.add(new Attribut("h"));
		atts.add(new Attribut("j"));
		atts.add(new Attribut("z"));

		Graphe g = Parseur.convertirEnGraphe(atts,
				"{F,j->F; h,z->F,j,z; h,j->F,j,z; h,j->h,j,z; j->z; h,j,z->F,h; F,h->h}");

		Graphe g2 = Parseur.convertirEnGraphe(atts, "{h,z->j; h,j->F; j->z}");

		Set<Attribut> a = new TreeSet<>();
		a.add(new Attribut("h"));
		a.add(new Attribut("z"));

		assertEquals("Test couverture minimale d'un ensemble de dépendances qui posait problème",
				Graphe.dependancesSimple(g2), Graphe.dependancesSimple(g.couvertureMinimale()));
	}
}
