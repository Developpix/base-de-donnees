package ihm.modele.bd;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import ihm.modele.exceptions.NomVideException;

public class AttributTest {

	/*
	 * Test du constructeur
	 */

	@Test(expected = NomVideException.class)
	public void testConstructeur1() throws NomVideException, IllegalArgumentException {
		new Attribut("");
	}

	@Test
	public void testConstructeur2() throws NomVideException, IllegalArgumentException {
		Attribut att = new Attribut("Attribut de test");
		assertTrue("Test que le nom de l'attribut est bien 'Attribut de test'",
				att.toString().equals("Attribut de test"));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testConstructeur3() throws NomVideException, IllegalArgumentException {
		new Attribut(null);
	}

	/*
	 * Test de la fonction de hachage
	 */

	@Test
	public void testHashCode1() throws NomVideException, IllegalArgumentException {
		Attribut att = new Attribut("Je test");
		assertTrue("Test que le hash code est le même si on reprend le même attribut",
				att.hashCode() == att.hashCode());
	}

	@Test
	public void testHashCode2() throws NomVideException, IllegalArgumentException {
		Attribut att1 = new Attribut("Je test");
		Attribut att2 = new Attribut("Je test");
		assertTrue("Test que le hash code est le même si les attributs ont le même nom",
				att1.hashCode() == att2.hashCode());
	}

	@Test
	public void testHashCode3() throws NomVideException, IllegalArgumentException {
		Attribut att1 = new Attribut("Je test");
		Attribut att2 = new Attribut("Bonjour tous le monde");
		assertTrue("Test que le hash code est différent pour des attributs avec un nom différent",
				att1.hashCode() != att2.hashCode());
	}

	@Test
	public void testHashCode4() throws NomVideException, IllegalArgumentException {
		Attribut att1 = new Attribut("Je test");
		Attribut att2 = new Attribut("Je test 2");
		assertTrue("Test que le hash code est différent pour des attributs avec un nom presque identique",
				att1.hashCode() != att2.hashCode());
	}

	/*
	 * Test du equals
	 */

	@Test
	public void testEquals1() throws NomVideException, IllegalArgumentException {
		Attribut att = new Attribut("Je test");
		assertTrue("Test que l'attribut est égal à lui même", att.equals(att));
	}

	@Test
	public void testEquals2() throws NomVideException, IllegalArgumentException {
		Attribut att1 = new Attribut("Je test");
		Attribut att2 = new Attribut("ndzbbcvsbcjdze vherb");
		assertTrue("Test que deux attributs avec un nom très différents ne sont pas égaux", !att1.equals(att2));
	}

	@Test
	public void testEquals3() throws NomVideException, IllegalArgumentException {
		Attribut att1 = new Attribut("Je test");
		Attribut att2 = new Attribut("Je test4");
		assertTrue("Test que deux attributs avec un nom presque identique ne sont pas égaux", !att1.equals(att2));
	}

	@Test
	public void testComparateur1() throws NomVideException, IllegalArgumentException {
		Attribut att1 = new Attribut("Je test4");
		Attribut att2 = new Attribut("Je test");
		assertTrue("Test comparaison premier attribut avant le deuxième", att1.compareTo(att2) > 0);
	}

	@Test
	public void testComparateur2() throws NomVideException, IllegalArgumentException {
		Attribut att1 = new Attribut("Je test");
		Attribut att2 = new Attribut("Je test10");
		assertTrue("Test comparaison premier attribut après le deuxième CT1", att1.compareTo(att2) < 0);
	}

	@Test
	public void testComparateur3() throws NomVideException, IllegalArgumentException {
		Attribut att1 = new Attribut("Je testZ");
		Attribut att2 = new Attribut("Je testD");
		assertTrue("Test comparaison premier attribut après le deuxième CT2", att1.compareTo(att2) > 0);
	}

	@Test
	public void testComparateur4() throws NomVideException, IllegalArgumentException {
		Attribut att1 = new Attribut("Je test l'égalité");
		Attribut att2 = new Attribut("Je test l'égalité");
		assertTrue("Test comparaison premier attribut égaux au deuxième", att1.compareTo(att2) == 0);
	}

}
