package ihm.vue;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.Insets;
import java.util.Map;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;
import javax.swing.border.EmptyBorder;

import ihm.controleur.gestionCompte.ControleurAffichageExercice;
import ihm.controleur.gestionCompte.ControleurModifPassword;
import ihm.controleur.transitions.ControleurAllerVueBienvenue;
import ihm.modele.bd.Relation;
import ihm.modele.utilisateur.Compte;
import ihm.modele.utilisateur.Etape;
import ihm.modele.utilisateur.Exercice;

/**
 * Classe permettant de créer la vue de présentation
 * 
 * @author Mathis JOBARD
 * @version 0.1
 */
public class VueGestionCompte extends JFrame {

	/**
	 * SerialVersionUID
	 */
	private Compte modele;
	private static final long serialVersionUID = 1L;
	private JLabel panneauTitre;
	private JTextField textAncienmdp;
	private JTextField textNouveaumdp;
	private JTextField textNouveaumdp2;
	private JComboBox<Integer> choixExercices;
	private JPanel panelAffichageCentre;
	private JLabel labelCommence;
	private JLabel labelAttributs;
	private JLabel labelDependances;
	private JLabel labelCouvMin;
	private JScrollPane tableau;
	private JLabel labelRelation;
	private JLabel labelFermTrans;
	private JLabel labelFermEns;
	private JLabel labelCle;
	private JLabel labelFormNorm;
	private JLabel labelDecompo;
	private JLabel labelSynthese;
	private JLabel labelPertesDonnees;
	private JLabel labelPertesDep;

	/**
	 * Contructeur permettant de créer la vue
	 * 
	 * @param modele le modele
	 * @param exos les ids des exercices réalisés par l'utilisateur
	 */
	public VueGestionCompte(Compte modele, Integer[] exos) {
		super("Base de données - Bienvenue");

		this.modele = modele;
		
		JPanel contentPane = new JPanel();
		contentPane.setLayout(new BorderLayout());

		/*
		 * Panneau d'en-tête de la vue
		 */
		this.panneauTitre = new JLabel("Gestion du compte : " + this.modele.getIdentifiant(), SwingConstants.CENTER);
		// Ajout du panneau d'en-tête en haut de la vue
		contentPane.add(this.panneauTitre, BorderLayout.NORTH);

		/*
		 * Contenu de la vue
		 */
		JPanel panneau = new JPanel();
		panneau.setLayout(new BorderLayout());

		/*
		 * Panel de gestion du mot de passe
		 */
		JPanel modifPassword = new JPanel();
		modifPassword.setLayout(new GridLayout(1, 3));

		/*
		 * Panel ancien mot de passe
		 */
		JPanel ancienmdp = new JPanel();
		ancienmdp.setLayout(new GridLayout(4, 1));
		JLabel LabelAncienmdp = new JLabel("Ancien mot de passe", SwingConstants.CENTER);
		ancienmdp.add(LabelAncienmdp);
		textAncienmdp = new JPasswordField();
		ancienmdp.add(textAncienmdp);

		/*
		 * Panel nouveau mdp
		 */
		JPanel nouveaumdp = new JPanel();
		nouveaumdp.setLayout(new GridLayout(4, 1));
		JLabel LabelNouveaumdp = new JLabel("Nouveau mot de passe", SwingConstants.CENTER);
		nouveaumdp.add(LabelNouveaumdp);
		textNouveaumdp = new JPasswordField();
		nouveaumdp.add(textNouveaumdp);
		JLabel LabelNouveaumdp2 = new JLabel("Répéter nouveau mot de passe", SwingConstants.CENTER);
		nouveaumdp.add(LabelNouveaumdp2);
		textNouveaumdp2 = new JPasswordField();
		nouveaumdp.add(textNouveaumdp2);

		/*
		 * Panel bouton modif mdp
		 */
		JPanel PanelBoutonmdp = new JPanel();
		PanelBoutonmdp.add(new JPanel());
		PanelBoutonmdp.setLayout(new GridLayout(4, 1));
		JButton boutonModif = new JButton("Modifier");
		boutonModif.addActionListener(new ControleurModifPassword(this, modele));
		PanelBoutonmdp.add(boutonModif);

		// Ajout panel ancien et nouveaux mdp au panel gestion de mdp
		modifPassword.add(ancienmdp);
		modifPassword.add(nouveaumdp);
		modifPassword.add(PanelBoutonmdp);

		/*
		 * Panel affichage
		 */

		JPanel panelAffichage = new JPanel();
		panelAffichage.setLayout(new BorderLayout());

		/*
		 * Panel affichage --> Nord
		 */
		JPanel panelAffichageNord = new JPanel();
		panelAffichageNord.setLayout(new GridLayout(2, 1));
		JLabel labelExercice = new JLabel("Avancé des exercices");
		labelExercice.setBorder(new EmptyBorder(new Insets(0, 25, 0, 25)));
		Font policeTitre = new Font("arial", 20, 20);
		labelExercice.setFont(policeTitre);
		panelAffichageNord.add(labelExercice);
		choixExercices = new JComboBox<>(exos);
		this.choixExercices.addActionListener(new ControleurAffichageExercice(this, this.modele));
		panelAffichageNord.add(choixExercices);
		panelAffichage.add(panelAffichageNord, BorderLayout.NORTH);

		/*
		 * Panel affichage --> Sud
		 */
		this.panelAffichageCentre = new JPanel();
		this.panelAffichageCentre.setBorder(new EmptyBorder(new Insets(0, 25, 0, 25)));
		this.panelAffichageCentre.setLayout(new BoxLayout(this.panelAffichageCentre, BoxLayout.Y_AXIS));
		this.labelCommence = new JLabel("Commence avec:");
		panelAffichageCentre.add(labelCommence);
		this.labelAttributs = new JLabel("Attributs:");
		panelAffichageCentre.add(labelAttributs);
		this.labelDependances = new JLabel("Dependances:");
		panelAffichageCentre.add(labelDependances);
		JLabel labelStats = new JLabel("Statistiques de l'utilisateur");
		labelStats.setFont(policeTitre);
		panelAffichageCentre.add(labelStats);
		this.labelRelation = new JLabel("Nombre d'essais ratés pour la relation :");
		panelAffichageCentre.add(labelRelation);
		this.labelCouvMin = new JLabel("Nombre d'essais ratés pour la couverture minimale :");
		panelAffichageCentre.add(labelCouvMin);
		this.labelFermTrans = new JLabel("Nombre d'essais ratés pour la fermeture transitive :");
		panelAffichageCentre.add(labelFermTrans);
		this.labelFermEns = new JLabel("Nombre d'essais ratés pour la fermeture d'un ensemble :");
		panelAffichageCentre.add(labelFermEns);
		this.labelCle = new JLabel("Nombre d'essais ratés pour la clé :");
		panelAffichageCentre.add(labelCle);
		this.labelFormNorm = new JLabel("Nombre d'essais ratés pour la forme normale :");
		panelAffichageCentre.add(labelFormNorm);
		this.labelDecompo = new JLabel("Nombre d'essais ratés pour la décomposition :");
		panelAffichageCentre.add(labelDecompo);
		this.labelSynthese = new JLabel("Nombre d'essais ratés pour la synthèse :");
		panelAffichageCentre.add(labelSynthese);
		this.labelPertesDonnees = new JLabel("Nombre d'essais ratés pour la perte de données :");
		panelAffichageCentre.add(labelPertesDonnees);
		this.labelPertesDep = new JLabel("Nombre d'essais ratés pour la perte de dépendances :");
		panelAffichageCentre.add(labelPertesDep);

		// Ajout des panels Nord et Sud de l'affichage au panel de l'affichage

		panelAffichage.add(this.panelAffichageCentre, BorderLayout.CENTER);

		// Ajout panneau gestion de mdp et affichage au contenu de la vue
		panneau.add(modifPassword, BorderLayout.NORTH);
		panneau.add(panelAffichage, BorderLayout.CENTER);

		// Ajout panneau de contenu à la vue
		contentPane.add(new JScrollPane(panneau), BorderLayout.CENTER);

		/*
		 * Panneau footer de la vue
		 */
		JPanel footer = new JPanel();
		footer.setLayout(new BorderLayout());
		// Bouton quitter à gauche du footer
		JButton retour = new JButton("Retour");
		retour.addActionListener(new ControleurAllerVueBienvenue(modele, this));
		footer.add(retour, BorderLayout.WEST);
		// Ajout du panneau footer en bas de la vue
		contentPane.add(footer, BorderLayout.SOUTH);

		// Définition du contentPane
		this.setContentPane(contentPane);

		// Affichage de la vue
		this.pack();
		this.setSize(new Dimension(1024, 640));
		this.setExtendedState(Frame.MAXIMIZED_BOTH);
		this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		this.setVisible(true);
	}

	/**
	 * Méthode permettant de récupérer l'ancien password
	 * 
	 * @return l'ancien password de l'utilisateur
	 */
	public String donneAncienMdp() {
		return this.textAncienmdp.getText();
	}

	/**
	 * Méthode permettant de récupérer le nouveau password
	 * 
	 * @return le nouveau password de l'utilisateur
	 */
	public String donneNouveaumdp() {
		return this.textNouveaumdp.getText();
	}

	/**
	 * Méthode permettant de récupérer le nouveau password du 2ème champs de saisi
	 * 
	 * @return le nouveau password répété de l'utilisateur
	 */
	public String donneNouveaumdp2() {
		return this.textNouveaumdp2.getText();
	}

	/**
	 * Méthode permettant de récupérer le choix de l'exercice
	 * 
	 * 
	 * @return retourne le choix de l'exercice
	 */
	public int donneChoixExercice() {
		return (int) this.choixExercices.getSelectedItem();
	}

	/**
	 * Méthode permettant d'afficher une boite de dialogue d'erreur à l'utilisateur
	 * 
	 * @param titre   la titre de la boîte de dialogue
	 * @param message le message
	 */
	public void afficherMessageErreur(String titre, String message) {
		JOptionPane.showMessageDialog(null, message, titre, JOptionPane.ERROR_MESSAGE);
	}

	/**
	 * Méthode permettant d'afficher une boite de dialogue informative à
	 * l'utilisateur
	 * 
	 * @param titre   la titre de la boîte de dialogue
	 * @param message le message
	 */
	public void afficherMessageInformatif(String titre, String message) {
		JOptionPane.showMessageDialog(null, message, titre, JOptionPane.INFORMATION_MESSAGE);
	}

	public void miseAJourAffichageExo(Exercice exo, Relation r, Map<Etape, Integer> stats) {

		if (this.tableau != null)
			this.panelAffichageCentre.remove(this.tableau);
		
		if (exo.getTuples() != null) {
			this.labelCommence.setText("Commence avec : Tuples");
			
			// Création du tableau
			this.tableau = new JScrollPane(new JTable(exo.getTuples()));
			panelAffichageCentre.add(this.tableau, 0, 3);
		} else {
			this.labelCommence.setText("Commence avec : Relation");
		}

		this.labelAttributs.setText("Attributs : " + r.getAttributs());
		this.labelDependances.setText("Dependances : " + r.getGraphe());
		this.labelRelation.setText("Nombre d'essais ratés pour la relation : "+ stats.get(Etape.Relation));
		this.labelCouvMin.setText("Nombre d'essais ratés pour la couverture minimale : "+ stats.get(Etape.CouvMin));
		this.labelFermTrans.setText("Nombre d'essais ratés pour la fermeture transitive : "+ stats.get(Etape.FermTran));
		this.labelFermEns.setText("Nombre d'essais ratés pour la fermeture d'un ensemble : "+ stats.get(Etape.FermEns));
		this.labelCle.setText("Nombre d'essais ratés pour la clé : "+ stats.get(Etape.Cle));
		this.labelFormNorm.setText("Nombre d'essais ratés pour la forme normale : "+ stats.get(Etape.FormeNormale));
		this.labelDecompo.setText("Nombre d'essais ratés pour la décompostion : "+ stats.get(Etape.Decomposition));
		this.labelSynthese.setText("Nombre d'essais ratés pour la synthèse : "+ stats.get(Etape.Synthese));
		this.labelPertesDep.setText("Nombre d'essais ratés pour la perte de dépendances : "+ stats.get(Etape.PertesDependances));
		this.labelPertesDonnees.setText("Nombre d'essais ratés pour la perte de données : "+ stats.get(Etape.PertesDonnees));

	}

}