package ihm.vue;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;

import ihm.composants.ImagePanel;
import ihm.controleur.transitions.ControleurAllerGestionDuCompte;
import ihm.controleur.transitions.ControleurDebuter;
import ihm.controleur.transitions.ControleurQuitter;
import ihm.modele.utilisateur.Compte;

/**
 * Classe permettant de créer la vue de présentation
 * 
 * @author Mathis JOBARD
 * @version 0.1
 */
public class VueBienvenue extends JFrame {

	/**
	 * SerialVersionUID
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Contructeur permettant de créer la vue
	 * 
	 * @param modele le modèle
	 */
	public VueBienvenue(Compte modele) {
		super("Base de données - Bienvenue");

		JPanel contentPane = new JPanel();
		contentPane.setLayout(new BorderLayout());

		/*
		 * Panneau d'en-tête de la vue
		 */
		JPanel titre = new JPanel();
		titre.setLayout(new GridLayout(1, 1));
		titre.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		// Label de titre de la vue
		JLabel labelTitre = new JLabel("Bienvenue dans notre logiciel de gestion de base de données", SwingConstants.CENTER);
		Font policeTitre = new Font("arial", 18, 18);
		labelTitre.setFont(policeTitre);
		titre.add(labelTitre);
		// Ajout du panneau d'en-tête en haut de la vue
		contentPane.add(titre, BorderLayout.NORTH);

		/*
		 * Contenu de la vue
		 */
		contentPane.add(new ImagePanel("img/image_bienvenue.png"), BorderLayout.CENTER);

		/*
		 * Panneau footer de la vue
		 */
		JPanel footer = new JPanel();
		footer.setLayout(new BorderLayout());
		// Bouton commencer à droite du footer
		JButton authentification = new JButton("Débuter");
		authentification.addActionListener(new ControleurDebuter(modele, this));
		footer.add(authentification, BorderLayout.EAST);
		/*
		 * panel de boutons à gauche du footer
		 */
		JPanel panelGaucheFooter = new JPanel();
		panelGaucheFooter.setLayout(new GridLayout(1,2));
		// Bouton quitter à gauche du footer
		JButton quitter = new JButton("Quitter");
		quitter.addActionListener(new ControleurQuitter(this));
		panelGaucheFooter.add(quitter);
		if (modele != null) {
			JButton compte = new JButton("Gestion du compte");
			compte.addActionListener(new ControleurAllerGestionDuCompte(modele, this));
			panelGaucheFooter.add(compte);
		}
		//Ajout panel gauche footer à la vue du footer
		footer.add(panelGaucheFooter, BorderLayout.WEST);
		// Ajout du panneau footer en bas de la vue
		contentPane.add(footer, BorderLayout.SOUTH);

		// Définition du contentPane
		this.setContentPane(contentPane);

		// Affichage de la vue
		this.pack();
		// Centrer la fenêtre
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		this.setVisible(true);
	}

	/**
	 * Méthode permettant d'afficher une boite de dialogue à l'utilisateur
	 * 
	 * @param titre   la titre de la boîte de dialogue
	 * @param message le message
	 */
	public void afficherMessage(String titre, String message) {
		JOptionPane.showMessageDialog(null, message, titre, JOptionPane.ERROR_MESSAGE);
	}
}
