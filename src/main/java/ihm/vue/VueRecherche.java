package ihm.vue;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

import ihm.composants.PanneauTitre;
import ihm.composants.PanneauValidation;
import ihm.controleur.recherches.ControleurAjoutAttributCle;
import ihm.controleur.recherches.ControleurSuppressionAttributCle;
import ihm.controleur.recherches.ControleurVerificationCle;
import ihm.controleur.recherches.ControleurVerificationCouvMin;
import ihm.controleur.recherches.ControleurVerificationFermEns;
import ihm.controleur.recherches.ControleurVerificationFermTran;
import ihm.controleur.recherches.ControleurVerificationFormeNormale;
import ihm.controleur.transitions.ControleurAllerVueAlgorithmes;
import ihm.controleur.transitions.ControleurAllerVueSaisie;
import ihm.modele.bd.Attribut;
import ihm.modele.bd.FormeNormale;
import ihm.modele.utilisateur.Compte;

/**
 * Classe permettant de créer la vue des recherches préliminaires
 * 
 * @author Thibaut PICHON
 * @version 0.1
 */
public class VueRecherche extends JFrame {

	/**
	 * SerialVersionUID
	 */
	private static final long serialVersionUID = 1L;

	private Compte modele;

	private JTextPane couvMin;
	private JTextPane ensemblePourFermTran;
	private JTextPane fermTran;
	private JTextPane ensemblePourFermEns;

	private PanneauValidation validationFermTran;
	private PanneauValidation validationCouvMin;
	private PanneauValidation validationFermEns;
	private PanneauValidation validationCle;

	private JTextPane attsInitiauxFermEns;
	private JTextPane attsFermEns;

	private DefaultListModel<Attribut> attributsDispoCle;
	private JList<Attribut> jlistAttributsDispoCle;
	private DefaultListModel<Attribut> attributsCle;
	private JList<Attribut> jlistAttributsCle;

	JComboBox<FormeNormale> listeFormesNormales;
	private PanneauValidation validationFormeNormale;

	private JButton suivant;

	private PanneauTitre panneauTitre;

	/**
	 * Contructeur permettant de créer la vue
	 * 
	 * @param modele le modèle
	 */
	public VueRecherche(Compte modele) {
		super("Base de données - Recherches préliminaires");

		this.modele = modele;

		JPanel contentPane = new JPanel();
		contentPane.setLayout(new BorderLayout());
		contentPane.setBorder(BorderFactory.createEmptyBorder(10, 25, 10, 25));

		/*
		 * Panneau d'en-tête de la vue
		 */
		this.panneauTitre = new PanneauTitre("Recherches préliminaires", this.modele.getExerciceEnCours());
		// Ajout du panneau d'en-tête en haut de la vue
		contentPane.add(this.panneauTitre, BorderLayout.NORTH);

		/*
		 * Contenu de la vue
		 */

		JPanel panneau = new JPanel();
		panneau.setLayout(new GridLayout(5, 1));

		/*
		 * Couverture minimale
		 */
		JPanel panneauCouvMin = new JPanel();
		panneauCouvMin.setBorder(new LineBorder(Color.GRAY));
		panneauCouvMin.setLayout(new BorderLayout());
		panneauCouvMin.add(new JLabel("Couverture minimale de l'ensemble de dépendances de la relation", SwingConstants.CENTER),
				BorderLayout.NORTH);
		JPanel partiesCouvMin = new JPanel();
		panneauCouvMin.add(partiesCouvMin, BorderLayout.CENTER);
		partiesCouvMin.setLayout(new GridLayout(1, 2));
		partiesCouvMin.setBorder(new LineBorder(Color.GRAY));
		this.couvMin = new JTextPane();
		if (this.modele.getExerciceEnCours().getCouvMin() == null)
			this.couvMin.setText("{}");
		else
			this.couvMin.setText(this.modele.getExerciceEnCours().getCouvMin().toString());
		partiesCouvMin.add(this.couvMin);
		// Création panneau de validation de la couverture minimale
		this.validationCouvMin = new PanneauValidation(new ControleurVerificationCouvMin(this.modele, this));
		// Ajout panneau des gestionnaires de dépendances au panneau du contenu de la
		// vue
		partiesCouvMin.add(this.validationCouvMin);
		// Ajout panneau de la couverture minimale au panneau du contenu
		panneau.add(panneauCouvMin);

		/*
		 * Fermeture transitive d'un ensemble de dépendances fonctionnelles
		 */
		JPanel panneauFermetureTransitive = new JPanel();
		panneauFermetureTransitive.setBorder(new LineBorder(Color.GRAY));
		panneauFermetureTransitive.setLayout(new GridLayout(1, 2));

		// Panneau contenant les saisie des ensemble de dépendances
		JPanel panneauEnsembleFermeture = new JPanel();
		panneauEnsembleFermeture.setLayout(new GridLayout(2, 1));
		panneauFermetureTransitive.add(panneauEnsembleFermeture);

		JPanel partiesEnsemblePourFermTran = new JPanel();
		panneauEnsembleFermeture.add(partiesEnsemblePourFermTran);
		partiesEnsemblePourFermTran.setLayout(new BorderLayout());
		partiesEnsemblePourFermTran.setBorder(new LineBorder(Color.GRAY));
		partiesEnsemblePourFermTran.add(
				new JLabel("Ensemble de dépendances sur lequel réaliser la fermeture transitive"), BorderLayout.NORTH);
		this.ensemblePourFermTran = new JTextPane();
		this.ensemblePourFermTran.setText("{}");
		partiesEnsemblePourFermTran.add(this.ensemblePourFermTran, BorderLayout.CENTER);

		JPanel partiesFermTran = new JPanel();
		panneauEnsembleFermeture.add(partiesFermTran);
		partiesFermTran.setLayout(new BorderLayout());
		partiesFermTran.setBorder(new LineBorder(Color.GRAY));
		partiesFermTran.add(new JLabel("Fermeture transitive de l'ensemble de dépendances (réponse à vérifier)"),
				BorderLayout.NORTH);
		this.fermTran = new JTextPane();
		this.fermTran.setText("{}");
		partiesFermTran.add(this.fermTran);
		// Panneau de validation
		this.validationFermTran = new PanneauValidation(new ControleurVerificationFermTran(this.modele, this));
		// Ajout panneau des gestionnaires de dépendances au panneau du contenu de la
		// vue
		panneauFermetureTransitive.add(this.validationFermTran);
		// Ajout panneau fermeture transitive au panneau de contenu
		panneau.add(panneauFermetureTransitive);

		/*
		 * Panneau fermeture d'un ensemble
		 */
		JPanel panneauFermEns = new JPanel();
		panneauFermEns.setLayout(new GridLayout(1, 2));
		panneau.add(panneauFermEns);
		/*
		 * Panneau pour les éléments de saisie
		 */
		JPanel panneauSaisieFermEns = new JPanel();
		panneauSaisieFermEns.setBorder(new LineBorder(Color.GRAY));
		panneauSaisieFermEns.setLayout(new GridLayout(4, 1));
		panneauFermEns.add(panneauSaisieFermEns);
		/*
		 * Panneau pour l'ensemble de dépendances
		 */
		JPanel panneauEnsPourFermEns = new JPanel();
		panneauEnsPourFermEns.setLayout(new BorderLayout());
		panneauSaisieFermEns.add(panneauEnsPourFermEns);
		panneauEnsPourFermEns.add(
				new JLabel("Ensemble de dépendances pour la fermeture d'un ensemble d'attributs", SwingConstants.CENTER),
				BorderLayout.NORTH);
		this.ensemblePourFermEns = new JTextPane();
		this.ensemblePourFermEns.setText(this.modele.getExerciceEnCours().getRelation().getGraphe().toString());
		panneauEnsPourFermEns.add(this.ensemblePourFermEns, BorderLayout.CENTER);
		/*
		 * Panneau des attributs de la relation
		 */
		JPanel panneauAttsRelationFermEns = new JPanel();
		panneauAttsRelationFermEns.setLayout(new BorderLayout());
		panneauSaisieFermEns.add(panneauAttsRelationFermEns);
		panneauAttsRelationFermEns.add(new JLabel("Attributs de la relation", SwingConstants.CENTER), BorderLayout.NORTH);
		JTextPane attsRelation = new JTextPane();
		attsRelation.setText(this.modele.getExerciceEnCours().getRelation().getAttributs().toString().replace("[", "{")
				.replace("]", "}"));
		attsRelation.setEditable(false);
		panneauAttsRelationFermEns.add(attsRelation, BorderLayout.CENTER);
		/*
		 * Panneau ensemble d'attributs initiaux
		 */
		JPanel panneauAttsInitiauxFermEns = new JPanel();
		panneauAttsInitiauxFermEns.setLayout(new BorderLayout());
		panneauSaisieFermEns.add(panneauAttsInitiauxFermEns);
		panneauAttsInitiauxFermEns.add(new JLabel("Ensemble d'attributs", SwingConstants.CENTER), BorderLayout.NORTH);
		this.attsInitiauxFermEns = new JTextPane();
		this.attsInitiauxFermEns.setText("{}");
		panneauAttsInitiauxFermEns.add(this.attsInitiauxFermEns, BorderLayout.CENTER);
		/*
		 * Panneau fermeture de l'ensemble d'attributs
		 */
		JPanel panneauAttsFermEns = new JPanel();
		panneauAttsFermEns.setLayout(new BorderLayout());
		panneauSaisieFermEns.add(panneauAttsFermEns);
		panneauAttsFermEns.add(new JLabel("Fermeture de l'ensemble d'attributs (réponse à vérifier)", SwingConstants.CENTER),
				BorderLayout.NORTH);
		this.attsFermEns = new JTextPane();
		this.attsFermEns.setText("{}");
		panneauAttsFermEns.add(this.attsFermEns, BorderLayout.CENTER);
		/*
		 * Panneau de validation de la fermeture de l'ensemble
		 */
		this.validationFermEns = new PanneauValidation(new ControleurVerificationFermEns(this.modele, this));
		panneauFermEns.add(this.validationFermEns);

		/*
		 * Panneau pour la recherche de la clé
		 */
		JPanel panneauCle = new JPanel();
		panneauCle.setBorder(new LineBorder(Color.GRAY));
		panneauCle.setLayout(new GridLayout(1, 2));
		/*
		 * Panneau de JList
		 */
		JPanel panneauJListCle = new JPanel();
		panneauJListCle.setLayout(new GridLayout(2, 1));
		/*
		 * Panneau dispo
		 */
		JPanel panneauDispoCle = new JPanel();
		panneauDispoCle.setLayout(new BorderLayout());
		JLabel label = new JLabel("Attributs de la relation", SwingConstants.CENTER);
		label.setBorder(new EmptyBorder(0, 20, 0, 20));
		panneauDispoCle.add(label, BorderLayout.NORTH);
		// JList des attributs disponible
		this.attributsDispoCle = new DefaultListModel<>();
		this.jlistAttributsDispoCle = new JList<>(this.attributsDispoCle);
		this.jlistAttributsDispoCle.setToolTipText("Cliquer sur un attribut pour l'ajouter à la liste");
		this.jlistAttributsDispoCle.addListSelectionListener(new ControleurAjoutAttributCle(this.modele, this));
		panneauDispoCle.add(this.jlistAttributsDispoCle, BorderLayout.CENTER);
		panneauJListCle.add(panneauDispoCle);
		/*
		 * Panneau attributs de la clé
		 */
		JPanel panneauAttsCle = new JPanel();
		panneauAttsCle.setLayout(new BorderLayout());
		panneauAttsCle.add(new JLabel("Clé primaire (réponse à vérifier)", SwingConstants.CENTER), BorderLayout.NORTH);
		// JList des attributs de la clé
		this.attributsCle = new DefaultListModel<>();
		this.jlistAttributsCle = new JList<>(this.attributsCle);
		this.jlistAttributsCle.setToolTipText("Cliquer sur un attribut pour le retirer de la liste");
		this.jlistAttributsCle.addListSelectionListener(new ControleurSuppressionAttributCle(this.modele, this));
		panneauAttsCle.add(this.jlistAttributsCle, BorderLayout.CENTER);
		panneauJListCle.add(panneauAttsCle);
		// Chargement des données
		this.miseAJourSaisieCle();
		// Ajout panneau JList au panneau de la clé
		panneauCle.add(panneauJListCle);
		// Panneau de validation
		this.validationCle = new PanneauValidation(new ControleurVerificationCle(this.modele, this));
		// On supprime les bordure par défaut du panneau de validation
		this.validationCle.setBorder(new EmptyBorder(0, 0, 0, 0));
		panneauCle.add(this.validationCle);
		// Ajout panneau pour la clé à la vue
		panneau.add(panneauCle);
		/*
		 * Forme normale
		 */
		JPanel panneauFormeNormale = new JPanel();
		panneauFormeNormale.setBorder(new LineBorder(Color.GRAY));
		panneauFormeNormale.setLayout(new BorderLayout());
		panneauFormeNormale.add(new JLabel("Forme normale de la relation", SwingConstants.CENTER), BorderLayout.NORTH);
		JPanel partiesFormeNormale = new JPanel();
		panneauFormeNormale.add(partiesFormeNormale, BorderLayout.CENTER);
		partiesFormeNormale.setLayout(new GridLayout(1, 2));
		FormeNormale listeNF[] = { FormeNormale.NF1, FormeNormale.NF2, FormeNormale.NF3, FormeNormale.NFBC3 };
		this.listeFormesNormales = new JComboBox<>(listeNF);
		partiesFormeNormale.add(this.listeFormesNormales);
		// Création panneau de validation de la couverture minimale
		this.validationFormeNormale = new PanneauValidation(new ControleurVerificationFormeNormale(this.modele, this));
		// Ajout panneau des gestionnaires de dépendances au panneau du contenu de la
		// vue
		partiesFormeNormale.add(this.validationFormeNormale);
		// Ajout panneau de la couverture minimale au panneau du contenu
		panneau.add(panneauFormeNormale);

		// Ajout panneau de contenu à la vue
		contentPane.add(new JScrollPane(panneau), BorderLayout.CENTER);

		/*
		 * Pied de page de la vue
		 */
		JPanel piedPage = new JPanel();
		piedPage.setBorder(new EmptyBorder(10, 0, 0, 0));
		piedPage.setLayout(new BorderLayout());
		// Bouton précédent
		JButton precedent = new JButton("Saisie des données");
		precedent.addActionListener(new ControleurAllerVueSaisie(this.modele, this));
		piedPage.add(precedent, BorderLayout.WEST);
		// Bouton suivant
		this.suivant = new JButton("Algorithmes");
		if (this.modele.getExerciceEnCours().getCle().size() <= 0)
			this.suivant.setEnabled(false);
		this.suivant.addActionListener(new ControleurAllerVueAlgorithmes(this.modele, this));
		piedPage.add(this.suivant, BorderLayout.EAST);
		// Ajout du pied de page au contentPane
		contentPane.add(piedPage, BorderLayout.SOUTH);

		// Définition du contentPane de la vue
		this.setContentPane(contentPane);

		// Mise à jour des affichages
		this.miseAJourProgression();
		this.miseAJourSaisieCle();
		this.miseAJourAffichageValidationCle();

		// Affichage de la vue
		this.pack();
		this.setSize(new Dimension(1024, 640));
		this.setExtendedState(Frame.MAXIMIZED_BOTH);
		this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		this.setVisible(true);
	}

	/**
	 * Méthode permettant de mettre à jour la barre de progression
	 */
	public void miseAJourProgression() {
		this.panneauTitre.miseAJourProgression(this.modele.donneProgression());
	}

	/**
	 * Méthode permettant d'activer/désactiver le bouton suivant
	 * 
	 * @param etat vrai pour activer, faux sinon
	 */
	public void activerSuivant(boolean etat) {
		this.suivant.setEnabled(etat);
	}

	/*
	 * Couverture minimale
	 */

	/**
	 * Méthode permettant d'obtenir la couverture minimale saisie
	 * 
	 * @return String la couverture minimale saisie
	 */
	public String donneCouvMin() {
		return this.couvMin.getText();
	}

	/**
	 * Méthode permettant de corriger la saisie de la couverture minimale
	 * 
	 * @param correction la correction à afficher
	 */
	public void validerSaisieCouvMin(String correction) {
		String affichage = "Incorrect\n" + correction;
		this.validationCouvMin.miseAJourAffichageValidation(affichage);

		// Si la correction est un ensemble de dépendance
		if (correction.startsWith("Fmin = "))
			this.ensemblePourFermTran.setText(correction.replace("Fmin = ", ""));
	}

	/**
	 * Méthode permettant de d'afficher que la saisie de la couverture minimale est
	 * correct
	 */
	public void validerSaisieCouvMin() {
		this.validationCouvMin.miseAJourAffichageValidation("Correct");

		this.ensemblePourFermTran.setText(this.couvMin.getText());
	}

	/*
	 * Fermeture transitive
	 */

	/**
	 * Méthode permettant d'obtenir l'ensemble de dépendances saisie sur lequel
	 * réaliser la fermeture transtive
	 * 
	 * @return String l'ensemble de dépendances saisie sur lequel réaliser la
	 *         fermeture transtive
	 */
	public String donneEnsemblePourFermTran() {
		return this.ensemblePourFermTran.getText();
	}

	/**
	 * Méthode permettant d'obtenir la fermeture transitive saisie
	 * 
	 * @return String la fermeture transitive saisie
	 */
	public String donneFermTran() {
		return this.fermTran.getText();
	}

	/**
	 * Méthode permettant de corriger la saisie de la fermeture transitive
	 * 
	 * @param correction la correction à afficher
	 */
	public void validerSaisieFermTran(String correction) {
		String affichage = "Incorrect\n" + correction;
		this.validationFermTran.miseAJourAffichageValidation(affichage);

	}

	/**
	 * Méthode permettant de d'afficher que la saisie de la fermeture transitive est
	 * correct
	 */
	public void validerSaisieFermTran() {
		this.validationFermTran.miseAJourAffichageValidation("Correct");

	}

	/*
	 * Fermeture ensemble
	 */

	/**
	 * Méthode permettant de récupérer l'ensemble de dépendances à partir duquel
	 * réaliser la fermeture de l'ensemble d'attribut
	 * 
	 * @return l'ensemble de dépendances à partir duquel réaliser la fermeture de
	 *         l'ensemble d'attribut
	 */
	public String donneEnsemblePourFermetureEns() {
		return this.ensemblePourFermEns.getText();
	}

	/**
	 * Méthode permettant de récupérer l'ensemble d'attribut pour la fermeture de
	 * l'ensemble
	 * 
	 * @return l'ensemble d'attribut pour la fermeture de l'ensemble
	 */
	public String donneEnsembleAttsPourFermEns() {
		return this.attsInitiauxFermEns.getText();
	}

	/**
	 * Méthode permettant de récupérer la fermeture de l'ensemble d'attribut
	 * 
	 * @return la fermeture de l'ensemble d'attribut
	 */
	public String donneFermetureEns() {
		return this.attsFermEns.getText();
	}

	/**
	 * Méthode permettant de corriger la saisie de la fermeture d'un ensemble
	 * 
	 * @param correction la correction à afficher
	 */
	public void validerSaisieFermEns(String correction) {
		String affichage = "Incorrect\n" + correction;
		this.validationFermEns.miseAJourAffichageValidation(affichage);

	}

	/**
	 * Méthode permettant de d'afficher que la saisie de la fermeture d'un ensemble
	 * est correct
	 */
	public void validerSaisieFermEns() {
		this.validationFermEns.miseAJourAffichageValidation("Correct");

	}

	/*
	 * Clé
	 */

	/**
	 * Mise à jour de l'affichage de la saisie de la clé
	 */
	public void miseAJourSaisieCle() {
		this.attributsDispoCle.clear();

		for (Attribut att : this.modele.getExerciceEnCours().getRelation().getAttributs()) {
			this.attributsDispoCle.addElement(att);
		}

		this.attributsCle.clear();

		for (Attribut att : this.modele.getExerciceEnCours().getCle()) {
			this.attributsCle.addElement(att);
		}

	}

	/**
	 * Méthode permettant de récuper l'attribut dispo pour la clé sélectionné
	 * 
	 * @return l'attribut dispo pour la clé sélectionné
	 */
	public Attribut donneAttributDispoCleSelectionne() {
		return this.jlistAttributsDispoCle.getSelectedValue();
	}

	/**
	 * Méthode permettant de récuper l'attribut dans la clé sélectionné
	 * 
	 * @return l'attribut dispo dans la clé sélectionné
	 */
	public Attribut donneAttributCleSelectionne() {
		return this.jlistAttributsCle.getSelectedValue();
	}

	/**
	 * Méthode permettant de mettre à jour l'affichage de validation de la clé
	 */
	public void miseAJourAffichageValidationCle() {
		String affichage = "Cle = " + this.modele.getExerciceEnCours().getCle().toString();
		this.validationCle.miseAJourAffichageValidation(affichage.replace('[', '{').replace(']', '}'));
	}

	/**
	 * Méthode permettant de corriger la clé primaire
	 * 
	 * @param correction la correction à afficher
	 */
	public void validerSaisieCle(String correction) {
		String affichage = "Cle = " + this.modele.getExerciceEnCours().getCle().toString();
		affichage += "\n\nIncorrect\n" + correction;
		this.validationCle.miseAJourAffichageValidation(affichage.replace('[', '{').replace(']', '}'));

	}

	/**
	 * Méthode permettant de d'afficher que la clé primaire est correct
	 */
	public void validerSaisieCle() {
		String affichage = "Cle = " + this.modele.getExerciceEnCours().getCle().toString();
		affichage += "\n\nCorrect";
		this.validationCle.miseAJourAffichageValidation(affichage.replace('[', '{').replace(']', '}'));

	}

	/*
	 * Forme normale
	 */

	/**
	 * Méthode permettant de récupérer la forme normale sélectionnée
	 * 
	 * @return la forme normale sélectionnée
	 */
	public FormeNormale donneNF() {
		return (FormeNormale) this.listeFormesNormales.getSelectedItem();
	}

	/**
	 * Méthode permettant de corriger la forme normale saisie
	 * 
	 * @param correction la correction
	 */
	public void validerSaisieFormeNormale(String correction) {
		String affichage = "Incorrect\n" + correction;
		this.validationFormeNormale.miseAJourAffichageValidation(affichage);
	}

	/**
	 * Méthode permettant de valider la forme normale saisie
	 */
	public void validerSaisieFormeNormale() {
		this.validationFormeNormale.miseAJourAffichageValidation("Correct");
	}

	/**
	 * Méthode permettant d'afficher une boite de dialogue à l'utilisateur
	 * 
	 * @param titre   la titre de la boîte de dialogue
	 * @param message le message
	 */
	public void afficherMessage(String titre, String message) {
		JOptionPane.showMessageDialog(null, message, titre, JOptionPane.ERROR_MESSAGE);
	}
}
