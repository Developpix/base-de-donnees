package ihm.vue;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Frame;
import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextPane;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;

import ihm.controleur.saisies.ControleurCreerRelation;
import ihm.controleur.transitions.ControleurAllerVueChoix;
import ihm.modele.bd.Relation;
import ihm.modele.utilisateur.Compte;

/**
 * Classe permettant de créer la vue des recherches préliminaires
 * 
 * @author Thibaut PICHON
 * @version 0.1
 */
public class VueSaisieRelation extends JFrame {

	/**
	 * SerialVersionUID
	 */
	private static final long serialVersionUID = 1L;

	private Compte modele;

	private JProgressBar progressBar;

	private JTextPane attributsSaisis;

	private JTextPane ensembleDependances;

	private JButton suivant;

	/**
	 * Contructeur permettant de créer la vue
	 * 
	 * @param modele le modèle
	 */
	public VueSaisieRelation(Compte modele) {
		super("Base de données - Saisie des données");

		this.modele = modele;

		JPanel contentPane = new JPanel();
		contentPane.setLayout(new BorderLayout());

		/*
		 * Panneau d'en-tête de la vue
		 */
		JPanel titre = new JPanel();
		titre.setLayout(new GridLayout(2, 1));
		titre.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		// Label de titre de la vue
		JLabel labelTitre = new JLabel("Saisie des données", SwingConstants.CENTER);
		Font policeTitre = new Font("arial", 18, 18);
		labelTitre.setFont(policeTitre);
		titre.add(labelTitre);
		// Barre de progression
		progressBar = new JProgressBar();
		progressBar.setValue(0);
		titre.add(progressBar);
		// Ajout du panneau d'en-tête en haut de la vue
		contentPane.add(titre, BorderLayout.NORTH);

		/*
		 * Contenu de la vue
		 */
		JPanel contenu = new JPanel();
		contenu.setLayout(new GridLayout((modele.getExerciceEnCours().getTuples() != null ? 3 : 2), 1));
		/*
		 * Table
		 */
		if (this.modele.getExerciceEnCours().getTuples() != null) {
			JTable table = new JTable(this.modele.getExerciceEnCours().getTuples());
			contenu.add(new JScrollPane(table));
		}

		/*
		 * Attributs
		 */
		JPanel panneauAttributs = new JPanel();
		panneauAttributs.setLayout(new BorderLayout());
		// Titre
		panneauAttributs.add(new JLabel("Attributs de la relation"), BorderLayout.NORTH);
		// JTextPane
		this.attributsSaisis = new JTextPane();
		panneauAttributs.add(this.attributsSaisis, BorderLayout.CENTER);
		contenu.add(panneauAttributs);
		/*
		 * Panneau création des dépendances
		 */
		JPanel panneauDependances = new JPanel();
		panneauDependances.setLayout(new BorderLayout());
		// Titre
		panneauDependances.add(new JLabel("Dépendances de la relation"), BorderLayout.NORTH);
		// JTextPane
		this.ensembleDependances = new JTextPane();
		panneauDependances.add(this.ensembleDependances);
		contenu.add(panneauDependances);
		// Ajout contenu à la vue
		contentPane.add(contenu, BorderLayout.CENTER);

		/*
		 * Panneau pied de page de la vue
		 */
		JPanel footerVue = new JPanel();
		footerVue.setLayout(new BorderLayout());
		footerVue.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		// Bouton précédent
		JButton precedent = new JButton("Retour aux choix");
		precedent.addActionListener(new ControleurAllerVueChoix(this.modele, this));
		footerVue.add(precedent, BorderLayout.WEST);
		// Bouton suivant
		this.suivant = new JButton("Recherches prélimaires");
		this.suivant.addActionListener(new ControleurCreerRelation(this.modele, this));
		footerVue.add(this.suivant, BorderLayout.EAST);
		// Ajout du panneau d'en-tête en haut de la vue
		contentPane.add(footerVue, BorderLayout.SOUTH);

		// Définition du contentPane de la vue
		this.setContentPane(contentPane);

		// Mise à jour affichage
		this.miseAJourAffichage();

		// Affichage de la vue
		this.pack();
		this.setSize(new Dimension(1024, 640));
		this.setExtendedState(Frame.MAXIMIZED_BOTH);
		this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		this.setVisible(true);
	}

	/**
	 * Méthode permettant de changer la valeur de la barre de progression
	 * 
	 * @param valeur la nouvelle valeur de progression
	 */
	public void changerProgression(int valeur) {
		this.progressBar.setValue(valeur);
	}

	/**
	 * Méthode permettant de mettre à jour les attributs saisis
	 */
	public void miseAJourAffichage() {
		if (this.modele.getExerciceEnCours().getRelation() != null) {
			this.attributsSaisis.setText(this.modele.getExerciceEnCours().getRelation().getAttributs().toString()
					.replace("[", "<").replace("]", ">"));

			this.ensembleDependances.setText(this.modele.getExerciceEnCours().getRelation().getGraphe().toString());
		} else {
			this.attributsSaisis.setText("<>");
			this.ensembleDependances.setText("{}");
		}
	}

	/**
	 * Méthode permettant de récupérer les attributs saisis
	 * 
	 * @return les attributs saisis
	 */
	public String donneAttributs() {
		return this.attributsSaisis.getText();
	}

	/**
	 * Méthode permettant de récupérer l'ensemble de dépendances saisie
	 * 
	 * @return l'ensemble de dépendances saisie
	 */
	public String donneEnsembleDependances() {
		return this.ensembleDependances.getText();
	}

	/**
	 * Méthode permettant de définir la relation à afficher
	 * 
	 * @param r la relation
	 */
	public void definirRelation(Relation r) {
		this.attributsSaisis.setText(r.getAttributs().toString().replace("[", "<").replace("]", ">"));
		this.ensembleDependances.setText(r.getGraphe().toString());
	}

	/**
	 * Méthode permettant d'afficher une boite de dialogue à l'utilisateur
	 * 
	 * @param titre   la titre de la boîte de dialogue
	 * @param message le message
	 */
	public void afficherMessage(String titre, String message) {
		JOptionPane.showMessageDialog(null, message, titre, JOptionPane.ERROR_MESSAGE);
	}

}
