package ihm.vue;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.GridLayout;

import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

import ihm.composants.PanneauTitre;
import ihm.composants.PanneauValidation;
import ihm.controleur.algorithmes.ControleurAjoutRelationSynthese;
import ihm.controleur.algorithmes.ControleurSuppressionRelationSynthese;
import ihm.controleur.algorithmes.ControleurVerificationDecompo;
import ihm.controleur.algorithmes.ControleurVerifierSynthese;
import ihm.controleur.transitions.ControleurAllerVueRecherche;
import ihm.controleur.transitions.ControleurAllerVueUllman;
import ihm.modele.bd.Relation;
import ihm.modele.utilisateur.Compte;
import ihm.modele.utilisateur.Parseur;

/**
 * Classe permettant de créer la vue des algorithmes
 * 
 * @author Thibaut PICHON
 * @version 0.1
 */
public class VueAlgorithmes extends JFrame {

	/**
	 * SerialVersionUID
	 */
	private static final long serialVersionUID = 1L;

	private Compte modele;

	private PanneauTitre panneauTitre;

	private DefaultListModel<String> relationsSynthese;
	private JList<String> jlistRelationsSynthese;
	private JTextPane ensembleDependancesSynthese;
	private JTextPane attributsSaisisSynthese;

	private JTextField nomRelationSynthese;

	private PanneauValidation panneauValidationSynthese;

	private PanneauValidation validationDecompo;

	private DefaultComboBoxModel<String> listeDependances;

	private JTextPane attribusR1;

	private JTextPane dependancesR1;

	private JTextPane attribusR2;

	private JTextPane dependancesR2;

	private DefaultListModel<Relation> listeRelationsDecompo;

	/**
	 * Contructeur permettant de créer la vue
	 * 
	 * @param modele le modèle
	 */
	public VueAlgorithmes(Compte modele) {
		super("Base de données - Algorithmes");

		this.modele = modele;

		JPanel contentPane = new JPanel();
		contentPane.setLayout(new BorderLayout());

		/*
		 * Panneau d'en-tête de la vue
		 */
		this.panneauTitre = new PanneauTitre("Exécution des algorithmes", this.modele.getExerciceEnCours());
		// Ajout du panneau d'en-tête en haut de la vue
		contentPane.add(this.panneauTitre, BorderLayout.NORTH);

		/*
		 * Contenu de la vue
		 */
		JPanel contenu = new JPanel();
		contenu.setLayout(new GridLayout(2, 1));
		contentPane.add(new JScrollPane(contenu), BorderLayout.CENTER);

		/*
		 * Algo décomposition
		 */
		JPanel panneauDecomposition = new JPanel();
		panneauDecomposition.setLayout(new GridLayout(1, 2));
		contenu.add(panneauDecomposition);
		/*
		 * Panneau de saisie de l'algo de décomposition
		 */
		JPanel panneauAffichageSaisieDecompo = new JPanel();
		panneauDecomposition.add(panneauAffichageSaisieDecompo);
		panneauAffichageSaisieDecompo.setLayout(new BorderLayout());
		// Label décomposition
		panneauAffichageSaisieDecompo.add(new JLabel("Décomposition suivant l'algorithme de décomposition", SwingConstants.CENTER),
				BorderLayout.NORTH);
		// Panneau saisie
		JPanel panneauSaisieDecompo = new JPanel();
		panneauAffichageSaisieDecompo.add(panneauSaisieDecompo, BorderLayout.CENTER);
		panneauSaisieDecompo.setLayout(new BorderLayout());
		// Liste des dépendances
		this.listeDependances = new DefaultComboBoxModel<>();
		JComboBox<String> listeDeps = new JComboBox<>(this.listeDependances);
		panneauSaisieDecompo.add(listeDeps, BorderLayout.NORTH);
		/*
		 * Panneau des relations pour l'algo de décompo
		 */
		JPanel panneauRelationsDecompo = new JPanel();
		panneauRelationsDecompo.setLayout(new GridLayout(1, 2));
		panneauSaisieDecompo.add(panneauRelationsDecompo, BorderLayout.CENTER);
		/*
		 * Panneau R1
		 */
		JPanel panneauR1 = new JPanel();
		panneauRelationsDecompo.add(panneauR1);
		panneauR1.setLayout(new BorderLayout());
		panneauR1.add(new JLabel("R1", SwingConstants.CENTER), BorderLayout.NORTH);
		// Panneau saisie info R1
		JPanel panneauSaisieR1 = new JPanel();
		panneauSaisieR1.setBorder(new LineBorder(Color.GRAY));
		panneauR1.add(panneauSaisieR1);
		panneauSaisieR1.setLayout(new GridLayout(4, 1));
		panneauSaisieR1.add(new JLabel("Attributs"));
		this.attribusR1 = new JTextPane();
		this.attribusR1.setText("<>");
		panneauSaisieR1.add(this.attribusR1);
		panneauSaisieR1.add(new JLabel("Dépendances"));
		this.dependancesR1 = new JTextPane();
		panneauSaisieR1.add(this.dependancesR1);
		this.dependancesR1.setText("{}");
		/*
		 * Panneau R2
		 */
		JPanel panneauR2 = new JPanel();
		panneauRelationsDecompo.add(panneauR2);
		panneauR2.setLayout(new BorderLayout());
		panneauR2.add(new JLabel("R2", SwingConstants.CENTER), BorderLayout.NORTH);
		// Panneau saisie info R2
		JPanel panneauSaisieR2 = new JPanel();
		panneauR2.add(panneauSaisieR2);
		panneauSaisieR2.setBorder(new LineBorder(Color.GRAY));
		panneauSaisieR2.setLayout(new GridLayout(4, 1));
		panneauSaisieR2.add(new JLabel("Attributs"));
		this.attribusR2 = new JTextPane();
		this.attribusR2.setText("<>");
		panneauSaisieR2.add(this.attribusR2);
		panneauSaisieR2.add(new JLabel("Dépendances"));
		this.dependancesR2 = new JTextPane();
		this.dependancesR2.setText("{}");
		panneauSaisieR2.add(this.dependancesR2);
		/*
		 * Panneau liste relations
		 */
		this.listeRelationsDecompo = new DefaultListModel<Relation>();
		JList<Relation> jlistRelationsDecompo = new JList<>(this.listeRelationsDecompo);
		panneauSaisieDecompo.add(jlistRelationsDecompo, BorderLayout.SOUTH);
		/*
		 * Panneau de validation algo de décomposition
		 */
		this.validationDecompo = new PanneauValidation(new ControleurVerificationDecompo(this.modele, this));
		panneauDecomposition.add(this.validationDecompo);

		/*
		 * Algo synthèse
		 */
		JPanel panneauSynthese = new JPanel();
		panneauSynthese.setLayout(new GridLayout(1, 2));
		contenu.add(panneauSynthese);
		/*
		 * Panneau de saisie de l'algo de synthèse
		 */
		JPanel panneauSaisieSynthese = new JPanel();
		panneauSaisieSynthese.setLayout(new GridLayout(2, 1));
		panneauSynthese.add(panneauSaisieSynthese);
		/*
		 * Panneau d'ajout à la décompo suivant l'algo de synthèse
		 */
		JPanel panneauAjoutSynthese = new JPanel();
		panneauAjoutSynthese.setLayout(new BorderLayout());
		panneauSaisieSynthese.add(panneauAjoutSynthese);
		/*
		 * Panneau info sur la relation
		 */
		JPanel panneauInfoRelationSynthese = new JPanel();
		panneauInfoRelationSynthese.setLayout(new GridLayout(3, 1));
		panneauInfoRelationSynthese.add(new JLabel("Décomposition suivant l'algorithme de synthèse", SwingConstants.CENTER));
		panneauInfoRelationSynthese.add(new JLabel("Nom de la relation à ajouter / modifier"));
		this.nomRelationSynthese = new JTextField();
		panneauInfoRelationSynthese.add(this.nomRelationSynthese);
		panneauAjoutSynthese.add(panneauInfoRelationSynthese, BorderLayout.NORTH);
		/*
		 * Panneau saisi de relation
		 */
		JPanel panneauManipulationSynthese = new JPanel();
		panneauManipulationSynthese.setLayout(new GridLayout(2, 1));
		/*
		 * Attributs
		 */
		JPanel panneauAttributsSynthese = new JPanel();
		panneauAttributsSynthese.setLayout(new BorderLayout());
		// Titre
		panneauAttributsSynthese.add(new JLabel("Attributs de la relation"), BorderLayout.NORTH);
		// JTextPane
		this.attributsSaisisSynthese = new JTextPane();
		panneauAttributsSynthese.add(this.attributsSaisisSynthese, BorderLayout.CENTER);
		panneauManipulationSynthese.add(panneauAttributsSynthese);
		/*
		 * Panneau création des dépendances
		 */
		JPanel panneauDependancesSynthese = new JPanel();
		panneauDependancesSynthese.setLayout(new BorderLayout());
		// Titre
		panneauDependancesSynthese.add(new JLabel("Dépendances de la relation"), BorderLayout.NORTH);
		// JTextPane
		this.ensembleDependancesSynthese = new JTextPane();
		panneauDependancesSynthese.add(this.ensembleDependancesSynthese);
		panneauManipulationSynthese.add(panneauDependancesSynthese);
		// Ajout panneau manipulation au panneau de la décompo
		panneauAjoutSynthese.add(panneauManipulationSynthese, BorderLayout.CENTER);
		// Création du bouton ajouter
		JButton ajouterSynthese = new JButton("Ajouter / Modifier");
		ajouterSynthese.addActionListener(new ControleurAjoutRelationSynthese(this.modele, this));
		panneauAjoutSynthese.add(ajouterSynthese, BorderLayout.SOUTH);
		/*
		 * Panneau affichage synthèse
		 */
		JPanel panneauAffichageSynthese = new JPanel();
		panneauAffichageSynthese.setLayout(new BorderLayout());
		panneauSaisieSynthese.add(panneauAffichageSynthese);
		panneauAffichageSynthese.add(new JLabel("Liste des relations", SwingConstants.CENTER), BorderLayout.NORTH);
		// JList
		this.relationsSynthese = new DefaultListModel<>();
		this.jlistRelationsSynthese = new JList<>(this.relationsSynthese);
		panneauAffichageSynthese.add(this.jlistRelationsSynthese, BorderLayout.CENTER);
		// Création du bouton supprimer
		JButton supprimerRelationSynthese = new JButton("Supprimer");
		supprimerRelationSynthese.addActionListener(new ControleurSuppressionRelationSynthese(this.modele, this));
		panneauAffichageSynthese.add(supprimerRelationSynthese, BorderLayout.SOUTH);
		// Ajout panneau affichage au panneau de la décompo
		panneauSaisieSynthese.add(panneauAffichageSynthese);
		/*
		 * Panneau de validation
		 */
		this.panneauValidationSynthese = new PanneauValidation(new ControleurVerifierSynthese(this.modele, this));
		panneauSynthese.add(this.panneauValidationSynthese);

		/*
		 * Pied de page de la vue
		 */
		JPanel piedPage = new JPanel();
		piedPage.setBorder(new EmptyBorder(10, 0, 0, 0));
		piedPage.setLayout(new BorderLayout());
		// Bouton précédent
		JButton precedent = new JButton("Recherches préliminaires");
		precedent.addActionListener(new ControleurAllerVueRecherche(this.modele, this));
		piedPage.add(precedent, BorderLayout.WEST);
		// Bouton suivant
		JButton suivant = new JButton("Ullman");
		suivant.addActionListener(new ControleurAllerVueUllman(this.modele, this));
		piedPage.add(suivant, BorderLayout.EAST);
		// Ajout du pied de page au contentPane
		contentPane.add(piedPage, BorderLayout.SOUTH);

		// Définition du contentPane de la vue
		this.setContentPane(contentPane);

		// Mise à jour de l'affichage
		this.miseAJourProgression();
		this.miseAJourListeRelationsDecompo();
		this.miseAJourListeSynthese();

		// Affichage de la vue
		this.pack();
		this.setSize(new Dimension(1024, 640));
		this.setExtendedState(Frame.MAXIMIZED_BOTH);
		this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		this.setVisible(true);
	}

	/**
	 * Méthode permettant de mettre à jour la barre de progression
	 */
	public void miseAJourProgression() {
		this.panneauTitre.miseAJourProgression(this.modele.donneProgression());
	}

	/**
	 * Méthode permettant d'afficher une boite de dialogue à l'utilisateur
	 * 
	 * @param titre   la titre de la boîte de dialogue
	 * @param message le message
	 */
	public void afficherMessage(String titre, String message) {
		JOptionPane.showMessageDialog(null, message, titre, JOptionPane.ERROR_MESSAGE);
	}

	/*
	 * Méthodes pour l'algo de décomposition
	 */

	/**
	 * Méthode permettant de récupérer la dépendance pour la décomposition
	 * sélectionné
	 * 
	 * @return la dépendance sélectionné
	 */
	public String donnerDependanceDecompoSelectionne() {
		return (String) this.listeDependances.getSelectedItem();
	}

	/**
	 * Méthode permettant de récupérer les attributs de R1
	 * 
	 * @return les attributs de R1
	 */
	public String donnerAttributsR1() {
		return this.attribusR1.getText();
	}

	/**
	 * Méthode permettant de récupérer les dépendances de R2
	 * 
	 * @return les dépendances de R2
	 */
	public String donnerDependancesR1() {
		return this.dependancesR1.getText();
	}

	/**
	 * Méthode permettant de récupérer les attributs de R2
	 * 
	 * @return les attributs de R2
	 */
	public String donnerAttributsR2() {
		return this.attribusR2.getText();
	}

	/**
	 * Méthode permettant de récupérer les dépendances de R2
	 * 
	 * @return les dépendances de R2
	 */
	public String donnerDependancesR2() {
		return this.dependancesR2.getText();
	}

	/**
	 * Méthode permettant de mettre à jour la liste des relations résultat de la
	 * décomposition en suivant l'algorithme de décomposition
	 */
	public void miseAJourListeRelationsDecompo() {
		this.listeDependances.removeAllElements();
		this.listeRelationsDecompo.clear();

		for (Relation r : this.modele.getExerciceEnCours().getDecompoDecomposition()) {
			this.listeRelationsDecompo.addElement(r);
		}

		Relation r2 = this.modele.getExerciceEnCours().getRelationRestante();

		if (r2 != null) {
			try {
				for (String dep : Parseur.parserListeDependances(
						this.modele.getExerciceEnCours().getCouvMin().grapheInduit(r2.getAttributs()).toString())) {
					this.listeDependances.addElement(dep);
				}
			} catch (Exception e) {
				/* NOTHING */ }

			this.attribusR1.setText("<>");
			this.dependancesR1.setText("{}");

			this.attribusR2.setText(r2.getAttributs().toString().replace("[", "<").replace("]", ">"));
			this.dependancesR2.setText(r2.getGraphe().toString());
		} else {
			this.attribusR1.setText("<>");
			this.attribusR1.setEnabled(false);
			this.dependancesR1.setText("{}");
			this.dependancesR1.setEnabled(false);

			this.attribusR2.setText("<>");
			this.attribusR2.setEnabled(false);
			this.dependancesR2.setText("{}");
			this.dependancesR2.setEnabled(false);
		}
	}

	/**
	 * Méthode permettant de mettre à jour la validation de la décompositon
	 * 
	 * @param correction la correction
	 */
	public void miseAJourValidationDecompo(String correction) {
		this.validationDecompo.miseAJourAffichageValidation("Incorrect\n\n" + correction);
	}

	/**
	 * Méthode permettant de mettre à jour la validation de la décompositon
	 */
	public void miseAJourValidationDecompo() {
		this.validationDecompo.miseAJourAffichageValidation("Correct");
	}

	/*
	 * Méthodes pour l'algo de synthèse
	 */

	/**
	 * Méthode permettant de mettre à jour l'affichage de la liste des relations
	 * saisies pour l'algorithme de synthèse
	 */
	public void miseAJourListeSynthese() {
		int index = 0;
		if (this.jlistRelationsSynthese.getSelectedValue() != null) {
			index = this.jlistRelationsSynthese.getSelectedIndex();
		}

		this.relationsSynthese.clear();

		for (String nomRelation : this.modele.getExerciceEnCours().getDecompoSynthese().keySet()) {
			this.relationsSynthese.addElement(
					nomRelation + " " + this.modele.getExerciceEnCours().getDecompoSynthese().get(nomRelation));
		}

		if (this.relationsSynthese.getSize() > 0) {
			this.jlistRelationsSynthese.setSelectedIndex(
					(index >= this.relationsSynthese.getSize()) ? this.relationsSynthese.getSize() - 1 : index);
		}

		this.nomRelationSynthese.setText("");
		this.attributsSaisisSynthese.setText("<>");
		this.ensembleDependancesSynthese.setText("{}");
	}

	/**
	 * Méthode permettant de récupérer la relation (dans la liste de la
	 * décomposition suivant l'algorithme de synthèse) sélectionné
	 * 
	 * @return la relation sélectionnée
	 */
	public String donneRelationSyntheseSelectionne() {
		return this.jlistRelationsSynthese.getSelectedValue();
	}

	/**
	 * Méthode permettant de récupérer le nom de la relation saisi
	 * 
	 * @return le nom de la relation saisi
	 */
	public String donneNomRelationSynthese() {
		return this.nomRelationSynthese.getText();
	}

	/**
	 * Méthode permettant de récupérer l'ensemble d'attributs pour la relation saisi
	 * 
	 * @return l'ensemble d'attributs pour la relation saisi
	 */
	public String donneAttributsSynthese() {
		return this.attributsSaisisSynthese.getText();
	}

	/**
	 * Méthode permettant de récupérer l'ensemble de dépendances pour la relation
	 * saisi
	 * 
	 * @return l'ensemble de dépendances pour la relation saisi
	 */
	public String donneDependancesSynthese() {
		return this.ensembleDependancesSynthese.getText();
	}

	/**
	 * Méthode permettant de valider la saisie pour l'algorithme de synthèse
	 */
	public void validerSynthese() {
		this.panneauValidationSynthese.miseAJourAffichageValidation("Correct");
	}

	/**
	 * Méthode permettant de corriger la saisie pour l'algorithme de synthèse
	 * 
	 * @param correction la réponse correcte
	 */
	public void validerSynthese(String correction) {
		this.panneauValidationSynthese.miseAJourAffichageValidation("Incorrect\n" + correction);
	}
}
