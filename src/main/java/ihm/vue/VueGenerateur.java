package ihm.vue;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;

import ihm.controleur.generateur.ControleurDecrementeNbAtts;
import ihm.controleur.generateur.ControleurDecrementeNbDepsTuples;
import ihm.controleur.generateur.ControleurGenerer;
import ihm.controleur.generateur.ControleurIncrementeNbAtts;
import ihm.controleur.generateur.ControleurIncrementeNbDepsTuples;
import ihm.controleur.transitions.ControleurAllerVueChoix;
import ihm.modele.utilisateur.Compte;

/**
 * Classe permettant de créer la vue de présentation
 * 
 * @author Mathis JOBARD
 * @version 0.1
 */
public class VueGenerateur extends JFrame {

	/**
	 * SerialVersionUID
	 */
	private static final long serialVersionUID = 1L;

	private Compte modele;

	private int nbAtts;
	private int nbDepsTuples;

	private JLabel nombreAttributs;
	private JLabel nombreDepsTuples;

	/**
	 * Contructeur permettant de créer la vue
	 * 
	 * @param modele        le modèle
	 * @param genererTuples un bouléen pour dire si l'on génére des tuples ou non
	 */
	public VueGenerateur(Compte modele, boolean genererTuples) {
		super("Base de données - Bienvenue");

		this.modele = modele;
		this.nbAtts = 1;
		this.nbDepsTuples = 0;

		JPanel contentPane = new JPanel();
		contentPane.setLayout(new BorderLayout());

		/*
		 * Panneau d'en-tête de la vue
		 */
		JLabel labelTitre = new JLabel((genererTuples ? "Générateur de tuples" : "Générateur de relation"),
				SwingConstants.CENTER);
		Font policeTitre = new Font("arial", 18, 18);
		labelTitre.setFont(policeTitre);
		// Ajout du panneau d'en-tête en haut de la vue
		contentPane.add(labelTitre, BorderLayout.NORTH);

		/*
		 * Contenu de la vue
		 */
		JPanel panneau = new JPanel();
		panneau.setLayout(new GridLayout(2, 1));

		/*
		 * Panneau Attributs
		 */
		JPanel panneauAttributs = new JPanel();
		panneauAttributs.setLayout(new GridLayout(1, 4));
		JLabel labelNombreAttributs = new JLabel("Nombre d'attributs", SwingConstants.CENTER);
		panneauAttributs.add(labelNombreAttributs);
		JButton moins1 = new JButton("-");
		moins1.addActionListener(new ControleurDecrementeNbAtts(this));
		panneauAttributs.add(moins1);
		this.nombreAttributs = new JLabel(Integer.toString(this.nbAtts), SwingConstants.CENTER);
		panneauAttributs.add(nombreAttributs);
		JButton plus1 = new JButton("+");
		plus1.addActionListener(new ControleurIncrementeNbAtts(this));
		panneauAttributs.add(plus1);

		panneau.add(panneauAttributs);

		/*
		 * Panneau dépendances ou tuples
		 */

		JPanel panneauDepsTuples = new JPanel();
		panneauDepsTuples.setLayout(new GridLayout(1, 4));
		JLabel labelNombreTuples = new JLabel((genererTuples ? "Nombre de tuples" : "Nombre de dépendances"),
				SwingConstants.CENTER);
		panneauDepsTuples.add(labelNombreTuples);
		JButton moins2 = new JButton("-");
		moins2.addActionListener(new ControleurDecrementeNbDepsTuples(this, genererTuples));
		panneauDepsTuples.add(moins2);
		this.nombreDepsTuples = new JLabel(Integer.toString(this.nbDepsTuples), SwingConstants.CENTER);
		panneauDepsTuples.add(nombreDepsTuples);
		JButton plus2 = new JButton("+");
		plus2.addActionListener(new ControleurIncrementeNbDepsTuples(this, genererTuples));
		panneauDepsTuples.add(plus2);

		panneau.add(panneauDepsTuples);

		// Ajout panneau de contenu à la vue
		contentPane.add(new JScrollPane(panneau), BorderLayout.CENTER);

		/*
		 * Panneau footer de la vue
		 */
		JPanel footer = new JPanel();
		footer.setLayout(new BorderLayout());
		// Bouton générer à droite du footer
		JButton generer = new JButton("Générer");
		generer.addActionListener(new ControleurGenerer(this, this.modele, genererTuples));
		footer.add(generer, BorderLayout.EAST);
		// Bouton retour aux choix à gauche du footer
		JButton retourauxchoix = new JButton("Retour aux choix");
		retourauxchoix.addActionListener(new ControleurAllerVueChoix(this.modele, this));
		footer.add(retourauxchoix, BorderLayout.WEST);
		// Ajout du panneau footer en bas de la vue
		contentPane.add(footer, BorderLayout.SOUTH);

		// Définition du contentPane
		this.setContentPane(contentPane);

		// Affichage de la vue
		this.pack();
		// Centrer la fenêtre
		this.setLocationRelativeTo(null);
		this.setSize(new Dimension(720, 160));
		this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		this.setVisible(true);
	}

	/**
	 * Méthode permettant de récupérer le nombre d'attributs affichés
	 * 
	 * @return le nombre d'attributs saisies
	 */
	public int donneNbAtts() {
		return this.nbAtts;
	}

	/**
	 * Méthode permettant de récupérer le nombre de dépendances ou tuples affichés
	 * 
	 * @return le nombre de tuples saisies
	 */
	public int donneNbDepsTuples() {
		return this.nbDepsTuples;
	}

	/**
	 * Méthode permettant de définir le nombre d'attributs à affichés
	 * 
	 * @param nbAtts le nombre d'attributs
	 */
	public void setNbAtts(int nbAtts) {
		this.nbAtts = nbAtts;
		this.nombreAttributs.setText(Integer.toString(this.nbAtts));
	}

	/**
	 * Méthode permettant de définir le nombre de dépendances ou tuples à affichés
	 * 
	 * @param nbDepsTuples le nombre de dépendances ou tuples à affichés
	 */
	public void setNbDepsTuples(int nbDepsTuples) {
		this.nbDepsTuples = nbDepsTuples;
		this.nombreDepsTuples.setText(Integer.toString(this.nbDepsTuples));
	}
}