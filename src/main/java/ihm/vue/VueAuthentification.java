package ihm.vue;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;

import ihm.controleur.transitions.ControleurAllerVueBienvenue;
import ihm.controleur.transitions.ControleurConnexion;
import ihm.controleur.transitions.ControleurInscription;

/**
 * Classe permettant de créer la vue de présentation
 * 
 * @author Mathis JOBARD
 * @version 0.1
 */
public class VueAuthentification extends JFrame {

	/**
	 * SerialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	private JLabel panneauTitre;
	private JTextField completerLogin;
	private JTextField completerPassword;

	/**
	 * Contructeur permettant de créer la vue
	 */
	public VueAuthentification() {
		super("Base de données - Bienvenue");

		JPanel contentPane = new JPanel();
		contentPane.setLayout(new BorderLayout());

		/*
		 * Panneau d'en-tête de la vue
		 */
		this.panneauTitre = new JLabel("Connexion/Inscription", SwingConstants.CENTER);
		// Ajout du panneau d'en-tête en haut de la vue
		contentPane.add(this.panneauTitre, BorderLayout.NORTH);

		/*
		 * Contenu de la vue
		 */
		JPanel panneau = new JPanel();
		panneau.setLayout(new GridLayout(4, 1));
		JLabel login = new JLabel("Login", SwingConstants.CENTER);
		panneau.add(login);
		completerLogin = new JTextField();
		panneau.add(completerLogin);
		JLabel password = new JLabel("Password", SwingConstants.CENTER);
		panneau.add(password);
		completerPassword = new JPasswordField();
		panneau.add(completerPassword);

		// Ajout panneau de contenu à la vue

		contentPane.add(new JScrollPane(panneau), BorderLayout.CENTER);

		/*
		 * Panneau footer de la vue
		 */
		JPanel footer = new JPanel();
		footer.setLayout(new BorderLayout());
		// Bouton connexion à droite du footer
		JButton connexion = new JButton("Connexion");
		connexion.addActionListener(new ControleurConnexion(this));
		footer.add(connexion, BorderLayout.EAST);
		// Bouton inscription à droite du footer
		JButton inscription = new JButton("Inscription");
		inscription.addActionListener(new ControleurInscription(this));
		footer.add(inscription, BorderLayout.CENTER);
		// Bouton quitter à gauche du footer
		JButton retour = new JButton("Retour");
		retour.addActionListener(new ControleurAllerVueBienvenue(this));
		footer.add(retour, BorderLayout.WEST);
		// Ajout du panneau footer en bas de la vue
		contentPane.add(footer, BorderLayout.SOUTH);

		// Définition du contentPane
		this.setContentPane(contentPane);

		// Affichage de la vue
		this.pack();
		// Centrer la fenêtre
		this.setLocationRelativeTo(null);
		this.setSize(new Dimension(320, 192));
		this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		this.setVisible(true);
	}

	/**
	 * Méthode permettant de récupérer le login
	 * 
	 * @return le login de l'utilisateur
	 */
	public String donneLogin() {
		return this.completerLogin.getText();
	}

	/**
	 * Méthode permettant de récupérer le password
	 * 
	 * @return le password de l'utilisateur
	 */
	public String donnePassword() {
		return this.completerPassword.getText();
	}

	/**
	 * Méthode permettant d'afficher une boite de dialogue d'erreur à l'utilisateur
	 * 
	 * @param titre   la titre de la boîte de dialogue
	 * @param message le message
	 */
	public void afficherMessageErreur(String titre, String message) {
		JOptionPane.showMessageDialog(null, message, titre, JOptionPane.ERROR_MESSAGE);
	}

	/**
	 * Méthode permettant d'afficher une boite de dialogue informative à
	 * l'utilisateur
	 * 
	 * @param titre   la titre de la boîte de dialogue
	 * @param message le message
	 */
	public void afficherMessageInformatif(String titre, String message) {
		JOptionPane.showMessageDialog(null, message, titre, JOptionPane.INFORMATION_MESSAGE);
	}

}
