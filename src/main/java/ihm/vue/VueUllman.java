package ihm.vue;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

import ihm.composants.PanneauTitre;
import ihm.composants.PanneauValidation;
import ihm.controleur.transitions.ControleurAllerVueAlgorithmes;
import ihm.controleur.ullman.ControleurTerminerExercice;
import ihm.controleur.ullman.ControleurVerificationDependances;
import ihm.controleur.ullman.ControleurVerificationDonnees;
import ihm.modele.utilisateur.Compte;

/**
 * Classe permettant de créer la vue de Ullman
 * 
 * @author Mathis JOBARD
 * @version 0.1
 */
public class VueUllman extends JFrame {

	/**
	 * SerialVersionUID
	 */
	private static final long serialVersionUID = 1L;

	private Compte modele;

	private PanneauValidation validationDonnees;

	private PanneauValidation validationDependances;

	private JTextPane attributsSaisis;

	private JTextPane dependancesSaisis;

	private JTextPane attributsSaisisR1;

	private JTextPane dependancesSaisisR1;

	private JTextPane attributsSaisisR2;

	private JTextPane dependancesSaisisR2;

	private JComboBox<String> listeChoixValidationDependances;

	private JComboBox<String> listeChoixValidationDonnees;

	private PanneauTitre panneauTitre;

	/**
	 * Contructeur permettant de créer la vue
	 * 
	 * @param modele le modèle
	 */
	public VueUllman(Compte modele) {
		super("Base de données - Ullman");

		this.modele = modele;

		JPanel contentPane = new JPanel();
		contentPane.setLayout(new BorderLayout());

		/*
		 * Panneau d'en-tête de la vue
		 */
		this.panneauTitre = new PanneauTitre("Ullman", this.modele.getExerciceEnCours());
		// Ajout du panneau d'en-tête en haut de la vue
		contentPane.add(this.panneauTitre, BorderLayout.NORTH);

		/*
		 * Contenu de la vue
		 */
		JPanel panneau = new JPanel();
		panneau.setLayout(new GridLayout(4, 1));

		/*
		 * Création du panneau pour la relation principale
		 */
		JPanel panneauRelation = new JPanel();
		panneauRelation.setBorder(new LineBorder(Color.GRAY));
		panneauRelation.setLayout(new BorderLayout());

		// Ajout du label en haut du panneau pour la relation principale
		JLabel titrerelation = new JLabel("Relation Initial", SwingConstants.CENTER);
		panneauRelation.add(titrerelation, BorderLayout.NORTH);

		/*
		 * Création du panneau pour entrer les attributs et les dependances de la
		 * relation principale
		 */

		JPanel panneauInformationsRelation = new JPanel();
		panneauInformationsRelation.setLayout(new GridLayout(2, 2));

		// Ajout des JTextPane pour entrer les attributs et les dépendances avec des
		// labels au dessus
		JLabel attributsRelation = new JLabel("Attributs", SwingConstants.CENTER);
		panneauInformationsRelation.add(attributsRelation);
		JLabel dependancesRelation = new JLabel("Dependances", SwingConstants.CENTER);
		panneauInformationsRelation.add(dependancesRelation);
		this.attributsSaisis = new JTextPane();
		attributsSaisis.setText("<>");
		attributsSaisis.setBorder(new LineBorder(Color.GRAY));
		panneauInformationsRelation.add(this.attributsSaisis);
		this.dependancesSaisis = new JTextPane();
		dependancesSaisis.setText("{}");
		dependancesSaisis.setBorder(new LineBorder(Color.GRAY));
		panneauInformationsRelation.add(this.dependancesSaisis);

		// Ajout des sous panneaux au panneau de contenu
		panneauRelation.add(panneauInformationsRelation, BorderLayout.CENTER);
		panneau.add(panneauRelation);

		/*
		 * Création du panneau pour les relations R1 et R2
		 */

		JPanel panneauAutresRelations = new JPanel(new GridLayout(1, 2));

		/*
		 * Création du panneau R1
		 */

		JPanel panneauR1 = new JPanel();
		panneauR1.setLayout(new GridLayout(5, 1));
		// Ajout label de présentation de la relation R1
		JLabel labelR1 = new JLabel("R1", SwingConstants.CENTER);
		// Ajout des JTextPane pour entrer les attributs et les dépendances avec les
		// labels pour décrire
		JLabel labelAttributsR1 = new JLabel("Attributs");
		this.attributsSaisisR1 = new JTextPane();
		attributsSaisisR1.setText("<>");
		attributsSaisisR1.setBorder(new LineBorder(Color.GRAY));
		JLabel labelDependancesR1 = new JLabel("Dependances");
		this.dependancesSaisisR1 = new JTextPane();
		dependancesSaisisR1.setText("{}");
		dependancesSaisisR1.setBorder(new LineBorder(Color.GRAY));
		// Ajout des informations au panneau R1
		panneauR1.add(labelR1);
		panneauR1.add(labelAttributsR1);
		panneauR1.add(attributsSaisisR1);
		panneauR1.add(labelDependancesR1);
		panneauR1.add(dependancesSaisisR1);

		/*
		 * Création du panneau R2
		 */

		JPanel panneauR2 = new JPanel();
		panneauR2.setLayout(new GridLayout(5, 1));
		// Ajout label de présentation de la relation R2
		JLabel labelR2 = new JLabel("R2", SwingConstants.CENTER);
		// Ajout des JTextPane pour entrer les attributs et les dépendances avec les
		// labels pour décrire
		JLabel labelAttributsR2 = new JLabel("Attributs");
		this.attributsSaisisR2 = new JTextPane();
		attributsSaisisR2.setText("<>");
		attributsSaisisR2.setBorder(new LineBorder(Color.GRAY));
		JLabel labelDependancesR2 = new JLabel("Dependances");
		this.dependancesSaisisR2 = new JTextPane();
		dependancesSaisisR2.setText("{}");
		dependancesSaisisR2.setBorder(new LineBorder(Color.GRAY));
		// Ajout des informations au panneau R2
		panneauR2.add(labelR2);
		panneauR2.add(labelAttributsR2);
		panneauR2.add(attributsSaisisR2);
		panneauR2.add(labelDependancesR2);
		panneauR2.add(dependancesSaisisR2);

		// Ajout des sous panneaux au panneau de contenu
		panneauAutresRelations.add(panneauR1);
		panneauAutresRelations.add(panneauR2);
		panneau.add(panneauAutresRelations);

		/*
		 * Création du panneau de validation des données
		 */

		JPanel panneauValidationDonnees = new JPanel();
		panneauValidationDonnees.setBorder(new LineBorder(Color.GRAY));
		panneauValidationDonnees.setLayout(new GridLayout(1, 2));
		// Création du panneau de validation préconstruit
		this.validationDonnees = new PanneauValidation(new ControleurVerificationDonnees(this, modele));

		// Création du panneau de sélection si il y a perte ou non de données
		JPanel panneauSelectionDonnees = new JPanel();
		panneauSelectionDonnees.setLayout(new GridLayout(2, 1));
		JLabel labelVerifDonnees = new JLabel("Vérification des données");
		// Création de la liste déroulante
		String[] choixValidationDonnees = { "Pertes", "Pas Pertes" };
		this.listeChoixValidationDonnees = new JComboBox<>(choixValidationDonnees);
		this.listeChoixValidationDonnees.setSelectedIndex(0);

		// Ajout du label et de la liste déroulante au panneau de sélection
		panneauSelectionDonnees.add(labelVerifDonnees);
		panneauSelectionDonnees.add(this.listeChoixValidationDonnees);
		// Ajout panneau de sélection et panneau de validation au panneau global de
		// validation
		panneauValidationDonnees.add(panneauSelectionDonnees);
		panneauValidationDonnees.add(validationDonnees);
		// Ajout panneau global de validation au panneau du contenu de la vue
		panneau.add(panneauValidationDonnees);

		/*
		 * Création du panneau de validation des dépendances
		 */

		JPanel panneauValidationDependances = new JPanel();
		panneauValidationDependances.setBorder(new LineBorder(Color.GRAY));
		panneauValidationDependances.setLayout(new GridLayout(1, 2));
		// Création du panneau de validation préconstruit
		this.validationDependances = new PanneauValidation(new ControleurVerificationDependances(this, modele));

		// Création du panneau de sélection si il y a perte ou non de dépendances
		JPanel panneauSelectionDependances = new JPanel();
		panneauSelectionDependances.setLayout(new GridLayout(2, 1));
		JLabel labelVerifDependances = new JLabel("Vérification des dépendances");
		// Création de la liste déroulante
		String[] choixValidationDependances = { "Pertes", "Pas Pertes" };
		this.listeChoixValidationDependances = new JComboBox<>(choixValidationDependances);
		this.listeChoixValidationDependances.setSelectedIndex(0);

		// Ajout du label et de la liste déroulante au panneau de sélection
		panneauSelectionDependances.add(labelVerifDependances);
		panneauSelectionDependances.add(this.listeChoixValidationDependances);

		// Ajout panneau de sélection et panneau de validation au panneau global de
		// validation des dépendances
		panneauValidationDependances.add(panneauSelectionDependances);
		panneauValidationDependances.add(this.validationDependances);
		// Ajout panneau global de validation au panneau du contenu de la vue
		panneau.add(panneauValidationDependances);

		// Ajout panneau de contenu à la vue
		contentPane.add(new JScrollPane(panneau), BorderLayout.CENTER);

		/*
		 * Pied de page de la vue
		 */
		JPanel piedPage = new JPanel();
		piedPage.setBorder(new EmptyBorder(10, 0, 0, 0));
		piedPage.setLayout(new BorderLayout());
		// Bouton précédent
		JButton precedent = new JButton("Algorithmes");
		precedent.addActionListener(new ControleurAllerVueAlgorithmes(modele, this));
		piedPage.add(precedent, BorderLayout.WEST);
		// Bouton suivant
		JButton suivant = new JButton("Accueil");
		suivant.addActionListener(new ControleurTerminerExercice(this, this.modele));
		piedPage.add(suivant, BorderLayout.EAST);
		// Ajout du pied de page au contentPane
		contentPane.add(piedPage, BorderLayout.SOUTH);

		// Définition du contentPane de la vue
		this.setContentPane(contentPane);

		// Mise à jour des affichages
		this.miseAJourProgression();

		// Affichage de la vue
		this.pack();
		this.setSize(new Dimension(1024, 640));
		this.setExtendedState(Frame.MAXIMIZED_BOTH);
		this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		this.setVisible(true);
	}

	/**
	 * Méthode permettant de mettre à jour la barre de progression
	 */
	public void miseAJourProgression() {
		this.panneauTitre.miseAJourProgression(this.modele.donneProgression());
	}

	/**
	 * Méthode permettant de récupérer les attributs saisis dans la relation
	 * initiale
	 * 
	 * @return les attributs saisis
	 */
	public String donneAttributs() {
		return this.attributsSaisis.getText();
	}

	/**
	 * Méthode permettant de récupérer les attributs saisis pour la relation R1
	 * 
	 * @return les attributs saisis pour R1
	 */
	public String donneAttributsR1() {
		return this.attributsSaisisR1.getText();
	}

	/**
	 * Méthode permettant de récupérer les attributs saisis pour la relation R2
	 * 
	 * @return les attributs saisis pour R2
	 */
	public String donneAttributsR2() {
		return this.attributsSaisisR2.getText();
	}

	/**
	 * Méthode permettant de récupérer l'ensemble de dépendances saisie pour la
	 * relation initiale
	 * 
	 * @return l'ensemble de dépendances saisie
	 */
	public String donneEnsembleDependances() {
		return this.dependancesSaisis.getText();
	}

	/**
	 * Méthode permettant de récupérer l'ensemble de dépendances saisie pour la
	 * relation R1
	 * 
	 * @return l'ensemble de dépendances saisie pour R1
	 */
	public String donneEnsembleDependancesR1() {
		return this.dependancesSaisisR1.getText();
	}

	/**
	 * Méthode permettant de récupérer l'ensemble de dépendances saisie pour la
	 * relation R2
	 * 
	 * @return l'ensemble de dépendances saisie pour R2
	 */
	public String donneEnsembleDependancesR2() {
		return this.dependancesSaisisR2.getText();
	}

	/**
	 * Méthode permettant d'afficher une boite de dialogue à l'utilisateur
	 * 
	 * @param titre   la titre de la boîte de dialogue
	 * @param message le message
	 */
	public void afficherMessage(String titre, String message) {
		JOptionPane.showMessageDialog(null, message, titre, JOptionPane.ERROR_MESSAGE);
	}

	/**
	 * Méthode permettant de récupérer le choix de l'étudiant pour la perte de
	 * données
	 * 
	 * @return pertes ou pas pertes
	 */
	public String choixDonnees() {
		return (String) this.listeChoixValidationDonnees.getSelectedItem();
	}

	/**
	 * Méthode permettant de récupérer le choix de l'étudiant pour la perte de
	 * dépendances
	 * 
	 * @return pertes ou pas pertes
	 */
	public String choixDependances() {
		return (String) this.listeChoixValidationDependances.getSelectedItem();
	}

	/**
	 * Méthode permettant de corriger la réponse de l'utilisateur concernant la
	 * perte ou non de données
	 * 
	 * @param correction la correction
	 */
	public void validerSaisieDonnees(String correction) {
		String affichage = "Incorrect\n\n" + correction;
		this.validationDonnees.miseAJourAffichageValidation(affichage);
	}

	/**
	 * Méthode permettant de valider le choix de l'utilisateur concernant la perte
	 * ou non de données
	 */
	public void validerSaisieDonnees() {
		this.validationDonnees.miseAJourAffichageValidation("Correct");
	}

	/**
	 * Méthode permettant de corriger la réponse de l'utilisateur concernant la
	 * perte ou non de dépendances
	 * 
	 * @param correction la correction
	 */
	public void validerSaisieDependances(String correction) {
		String affichage = "Incorrect\n\n" + correction;
		this.validationDependances.miseAJourAffichageValidation(affichage);
	}

	/**
	 * Méthode permettant de valider le choix de l'utilisateur concernant la perte
	 * ou non de dépendances
	 */
	public void validerSaisieDependances() {
		this.validationDependances.miseAJourAffichageValidation("Correct");
	}
}
