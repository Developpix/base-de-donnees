package ihm.vue;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Frame;
import java.awt.GridLayout;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;

import ihm.controleur.saisies.ControleurAjoutAttribut;
import ihm.controleur.saisies.ControleurAjoutTuple;
import ihm.controleur.saisies.ControleurSuppressionAttribut;
import ihm.controleur.saisies.ControleurSuppressionTuple;
import ihm.controleur.transitions.ControleurAllerVueChoix;
import ihm.controleur.transitions.ControleurSaisirRelation;
import ihm.modele.bd.Attribut;
import ihm.modele.utilisateur.Compte;

/**
 * Classe permettant de créer la vue des recherches préliminaires
 * 
 * @author Thibaut PICHON
 * @version 0.1
 */
public class VueSaisieTuples extends JFrame {

	/**
	 * SerialVersionUID
	 */
	private static final long serialVersionUID = 1L;

	private Compte modele;

	private JProgressBar progressBar;

	private JButton suivant;

	private JTable tableau;

	private JTextField nomAttribut;
	private JComboBox<String> choixAttribut;

	private JButton supprimerTuples;

	private JPanel panelTuple;
	private Map<Attribut, JTextField> champsTuple;

	/**
	 * Contructeur permettant de créer la vue
	 * 
	 * @param modele le modèle
	 */
	public VueSaisieTuples(Compte modele) {
		super("Base de données - Saisie des données");

		this.modele = modele;
		this.champsTuple = new TreeMap<>();

		JPanel contentPane = new JPanel();
		contentPane.setLayout(new BorderLayout());

		/*
		 * Panneau d'en-tête de la vue
		 */
		JPanel titre = new JPanel();
		titre.setLayout(new GridLayout(2, 1));
		titre.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		// Label de titre de la vue
		JLabel labelTitre = new JLabel("Saisie des tuples", SwingConstants.CENTER);
		Font policeTitre = new Font("arial", 18, 18);
		labelTitre.setFont(policeTitre);
		titre.add(labelTitre);
		// Barre de progression
		progressBar = new JProgressBar();
		progressBar.setValue(0);
		titre.add(progressBar);
		// Ajout du panneau d'en-tête en haut de la vue
		contentPane.add(titre, BorderLayout.NORTH);

		/*
		 * Panel centre de la vue
		 */

		JPanel tab = new JPanel();
		tab.setLayout(new BorderLayout());

		// Création du tableau
		this.tableau = new JTable(this.modele.getExerciceEnCours().getTuples());
		tab.add(new JScrollPane(this.tableau), BorderLayout.CENTER);

		/*
		 * Panel regroupant les informations sur les tuples sous le tableau
		 */
		JPanel panelBas = new JPanel();
		panelBas.setLayout(new GridLayout(2, 1));
		this.panelTuple = new JPanel();
		panelBas.add(this.panelTuple);
		JButton ajoutTuple = new JButton("Ajouter tuple");
		ajoutTuple.addActionListener(new ControleurAjoutTuple(this, this.modele));
		panelBas.add(ajoutTuple);
		tab.add(panelBas, BorderLayout.SOUTH);

		/*
		 * Panel regroupant les informations à droite du tableau
		 */

		JPanel panelDroit = new JPanel();
		panelDroit.setLayout(new BorderLayout());

		/*
		 * Panel de gestion des attributs
		 */

		JPanel panelGestionAttribut = new JPanel();
		panelGestionAttribut.setLayout(new GridLayout(5, 1));

		JLabel labelAttribut = new JLabel("Nom attribut", SwingConstants.CENTER);
		panelGestionAttribut.add(labelAttribut);
		this.nomAttribut = new JTextField();
		panelGestionAttribut.add(nomAttribut);
		JButton ajoutAttribut = new JButton("Ajout Attribut");
		ajoutAttribut.addActionListener(new ControleurAjoutAttribut(this, this.modele));
		panelGestionAttribut.add(ajoutAttribut);
		this.choixAttribut = new JComboBox<>();
		panelGestionAttribut.add(choixAttribut);
		JButton supprimerAttribut = new JButton("Supprimer Attribut");
		supprimerAttribut.addActionListener(new ControleurSuppressionAttribut(this, this.modele));
		panelGestionAttribut.add(supprimerAttribut);

		// Ajout panneau gestion des attributs en haut du panneau droit
		panelDroit.add(panelGestionAttribut, BorderLayout.NORTH);

		this.supprimerTuples = new JButton("Supprimer tuples sélectionnées");
		// supprimerLignes.setEnabled(false);
		this.supprimerTuples.addActionListener(new ControleurSuppressionTuple(this, this.modele));
		// Ajout bouton supression ligne en bas du panneau droit
		panelDroit.add(supprimerTuples, BorderLayout.SOUTH);

		// Ajout panel tab au centre de la vue et panel droit à droite de la vue
		contentPane.add(tab, BorderLayout.CENTER);
		contentPane.add(panelDroit, BorderLayout.EAST);

		/*
		 * Panneau pied de page de la vue
		 */
		JPanel footerVue = new JPanel();
		footerVue.setLayout(new BorderLayout());
		footerVue.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		// Bouton précédent
		JButton precedent = new JButton("Retour aux choix");
		precedent.addActionListener(new ControleurAllerVueChoix(this.modele, this));
		footerVue.add(precedent, BorderLayout.WEST);
		// Bouton suivant
		this.suivant = new JButton("Saisie relation");
		this.suivant.addActionListener(new ControleurSaisirRelation(this, this.modele));
		this.suivant.setEnabled(false);
		footerVue.add(this.suivant, BorderLayout.EAST);
		// Ajout du panneau d'en-tête en haut de la vue
		contentPane.add(footerVue, BorderLayout.SOUTH);

		// Définition du contentPane de la vue
		this.setContentPane(contentPane);

		// Mise à jour des affichages
		this.miseAJourGestionAttributs();
		this.miseAJourGestionTuples();

		// Affichage de la vue
		this.pack();
		this.setSize(new Dimension(1024, 640));
		this.setExtendedState(Frame.MAXIMIZED_BOTH);
		this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		this.setVisible(true);
	}

	/**
	 * Méthode permettant de changer la valeur de la barre de progression
	 * 
	 * @param valeur la nouvelle valeur de progression
	 */
	public void changerProgression(int valeur) {
		this.progressBar.setValue(valeur);
	}

	/**
	 * Méthode permettant de récupérer le nom attribut saisi
	 * 
	 * @return le nom attribut saisi
	 */
	public String donneNomAttribut() {
		return this.nomAttribut.getText();
	}

	/**
	 * Méthode permettant de récupérer le nom de l'attribut sélectionné
	 * 
	 * @return le nom de l'attribut sélectionné
	 */
	public String donneAttribut() {
		return (String) this.choixAttribut.getSelectedItem();
	}

	/**
	 * Méthode permettant de mettre à jour l'affichage de gestion des attributs
	 */
	public void miseAJourGestionAttributs() {
		this.nomAttribut.setText("");

		this.choixAttribut.removeAllItems();

		for (Attribut att : this.modele.getExerciceEnCours().getTuples().getAttributs()) {
			this.choixAttribut.addItem(att.toString());
		}
	}

	/**
	 * Méthode permettant de récupérer les n° des tuples sélectionnés
	 * 
	 * @return les n° des tuples sélectionnés
	 */
	public int[] donnerTuplesSelectionnes() {
		return this.tableau.getSelectedRows();
	}

	/**
	 * Méthode permettant d'activer/désactiver le bouton de suppression des tuples
	 * 
	 * @param actif l'état du bouton
	 */
	public void activerSuppressionTuples(boolean actif) {
		this.supprimerTuples.setEnabled(actif);
	}

	/**
	 * Méthode permettant de récupérer le tuple saisi
	 * 
	 * @return le tuple saisi
	 */
	public Map<Attribut, String> donneTuple() {
		Map<Attribut, String> res = new TreeMap<>();

		for (Attribut att : this.champsTuple.keySet()) {
			res.put(att, this.champsTuple.get(att).getText());
		}

		return res;
	}

	/**
	 * Méthode permettant de mettre à jour la gestion des tuples
	 */
	public void miseAJourGestionTuples() {
		this.panelTuple.setLayout(new GridLayout(1, this.modele.getExerciceEnCours().getTuples().getColumnCount()));

		this.panelTuple.removeAll();
		this.champsTuple = new HashMap<>();

		for (Attribut att : this.modele.getExerciceEnCours().getTuples().getAttributs()) {
			JTextField textField = new JTextField();
			textField.setToolTipText("Champs pour l'attribut " + att);

			this.champsTuple.put(att, textField);
			this.panelTuple.add(textField);
		}
	}
	
	/**
	 * Méthode permettant d'activer/désactiver le bouton suivant
	 * 
	 * @param etat l'état du bouton suivant
	 */
	public void activerSuivant(boolean etat) {
		this.suivant.setEnabled(etat);
	}

	/**
	 * Méthode permettant d'afficher une boite de dialogue à l'utilisateur
	 * 
	 * @param titre   la titre de la boîte de dialogue
	 * @param message le message
	 */
	public void afficherMessage(String titre, String message) {
		JOptionPane.showMessageDialog(null, message, titre, JOptionPane.ERROR_MESSAGE);
	}
}
