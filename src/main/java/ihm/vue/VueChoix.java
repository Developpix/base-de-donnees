package ihm.vue;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Frame;
import java.awt.GridLayout;
import java.util.Set;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;

import ihm.controleur.choix.ControleurCommencerExercice;
import ihm.controleur.choix.ControleurGenerateur;
import ihm.controleur.choix.ControleurSaisirDonnes;
import ihm.controleur.choix.ControleurSelectionExercice;
import ihm.controleur.transitions.ControleurAllerVueBienvenue;
import ihm.modele.bd.Attribut;
import ihm.modele.utilisateur.Compte;
import ihm.modele.utilisateur.Exercice;

/**
 * Classe permettant de créer la vue de présentation
 * 
 * @author Mathis JOBARD
 * @version 0.1
 */
public class VueChoix extends JFrame {

	/**
	 * SerialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	private Compte modele;
	private JComboBox<Integer> choixExercices;
	private JLabel labelrealise;
	private JLabel labelCommence;
	private JLabel labelAttributs;
	private JLabel labelInfoDepTuples;
	private JButton commencer;
	private JComboBox<String> choix;

	/**
	 * Contructeur permettant de créer la vue
	 * 
	 * @param modele le modèle
	 * @param exos les ids des exercices présent dans la base de données
	 */
	public VueChoix(Compte modele, Integer[] exos) {
		super("Base de données - Bienvenue");

		this.modele = modele;

		JPanel contentPane = new JPanel();
		contentPane.setLayout(new BorderLayout());

		/*
		 * Panneau d'en-tête de la vue
		 */
		JLabel labelTitre = new JLabel("Choix de l'exercice", SwingConstants.CENTER);
		Font policeTitre = new Font("arial", 18, 18);
		labelTitre.setFont(policeTitre);
		// Ajout du panneau d'en-tête en haut de la vue
		contentPane.add(labelTitre, BorderLayout.NORTH);

		/*
		 * Contenu de la vue
		 */
		JPanel panneau = new JPanel();
		panneau.setLayout(new GridLayout(1, 2));

		/*
		 * Panneau de choix relation ou tuple
		 */
		JPanel panneauchoix = new JPanel();
		panneauchoix.setLayout(new GridLayout(10, 1));
		JLabel labelCommencer = new JLabel("Commencer avec", SwingConstants.CENTER);
		labelCommencer.setFont(policeTitre);
		panneauchoix.add(labelCommencer);
		String[] leschoix = { "Relation", "Tuples" };
		this.choix = new JComboBox<>(leschoix);
		panneauchoix.add(choix);

		panneau.add(panneauchoix);

		/*
		 * Panneau choix de l'exercice
		 */

		JPanel panneauchoixexercices = new JPanel();
		panneauchoixexercices.setLayout(new GridLayout(10, 1));
		JLabel labelExercice = new JLabel("Exercice", SwingConstants.CENTER);
		labelExercice.setFont(policeTitre);
		panneauchoixexercices.add(labelExercice);
		this.choixExercices = new JComboBox<>(exos);
		this.choixExercices.setSelectedIndex(-1);
		this.choixExercices.addActionListener(new ControleurSelectionExercice(this));
		panneauchoixexercices.add(choixExercices);
		this.labelrealise = new JLabel("Réalisé:", SwingConstants.CENTER);
		panneauchoixexercices.add(labelrealise);
		this.labelCommence = new JLabel("Commence avec:", SwingConstants.CENTER);
		panneauchoixexercices.add(labelCommence);
		this.labelAttributs = new JLabel("Attributs:", SwingConstants.CENTER);
		panneauchoixexercices.add(labelAttributs);
		this.labelInfoDepTuples = new JLabel("Nombre de tuples:", SwingConstants.CENTER);
		panneauchoixexercices.add(labelInfoDepTuples);

		panneau.add(panneauchoixexercices);
		// Ajout panneau de contenu à la vue

		contentPane.add(new JScrollPane(panneau), BorderLayout.CENTER);

		/*
		 * Panneau footer de la vue
		 */
		JPanel footer = new JPanel();
		footer.setLayout(new GridLayout(1, 4));
		// Bouton accueil à gauche du footer
		JButton retour = new JButton("Accueil");
		retour.addActionListener(new ControleurAllerVueBienvenue(this.modele, this));
		footer.add(retour);
		// Bouton saisirdonnées à droite du footer
		JButton saisirdonnees = new JButton("Saisir les données");
		saisirdonnees.addActionListener(new ControleurSaisirDonnes(this, this.modele));
		footer.add(saisirdonnees);
		// Bouton générateur à droite du footer
		JButton generateur = new JButton("Générateur");
		generateur.addActionListener(new ControleurGenerateur(this, this.modele));
		footer.add(generateur);
		// Bouton commencer à droite du footer
		this.commencer = new JButton("Commencer");
		commencer.setEnabled(false);
		commencer.addActionListener(new ControleurCommencerExercice(this, this.modele));
		footer.add(commencer);
		// Ajout du panneau footer en bas de la vue
		contentPane.add(footer, BorderLayout.SOUTH);

		// Définition du contentPane
		this.setContentPane(contentPane);

		// Affichage de la vue
		this.pack();
		this.setSize(new Dimension(1024, 640));
		this.setExtendedState(Frame.MAXIMIZED_BOTH);
		this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		this.setVisible(true);
	}

	/**
	 * Méthode permettant de récupérer le choix pour le générateur ou la saisie
	 * (Relation ou Tuples)
	 * 
	 * @return retourne le choix 'Relation' ou 'Tuples'
	 */
	public String donneChoixGenerateurSaisie() {
		return (String) this.choix.getSelectedItem();
	}

	/**
	 * Méthode permettant de récupérer l'exercice selectionné
	 * 
	 * @return l'exercice selectionné
	 */
	public int donneExerciceSelectionne() {
		return (int) this.choixExercices.getSelectedItem();
	}

	/**
	 * Méthode permettant de mettre à jour l'affichage de l'exercice selectionné
	 * 
	 * @param exo l'exercice à afficher
	 */
	public void miseAJourAffichageExo(Exercice exo) {
		this.labelrealise.setText("Réalisé : " + (this.modele.getExosRealises().values().contains(exo) ? "OUI" : "NON"));

		if (exo.getTuples() != null) {
			this.labelCommence.setText("Commence avec : Tuples");
			this.labelAttributs.setText("Attributs : " + exo.getTuples().getAttributs());
			this.labelInfoDepTuples.setText("Nombre de tuples : " + exo.getTuples().getRowCount());
		} else {
			this.labelCommence.setText("Commence avec : Relation");
			this.labelAttributs.setText("Attributs : " + exo.getRelation().getAttributs());

			int nbDep = 0;
			for (Set<Attribut> sg : exo.getRelation().getGraphe().donneCles()) {
				nbDep += exo.getRelation().getGraphe().successeurs(sg).size();
			}

			this.labelInfoDepTuples.setText("Nombre de dépendances : " + nbDep);
		}

		this.commencer.setEnabled(true);
	}

	/**
	 * Méthode permettant d'afficher une boite de dialogue d'erreur à l'utilisateur
	 * 
	 * @param titre   la titre de la boîte de dialogue
	 * @param message le message
	 */
	public void afficherMessageErreur(String titre, String message) {
		JOptionPane.showMessageDialog(null, message, titre, JOptionPane.ERROR_MESSAGE);
	}

	/**
	 * Méthode permettant d'afficher une boite de dialogue informative à
	 * l'utilisateur
	 * 
	 * @param titre   la titre de la boîte de dialogue
	 * @param message le message
	 */
	public void afficherMessageInformatif(String titre, String message) {
		JOptionPane.showMessageDialog(null, message, titre, JOptionPane.INFORMATION_MESSAGE);
	}

}
