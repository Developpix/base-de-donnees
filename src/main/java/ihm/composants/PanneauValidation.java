package ihm.composants;

import java.awt.BorderLayout;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextPane;

/**
 * Classe permettant de créer un panneau de validation
 * 
 * @author PICHON Thibaut
 * @version 1.0
 *
 */
public class PanneauValidation extends JPanel {

	/**
	 * SerialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	
	private JTextPane texteValidation;

	/**
	 * Contructeur de la classe
	 * 
	 * @param controleurVerifier contrôleur gérant les actions de vérification
	 */
	public PanneauValidation(ActionListener controleurVerifier) {
		super();
		
		// Panneau de validation
		this.setLayout(new BorderLayout());
		// Bouton vérifier
		JButton verifier = new JButton("Vérifier");
		verifier.addActionListener(controleurVerifier);
		this.add(verifier, BorderLayout.WEST);
		// Ajout d'un TextArea au centre
		this.texteValidation = new JTextPane();
		this.texteValidation.setEditable(false);
		this.add(this.texteValidation, BorderLayout.CENTER);
	}
	
	/**
	 * Méthode permettant de mettre à jour l'affichage du panneau de validation
	 * 
	 * @param affichage le texte à afficher
	 */
	public void miseAJourAffichageValidation(String affichage) {
		this.texteValidation.setText(affichage);
	}
	
}
