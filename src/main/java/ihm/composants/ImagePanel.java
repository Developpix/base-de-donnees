package ihm.composants;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

/**
 * Classe permettant d'afficher une image
 * 
 * @author PICHON Thibaut
 * @version 0.1
 *
 */
public class ImagePanel extends JPanel {

	private static final long serialVersionUID = 1L;
	
	private BufferedImage image;

	/**
	 * Contructeur permettant de créer une image
	 * 
	 * @param chemin le chemin de l'image
	 */
	public ImagePanel(String chemin) {
		super();
		
		try {
			image = ImageIO.read(new File(chemin));
			this.setPreferredSize(new Dimension(image.getWidth()+200, image.getHeight()+100));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		if (image != null)
			g.drawImage(image, 100, 50, null);
	}
	
	
}
