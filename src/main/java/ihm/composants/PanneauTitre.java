package ihm.composants;

import java.awt.Font;
import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JTextPane;
import javax.swing.SwingConstants;

import ihm.modele.utilisateur.Exercice;

/**
 * Classe permettant de créer un panneau de titre (composant pour les vues)
 * 
 * @author Thibaut PICHON
 * @version 0.1
 */
public class PanneauTitre extends JPanel {

	/**
	 * Serial version UID
	 */
	private static final long serialVersionUID = 1L;

	private JProgressBar progressBar;

	/**
	 * Constructeur de la classe
	 * 
	 * @param titre  le titre à afficher
	 * @param modele le modèle (un exercice)
	 */
	public PanneauTitre(String titre, Exercice modele) {
		super();
		/*
		 * Panneau d'en-tête de la vue
		 */
		this.setLayout(new GridLayout(3, 1));
		this.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		// Label de titre de la vue
		JLabel labelTitre = new JLabel(titre, SwingConstants.CENTER);
		Font policeTitre = new Font("arial", 18, 18);
		labelTitre.setFont(policeTitre);
		this.add(labelTitre);
		// Barre de progression
		progressBar = new JProgressBar();
		progressBar.setValue(50);
		this.add(progressBar);
		// Label de la relation
		JTextPane relationInitiale = new JTextPane();
		relationInitiale.setText("Relation saisie : R" + " "
				+ modele.getRelation().getAttributs().toString().replace("[", "<").replace("]", ">") + " "
				+ modele.getRelation().getGraphe().toString().replace("[", "<").replace("]", ">"));
		relationInitiale.setFont(policeTitre);
		relationInitiale.setEditable(false);
		this.add(relationInitiale);
	}

	/**
	 * Méthode permettant de mettre à jour la barre de progression
	 * 
	 * @param progression la progession de l'utilisateur
	 */
	public void miseAJourProgression(int progression) {
		this.progressBar.setValue(progression);
	}
}
