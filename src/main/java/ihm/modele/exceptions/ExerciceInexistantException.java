package ihm.modele.exceptions;

/**
 * Classe permettant de créer une exception signalant l'absence d'un exercice
 * dans la base de données
 * 
 * @author Thibaut PICHON
 * @version 0.1
 */
public class ExerciceInexistantException extends Exception {

	/**
	 * Serial version UID
	 */
	private static final long serialVersionUID = 1L;

	public ExerciceInexistantException() {
		super("Exercice non présent dans la base de données");
	}

	public ExerciceInexistantException(String message) {
		super(message);
	}

	public ExerciceInexistantException(Throwable cause) {
		super(cause);
	}

	public ExerciceInexistantException(String message, Throwable cause) {
		super(message, cause);
	}

	public ExerciceInexistantException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
