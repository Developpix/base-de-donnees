package ihm.modele.exceptions;

/**
 * Classe permettant de créer un exception informant qu'un ensemble d'attributs
 * est vide
 * 
 * @author PICHON Thibaut
 * @version 1.0.0
 */
public class EnsembleAttributsVideException extends Exception {

	/**
	 * SerialVersionUID
	 */
	private static final long serialVersionUID = 1L;

	public EnsembleAttributsVideException() {
		this("L'ensemble d'attributs contient aucun attributs");
	}

	public EnsembleAttributsVideException(String arg0) {
		super(arg0);
	}

	public EnsembleAttributsVideException(Throwable arg0) {
		super(arg0);
	}

	public EnsembleAttributsVideException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	public EnsembleAttributsVideException(String arg0, Throwable arg1, boolean arg2, boolean arg3) {
		super(arg0, arg1, arg2, arg3);
	}

}
