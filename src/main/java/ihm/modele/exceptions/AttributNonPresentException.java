package ihm.modele.exceptions;

/**
 * Classe permettant de créer une Exception lorque un attribut n'est pas présent
 * dans le Graphe
 * 
 * @author PICHON Thibaut
 * @version 1.0.0
 */
public class AttributNonPresentException extends Exception {

	/**
	 * SerialVersionUID
	 */
	private static final long serialVersionUID = 1L;

	public AttributNonPresentException() {
		this("Arc non présent");
	}

	public AttributNonPresentException(String arg0) {
		super(arg0);
	}

	public AttributNonPresentException(Throwable arg0) {
		super(arg0);
	}

	public AttributNonPresentException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	public AttributNonPresentException(String arg0, Throwable arg1, boolean arg2, boolean arg3) {
		super(arg0, arg1, arg2, arg3);
	}

}
