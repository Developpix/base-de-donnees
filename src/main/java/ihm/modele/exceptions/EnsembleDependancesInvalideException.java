package ihm.modele.exceptions;

/**
 * Classe permettant de créer un exception informant que l'ensemble de
 * dépendances saisie n'est pas valide
 * 
 * @author thibaut
 * @version 0.1
 */
public class EnsembleDependancesInvalideException extends Exception {

	/**
	 * SerialVersionUID
	 */
	private static final long serialVersionUID = 1L;

	public EnsembleDependancesInvalideException() {
		this("L'ensemble de dépendances saisi ne commence pas par { ou ne fini pas par }");
	}

	public EnsembleDependancesInvalideException(String message) {
		super(message);
	}

	public EnsembleDependancesInvalideException(Throwable cause) {
		super(cause);
	}

	public EnsembleDependancesInvalideException(String message, Throwable cause) {
		super(message, cause);
	}

	public EnsembleDependancesInvalideException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
