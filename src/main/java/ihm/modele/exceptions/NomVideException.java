package ihm.modele.exceptions;

/**
 * Classe permettant de créer une exception de nom vide
 * 
 * @author PICHON THibaut
 * @version 1.0.0
 * 
 */
public class NomVideException extends Exception {

	/**
	 * SerialVersionUID
	 */
	private static final long serialVersionUID = 1L;

	public NomVideException() {
		this("Le nom est vide");
	}

	public NomVideException(String arg0) {
		super(arg0);
	}

	public NomVideException(Throwable arg0) {
		super(arg0);
	}

	public NomVideException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	public NomVideException(String arg0, Throwable arg1, boolean arg2, boolean arg3) {
		super(arg0, arg1, arg2, arg3);
	}

}
