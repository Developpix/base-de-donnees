package ihm.modele.exceptions;

/**
 * Classe permettant de créer une Exception lorque la connexion avec la base de
 * données pose problème
 * 
 * @author PICHON Thibaut
 * @version 1.0.0
 */
public class ConnexionException extends Exception {

	/**
	 * SerialVersionUID
	 */
	private static final long serialVersionUID = 1L;

	public ConnexionException() {
		this("Impossible de se connecter à la base de données");
	}

	public ConnexionException(String arg0) {
		super(arg0);
	}

	public ConnexionException(Throwable arg0) {
		super(arg0);
	}

	public ConnexionException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	public ConnexionException(String arg0, Throwable arg1, boolean arg2, boolean arg3) {
		super(arg0, arg1, arg2, arg3);
	}

}
