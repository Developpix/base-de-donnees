package ihm.modele.exceptions;

/**
 * Classe permettant de créer une Exception lorqu'un compte existe déjà
 * 
 * @author PICHON Thibaut
 * @version 0.1
 */
public class CompteExistantException extends Exception {

	/**
	 * SerialVersionUID
	 */
	private static final long serialVersionUID = 1L;

	public CompteExistantException() {
		this("Le compte existe déjà dans la base de données");
	}

	public CompteExistantException(String arg0) {
		super(arg0);
	}

	public CompteExistantException(Throwable arg0) {
		super(arg0);
	}

	public CompteExistantException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	public CompteExistantException(String arg0, Throwable arg1, boolean arg2, boolean arg3) {
		super(arg0, arg1, arg2, arg3);
	}

}
