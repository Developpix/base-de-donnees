package ihm.modele.utilisateur;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Map;
import java.util.TreeSet;

import com.google.gson.Gson;
import com.google.gson.JsonParseException;

import ihm.modele.database.configuration.DatabaseConfiguration;
import ihm.modele.database.configuration.TableConfiguration;
import ihm.modele.database.sessions.SessionDatabase;
import ihm.modele.database.sessions.SessionMySQL;

/**
 * Classe (Factory) permettant de créer des objets à partir des fichiers de
 * configuration de l'application
 * 
 * @author Thibaut PICHON
 * @version 0.1
 */
public class ConfigurationLoader {

	/**
	 * Méthode permettant de récupérer une session avec une base de données à partir
	 * d'un fichier contenant la configuration de la base de données a utilisé
	 * 
	 * @param uriFichier l'URI du fichier
	 * 
	 * @return la session avec la base de données
	 * 
	 * @throws SQLException       exception lévée si un problème est rencontré lors
	 *                            de la connexion à la base de données
	 * @throws JsonParseException exception levée si la configuration dans le ficier
	 *                            JSON est mal décrite
	 * @throws IOException        exception levée si un problème de lecture du
	 *                            fichier est rencontré
	 */
	public static SessionDatabase chargerSessionDatabaseConfiguration(String uriFichier)
			throws SQLException, JsonParseException, IOException {
		try {
			String jsonTexte = "";

			// Initialisation d'un flux d'entrée utilisant un buffer pour lire le fichier
			BufferedReader br = new BufferedReader(new FileReader(uriFichier));

			// Lecture de la ligne suivante
			String line = br.readLine();

			/*
			 * Tant qu'il reste une ligne on ajoute la ligne dans la variable stockant le
			 * texte du json
			 */
			while (line != null) {
				jsonTexte += line + '\n';
				line = br.readLine();
			}

			// On ferme le flux de lecture
			br.close();

			// On initialise un objet Gson pour lire la configuration stockée dans le JSON
			Gson gson = new Gson();
			DatabaseConfiguration config = gson.fromJson(jsonTexte, DatabaseConfiguration.class);

			// On stocke la configuration de la connexion dans une map
			Map<String, String> connexionConfig = config.getConnexion();

			// On créer un session de type MySQL ou Oracle en fonction du SGBD utilisé
			if (connexionConfig.get("type").equals("mysql"))
				return new SessionMySQL(connexionConfig.get("host"), Integer.parseInt(connexionConfig.get("port")),
						connexionConfig.get("database"), connexionConfig.get("username"),
						connexionConfig.get("password"));
//			else if (connexionConfig.get("type").equals("oracle"))
//				return new SessionOracle(connexionConfig.get("host"), Integer.parseInt(connexionConfig.get("port")),
//						connexionConfig.get("database"), connexionConfig.get("username"),
//						connexionConfig.get("password"));
			/*
			 * Si le type de SGBD n'est pas supporté on lève une exception pour signalé à
			 * l'utilisateur d'utiliser un SGBD supporté par l'application
			 */
			else
				throw new JsonParseException(
						"Le type de base de données n'est pas supporté. Utilisez une base de données mysql ou oracle");
		} catch (NumberFormatException e) {
			throw new JsonParseException("Le port n'est pas un nombre");
		} catch (IOException e) {
			throw new IOException("Impossible de lire le fichier de configuration de la base de données");
		}
	}

	/**
	 * Méthode permettant de charger la config des tables
	 * 
	 * @param uriFichier l'URI du fichier de configuration
	 * 
	 * @return une map contenant la configuration des tables
	 * 
	 * @throws JsonParseException exception levée lorsque la syntaxe du JSON est
	 *                            invalide
	 * @throws IOException        exception levée lorsqu'un problème d'accès au
	 *                            fichier de configuration est rencontré
	 */
	public static Map<String, TableConfiguration> chargerConfigTables(String uriFichier)
			throws JsonParseException, IOException {
		try {
			String jsonTexte = "";

			// Initialisation d'un flux d'entrée utilisant un buffer pour lire le fichier
			BufferedReader br = new BufferedReader(new FileReader(uriFichier));

			// Lecture de la ligne suivante
			String line = br.readLine();

			/*
			 * Tant qu'il reste une ligne on ajoute la ligne dans la variable stockant le
			 * texte du json
			 */
			while (line != null) {
				jsonTexte += line + '\n';
				line = br.readLine();
			}

			// On ferme le flux de lecture
			br.close();

			// On initialise un objet Gson pour lire la configuration stockée dans le JSON
			Gson gson = new Gson();
			DatabaseConfiguration config = gson.fromJson(jsonTexte, DatabaseConfiguration.class);

			Map<String, TableConfiguration> confTables = config.getTables();

			// Vérification configuration de la table Comptes
			if (confTables.get("Comptes") == null) {
				throw new JsonParseException("La configuration de la table 'Comptes' est manquante");
			} else {
				TableConfiguration confComptes = confTables.get("Comptes");

				if (confComptes.getTable().isEmpty())
					throw new JsonParseException("Nom de la table 'Comptes' manquant");

				if (!confComptes.getAttributs().keySet()
						.equals(new TreeSet<>(Arrays.asList(new String[] { "id", "identifiant", "motDePasse" }))))
					throw new JsonParseException("Configuration d'un attribut de la table 'Comptes' manquant");
			}

			// Vérification configuration de la table Travail
			if (confTables.get("Travail") == null) {
				throw new JsonParseException("La configuration de la table 'Travail' est manquante");
			} else {
				TableConfiguration confTravail = confTables.get("Travail");

				if (confTravail.getTable().isEmpty())
					throw new JsonParseException("Nom de la table 'Travail' manquant");

				if (!confTravail.getAttributs().keySet()
						.equals(new TreeSet<>(Arrays.asList(new String[] { "id", "id_compte", "id_exercice", "relationSaisie",
								"relation", "couvMin", "fermTran", "fermEns", "cle", "formeNormale", "decomposition",
								"synthese", "perteDonnees", "perteDependances" }))))
					throw new JsonParseException("Configuration d'un attribut de la table 'Travail' manquant");
			}

			// Vérification configuration de la table Exercices
			if (confTables.get("Exercices") == null) {
				throw new JsonParseException("La configuration de la table 'Exercices' est manquante");
			} else {
				TableConfiguration confExos = confTables.get("Exercices");

				if (confExos.getTable().isEmpty())
					throw new JsonParseException("Nom de la table 'Exercices' manquant");

				if (!confExos.getAttributs().keySet()
						.equals(new TreeSet<>(Arrays.asList(new String[] { "id", "relation", "tuples" }))))
					throw new JsonParseException("Configuration d'un attribut de la table 'Exercices' manquant");
			}

			// On retourne la configuration des tables
			return config.getTables();
		} catch (IOException e) {
			throw new IOException("Impossible de lire le fichier de configuration de la base de données");
		}
	}

}
