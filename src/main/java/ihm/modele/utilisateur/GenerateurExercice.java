package ihm.modele.utilisateur;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import ihm.modele.bd.Attribut;
import ihm.modele.bd.Graphe;
import ihm.modele.bd.Relation;
import ihm.modele.bd.Table;
import ihm.modele.exceptions.AttributNonPresentException;
import ihm.modele.exceptions.EnsembleAttributsVideException;
import ihm.modele.exceptions.NomVideException;

/**
 * Classe (Factory) permettant de générer des relations ou tuples aléatoirement
 * pour l'exercice
 * 
 * @author Thibaut PICHON
 * @version 0.1
 */
public class GenerateurExercice {

	/**
	 * Méthode permettant de générer une relation aléatoirement
	 * 
	 * @param nbAttributs   le nombre d'attributs de la relation (inférieur à 50)
	 * @param nbDependances le nombre de dépendances de la relation (supérieur ou
	 *                      égal 0, égal à 0 si 1 seul attribut
	 * 
	 * @return la relation générée
	 * 
	 * @throws IllegalArgumentException exception levée si le nombre d'attributs
	 *                                  et/ou de dépendances est incorrect
	 */
	public static Relation genererRelation(int nbAttributs, int nbDependances) throws IllegalArgumentException {
		if (nbAttributs > 50)
			throw new IllegalArgumentException("Le nombre d'attribut doit-être inférieur à 50");

		if (nbAttributs < 1)
			throw new IllegalArgumentException("Le nombre d'attribut doit-être supérieur ou égal à 1");

		if (nbDependances < 0)
			throw new IllegalArgumentException("Le nombre de dépendances doit-être supérieur ou égal à 0");

		if (nbAttributs == 1 && nbDependances > 0)
			throw new IllegalArgumentException("Impossible de générer des dépendances avec un attribut");

		String lettres = "AZERTYUOPQSDFGHJKLMWXCVBNazertyuiopqsdfghjkmwxcvbn";
		Set<Attribut> attributs = new TreeSet<>();
		Set<Integer> choisis = new TreeSet<>();
		int l;

		for (int i = 0; i < nbAttributs; i++) {
			do {
				l = (int) (Math.random() * 50);
			} while (!choisis.add(l));

			try {
				attributs.add(new Attribut(Character.toString(lettres.charAt(l))));
			} catch (NomVideException e) {
				/* NOTHING */ }
		}

		Graphe g = new Graphe(attributs);
		List<Attribut> atts = new LinkedList<>(attributs);
		Set<Attribut> sg = new TreeSet<>();
		Set<Attribut> sd = new TreeSet<>();

		for (int dep = 0; dep < nbDependances; dep++) {
			sg.clear();
			sd.clear();
			choisis.clear();

			int nbGauches = (int) (Math.random() > 0.7 ? Math.random() * nbAttributs / 2 + 1
					: (Math.random() > 0.5 ? Math.random() * 2 + 1 : 1));
			int nbDroites = (int) (Math.random() > 0.7 ? Math.random() * nbAttributs / 2 + 1
					: (Math.random() > 0.5 ? Math.random() * 2 + 1 : 1));

			for (int nbAttsGauche = 0; nbAttsGauche < nbGauches; nbAttsGauche++) {
				do {
					l = (int) (Math.random() * atts.size());
				} while (!choisis.add(l));
				sg.add(atts.get(l));
			}

			choisis.clear();
			for (int nbAttsDroits = 0; nbAttsDroits < nbDroites; nbAttsDroits++) {
				do {
					l = (int) (Math.random() * (atts.size() - sg.size()));
				} while (!choisis.add(l));

				List<Attribut> dispoDroite = new LinkedList<>(attributs);
				dispoDroite.removeAll(sg);

				sd.add(dispoDroite.get(l));
			}

			try {
				g.ajouteArc(new TreeSet<>(sg), new TreeSet<>(sd));
			} catch (IllegalArgumentException | AttributNonPresentException | EnsembleAttributsVideException e) {
				/* NOTHING */
			}
		}

		return new Relation(g, attributs);
	}

	/**
	 * Méthode permettant de générer des tuples aléatoirement
	 * 
	 * @param nbAttributs le nombre d'attributs de la relation (inférieur à 50)
	 * @param nbTuples    le nombre de tuples (supérieur ou égal 0)
	 * 
	 * @return les tuples générés
	 * 
	 * @throws IllegalArgumentException exception levée si le nombre d'attributs
	 *                                  et/ou de dépendances est incorrect
	 */
	public static Table genererTuples(int nbAttributs, int nbTuples) throws IllegalArgumentException {
		if (nbAttributs > 50)
			throw new IllegalArgumentException("Le nombre d'attribut doit-être inférieur à 50");

		if (nbAttributs < 1)
			throw new IllegalArgumentException("Le nombre d'attribut doit-être supérieur ou égal à 1");

		if (nbTuples < 0)
			throw new IllegalArgumentException("Le nombre de tuples doit-être supérieur ou égal à 0");

		String lettres = "AZERTYUOPQSDFGHJKLMWXCVBNazertyuiopqsdfghjkmwxcvbn";
		Set<Attribut> attributs = new TreeSet<>();
		Set<Integer> choisis = new TreeSet<>();
		int l;

		for (int i = 0; i < nbAttributs; i++) {
			do {
				l = (int) (Math.random() * 50);
			} while (!choisis.add(l));

			try {
				attributs.add(new Attribut(Character.toString(lettres.charAt(l))));
			} catch (NomVideException e) {
				/* NOTHING */ }
		}

		Table tuples = new Table(attributs);

		Map<Attribut, String> tuple = new TreeMap<>();

		for (int t = 0; t < nbTuples; t++) {
			tuple.clear();

			for (Attribut att : attributs) {
				l = (int) (Math.random() * nbTuples * 3 / 4) + 1;
				tuple.put(att, att.toString() + l);
			}

			tuples.ajouterTuple(tuple);
		}

		return tuples;
	}

	/**
	 * Méthode permettant de calculer le nombre de dépendances possible
	 * 
	 * @param nbAtts le nombre d'attributs
	 * 
	 * @return le nombre de dépendances possibles
	 */
	public static long nombreDependancesPossibles(int nbAtts) {
		long res = 0;

		for (int k = 1; k < nbAtts; k++) {
			long res2 = 0;
			
			for (int j = 1; j <= nbAtts - k; j++) {
				if (nbAtts - k == j)
					res2++;
				else
					res2 += factoriel(nbAtts - k) / (factoriel(j) * factoriel(nbAtts - k - j));
			}

			res += (factoriel(nbAtts) / (factoriel(k) * factoriel(nbAtts - k))) * res2;
		}

		return res;
	}

	/**
	 * Méthode permettant de calculer la factoriel
	 * 
	 * @param val le nombre
	 * 
	 * @return la factoriel du nombre
	 */
	public static long factoriel(long val) {
		long res = 1;

		for (long i = val; i > 1; i--) {
			res *= i;
		}

		return res;
	}
}
