package ihm.modele.utilisateur;

import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

import com.google.common.hash.Hashing;

/**
 * Classe permettant de créer un compte utilisateur
 * 
 * @author Thibaut PICHON
 * @version 0.1
 */
public class Compte {

	private int id;
	private String identifiant;
	private String motDePasse;
	private Exercice enCours;
	private Map<Integer, Exercice> exosRealises;
	private Map<Etape, Integer> etapes;

	/**
	 * Constructeur de la classe
	 * 
	 * @param identifiant l'identifiant de l'utilisateur
	 * @param motDePasse  le mot de passe de l'utilisateur
	 * 
	 * @throws IllegalArgumentException exception levé si un des arguments est null,
	 *                                  si id est inférieur à 0, si l'identifiant
	 *                                  est une chaine vide ou si le mot de passe
	 *                                  est une chaine vide
	 */
	public Compte(String identifiant, String motDePasse) throws IllegalArgumentException {
		if (identifiant == null || identifiant.isEmpty())
			throw new IllegalArgumentException("L'identifiant ne doit pas être null ou vide");
		if (motDePasse == null || motDePasse.isEmpty())
			throw new IllegalArgumentException("Le mot de passe ne doit pas être null ou vide");
		this.id = -1;
		this.identifiant = identifiant;
		this.exosRealises = new HashMap<>();
		this.etapes = new HashMap<>();

		this.motDePasse = Hashing.sha512().hashString(motDePasse, StandardCharsets.UTF_8).toString();
	}

	/**
	 * Constructeur de la classe
	 * 
	 * @param id          l'id du compte dans le SGBD
	 * @param identifiant l'identifiant de l'utilisateur
	 * @param motDePasse  le mot de passe de l'utilisateur
	 * 
	 * @throws IllegalArgumentException exception levé si un des arguments est null,
	 *                                  si id est inférieur à 0, si l'identifiant
	 *                                  est une chaine vide ou si le mot de passe
	 *                                  est une chaine vide
	 */
	public Compte(int id, String identifiant, String motDePasse) throws IllegalArgumentException {
		this(identifiant, motDePasse);

		if (id < 0)
			throw new IllegalArgumentException("L'id doit être positif");

		this.id = id;
	}

	/**
	 * Modifieur permettant de modifier l'ID du compte
	 * 
	 * @param id l'ID du compte
	 * 
	 * @throws IllegalArgumentException exception levée si l'id est invalide
	 */
	public void setID(int id) throws IllegalArgumentException {
		if (id < 0)
			throw new IllegalArgumentException("L'id doit être positif");

		this.id = id;
	}

	/**
	 * Accesseur permettant de récupérer l'ID du compte
	 * 
	 * @return l'id du compte
	 */
	public int getID() {
		return this.id;
	}

	/**
	 * Accesseur permettant de récupérer l'identifiant du compte
	 * 
	 * @return l'identifiant
	 */
	public String getIdentifiant() {
		return this.identifiant;
	}

	/**
	 * Modifieur permettant de modifier l'identifiant du compte
	 * 
	 * @param nouvelleIdentifiant le nouvelle identifiant
	 * 
	 * @throws IllegalArgumentException exception levé si l'identifiant est une
	 *                                  chaine vide ou null
	 */
	public void setIdentifiant(String nouvelleIdentifiant) throws IllegalArgumentException {
		if (nouvelleIdentifiant == null || nouvelleIdentifiant.isEmpty())
			throw new IllegalArgumentException("L'identifiant ne doit pas être null ou vide");

		this.identifiant = nouvelleIdentifiant;
	}

	/**
	 * Accesseur permettant de récupérer le mot de passe hashé de l'utilisateur
	 * 
	 * @return le mot de passe hashé
	 */
	public String getMotDePasse() {
		return this.motDePasse;
	}

	/**
	 * Modifieur permettant de changer le mot de passe de l'utilisateur
	 * 
	 * @param nouveauMotDePase le nouveau mot de passe
	 * 
	 * @throws IllegalArgumentException exception levé si le mot de passe est une
	 *                                  chaine vide ou null
	 */
	public void setMotDePasse(String nouveauMotDePase) throws IllegalArgumentException {
		if (nouveauMotDePase == null || nouveauMotDePase.isEmpty())
			throw new IllegalArgumentException("Le mot de passe ne doit pas être null ou vide");

		this.motDePasse = Hashing.sha512().hashString(nouveauMotDePase, StandardCharsets.UTF_8).toString();
	}

	/**
	 * Accesseur permettant de récupérer l'exercice en cours de réalisation
	 * 
	 * @return l'exercice en cours de réalisation
	 */
	public Exercice getExerciceEnCours() {
		return this.enCours;
	}

	/**
	 * Modifieur permettant de définir l'exercice en cours de réalisation
	 * 
	 * @param exo l'exercice en cours de réalisation
	 */
	public void setExercice(Exercice exo) {
		this.enCours = exo;
	}

	/**
	 * Modifieur permettant de générer un nouvelle exercice
	 */
	public void nouvelleExercice() {
		this.enCours = new Exercice();
		this.etapes.clear();
	}

	/**
	 * Accesseurs permettant de récupérer la liste des exos réalisés par
	 * l'utilisateur
	 * 
	 * @return la liste des exercices réalisés
	 */
	public Map<Integer, Exercice> getExosRealises() {
		return this.exosRealises;
	}

	/**
	 * Modifieur permettant de définir la liste des exos réalisés par l'utilisateur
	 * 
	 * @param exos les exos réalisés
	 */
	public void setExosRealises(Map<Integer, Exercice> exos) {
		this.exosRealises = exos;
	}

	/**
	 * Méthode permettant de définir le résultat d'une étape
	 * 
	 * @param etape        l'étape
	 * @param nbTentatives le nombre de tentative
	 */
	public void definirEtatEtape(Etape etape, int nbTentatives) {
		this.etapes.put(etape, nbTentatives);
	}

	/**
	 * Méthode permettant de récupérer le nombre de tentative d'une étape
	 * 
	 * @param etape l'étape
	 * 
	 * @return le nombre de tentative
	 */
	public int donnerEtatEtape(Etape etape) {
		if (!this.etapes.containsKey(etape))
			return 0;
		
		return this.etapes.get(etape);
	}
	
	public Map<Etape, Integer> getEtapes() {
		return this.etapes;
	}

	/**
	 * Méthode permettant de récupérer la progression de l'utilisateur
	 * 
	 * @return la progression de l'utilisateur
	 */
	public int donneProgression() {
		return Math.floorDiv(this.etapes.values().size() * 100, 10);
	}
}
