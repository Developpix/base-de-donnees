package ihm.modele.utilisateur;

/**
 * Enum permettant de définir l'étape d'un exercice
 * 
 * @author Thibaut PICHON
 * @version 0.1
 */
public enum Etape {
	/**
	 * Etape de saisie de la relation
	 */
	Relation("relation"),

	/**
	 * Etape de la recherche de la couverture minimale
	 */
	CouvMin("couverture minimale"),

	/**
	 * Etape de recherche de la fermeture transitive
	 */
	FermTran("fermeture transitive"),

	/**
	 * Etape de recherche de la fermeture transitive d'un ensemble d'attributs
	 */
	FermEns("fermeture d'un ensemble d'attribut"),

	/**
	 * Etape de recherche de la clé
	 */
	Cle("clé"),

	/**
	 * Etape de recherche de la forme normale
	 */
	FormeNormale("forme normale"),

	/**
	 * Etape de la décomposition en suivant l'algorithme de décomposition
	 */
	Decomposition("décomposition"),

	/**
	 * Etape de la décomposition en suivant l'algorithme de synthèse
	 */
	Synthese("synthèse"),

	/**
	 * Etape de la vérification de pertes de données à l'aide du théorème d'Ullman
	 */
	PertesDonnees("pertes données"),

	/**
	 * Etape de la vérification de pertes de dépendances à l'aide du théorème
	 * d'Ullman
	 */
	PertesDependances("pertes dépendances");

	private String nom;

	Etape(String nom) {
		this.nom = nom;
	}

	@Override
	public String toString() {
		return this.nom;
	}
}
