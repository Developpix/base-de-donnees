package ihm.modele.utilisateur;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import ihm.modele.bd.Attribut;
import ihm.modele.bd.Graphe;
import ihm.modele.bd.Relation;
import ihm.modele.bd.Table;

/**
 * Classe permettant de stocker les données de l'utilisateur
 * 
 * @author PICHON Thibaut
 * @version 1.0
 */
public class Exercice {

	private int id;
	private Relation relation, relationRestante;
	private Map<String, Relation> decompoSynthese;
	private List<Relation> decompoDecomposition;
	private Set<Attribut> cle, ancienneCle;
	private Graphe couvMin;
	private Table tuples;

	/**
	 * Constructeur permettant de créer un exercice charger depuis la base de
	 * données
	 * 
	 * @param id l'id de l'exercice dans la base de données
	 * 
	 * @throws IllegalArgumentException exception levée si l'ID est invalide
	 */
	public Exercice(int id) throws IllegalArgumentException {
		if (id < 0)
			throw new IllegalArgumentException("L'ID doit être supérieur ou égal à 0");

		this.id = id;
	}

	/**
	 * Constructeur pour créer un exercice
	 */
	public Exercice() {
		this.id = -1;
	}

	/*
	 * Getters & Setters
	 */

	/**
	 * Accesseur permettant de récupérer l'id de l'exercice
	 * 
	 * @return l'id de l'exercice
	 */
	public int getID() {
		return this.id;
	}

	/**
	 * Modifieur permettant de définir l'id de l'exercice
	 * 
	 * @param id l'id de l'exercice
	 */
	public void setID(int id) {
		this.id = id;
	}

	/**
	 * Accesseur permettant de récupérer la relation de base
	 * 
	 * @return relation la relation de base
	 */
	public Relation getRelation() {
		return relation;
	}

	/**
	 * Modifieur permettant de définir la relation de base
	 * 
	 * @param relation la relation de base
	 */
	public void setRelation(Relation relation) {
		this.relation = relation;

		this.relationRestante = new Relation(new Graphe(relation.getGraphe()), new TreeSet<>(relation.getAttributs()));
		this.cle = new TreeSet<>();
		this.ancienneCle = null;
		this.couvMin = null;
		this.decompoSynthese = new HashMap<>();
		this.decompoDecomposition = new LinkedList<>();
	}

	/**
	 * Méthode permettant de sérializer la relation de l'exercice
	 * 
	 * @return une chaîne de caractères représentant la relation serializée au
	 *         format JSON
	 */
	public String serializerRelation() {
		// Si l'exercice ne possède pas de relation on renvoi une chaîne vide
		if (this.relation == null)
			return "";

		// Sinon on serialize la relation au format JSON
		GsonBuilder builder = new GsonBuilder();
		Gson gson = builder.enableComplexMapKeySerialization().create();
		return gson.toJson(this.relation);
	}

	/**
	 * Méthode permettant de desérializer la relation de l'exercice
	 * 
	 * @param json une chaîne de caractères représentant la relation serializée au
	 *             format JSON
	 */
	public void deserializerRelation(String json) {
		GsonBuilder builder = new GsonBuilder();
		Gson gson = builder.enableComplexMapKeySerialization().create();
		this.setRelation(gson.fromJson(json, Relation.class));
	}

	/**
	 * Accesseur permettant de récupérer la relation restante dans l'algo de
	 * décomposition
	 * 
	 * @return la relation restante dans l'algo de décomposition
	 */
	public Relation getRelationRestante() {
		return this.relationRestante;
	}

	/**
	 * Modifieur permettant de définir la relation restante dans l'algo de
	 * décomposition
	 * 
	 * @param relation la nouvelle relation restante
	 */
	public void setRelationRestante(Relation relation) {
		this.relationRestante = relation;
	}

	/**
	 * Accesseur permettant de récupérer les tuples de l'exercice
	 * 
	 * @return les tuples de l'exercices
	 */
	public Table getTuples() {
		return this.tuples;
	}

	/**
	 * Modifieur permettant de définir les tuples de l'exercice
	 * 
	 * @param tuples les tuples de l'exercice
	 */
	public void setTuples(Table tuples) {
		this.tuples = tuples;
	}

	/**
	 * Méthode permettant de sérializer les tuples de l'exercice
	 * 
	 * @return la chaîne de caractères représentant les tuples au format JSON
	 */
	public String serializerTuples() {
		// Si l'exercice ne possède pas de tuples on renvoi une chaîne vide
		if (this.tuples == null)
			return "";

		// Sinon on serialize les tuples au format JSON
		GsonBuilder builder = new GsonBuilder();
		Gson gson = builder.enableComplexMapKeySerialization().create();
		return gson.toJson(this.tuples);
	}

	/**
	 * Méthode permettant de déserializer les tuples de l'exerice
	 * 
	 * @param json la chaîne de caractères représentant les tuples au format JSON
	 */
	public void deserializerTuples(String json) {
		GsonBuilder builder = new GsonBuilder();
		Gson gson = builder.enableComplexMapKeySerialization().create();
		this.tuples = gson.fromJson(json, Table.class);
	}

	/**
	 * Acesseurs permettant de récupérer l'ensemble d'attributs de la clé
	 * 
	 * @return l'ensemble d'attributs de la clé
	 */
	public Set<Attribut> getCle() {
		return cle;
	}

	/**
	 * Modifieur permettant de modifier la clé
	 * 
	 * @param cle l'ensemble d'attributs de la clé
	 */
	public void setCle(Set<Attribut> cle) {
		this.ancienneCle = new HashSet<>(this.cle);
		this.cle = new HashSet<>(cle);
	}

	/**
	 * Méthode permettant de recharger l'ancienne clé si elle est présente
	 */
	public void reinitialiserCle() {
		if (this.ancienneCle != null) {
			this.cle = this.ancienneCle;
			this.ancienneCle = null;
		}
	}

	/**
	 * Accesseurs permettant de récupérer la couverture minimale saisie
	 * 
	 * @return la couverture minimale saisie par l'utilisateur
	 */
	public Graphe getCouvMin() {
		return this.couvMin;
	}

	/**
	 * Modifieur permettant de définir la couverture minimale
	 * 
	 * @param couvMin la couverture minimale
	 */
	public void setCouvMin(Graphe couvMin) {
		this.couvMin = couvMin;
	}

	/**
	 * Accesseur permettant de récupérer la liste des relations représentant la
	 * décomposition avec l'algo de synthèse entrée par l'utilisateur
	 * 
	 * @return la liste des relations représentant la décomposition avec l'algo de
	 *         synthèse entrée par l'utilisateur
	 */
	public Map<String, Relation> getDecompoSynthese() {
		return decompoSynthese;
	}

	/**
	 * Accesseur permettant de récupérer la liste des relations représentant la
	 * décomposition avec l'algo de décomposition entrée par l'utilisateur
	 * 
	 * @return la liste des relations représentant la décomposition avec l'algo de
	 *         décomposition entrée par l'utilisateur
	 */
	public List<Relation> getDecompoDecomposition() {
		return decompoDecomposition;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((relation == null) ? 0 : relation.hashCode());
		result = prime * result + ((tuples == null) ? 0 : tuples.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Exercice other = (Exercice) obj;
		if (relation == null) {
			if (other.relation != null)
				return false;
		} else if (!relation.equals(other.relation))
			return false;
		if (tuples == null) {
			if (other.tuples != null)
				return false;
		} else if (!tuples.equals(other.tuples))
			return false;
		return true;
	}
}
