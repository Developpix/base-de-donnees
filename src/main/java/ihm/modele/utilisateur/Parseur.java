package ihm.modele.utilisateur;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import ihm.modele.bd.Attribut;
import ihm.modele.bd.Graphe;
import ihm.modele.exceptions.AttributNonPresentException;
import ihm.modele.exceptions.EnsembleAttributsVideException;
import ihm.modele.exceptions.EnsembleDependancesInvalideException;
import ihm.modele.exceptions.NomVideException;

/**
 * Classe (Factory) permettant de parser des chaînes de caractères en Graphe ou
 * collection d'attribut
 * 
 * @author PICHON Thibaut
 * @version 0.1
 */
public class Parseur {

	/**
	 * Méthode nétoyant le nom d'un attribut transforme les suites d'espaces en un
	 * seul espace retire les retours à la ligne
	 * 
	 * @param chaine le nom de le/les attribut(s) à nettoyer
	 * 
	 * @return le nome de le/les attribut(s) nettoyé(s)
	 */
	public static String nettoyerNomAttribut(String chaine) {
		return chaine.trim().replace("  ", "").replace("\t", "").replace("\n", "");
	}

	/**
	 * Méthod convertissant une chaine de caractères en collection d'attribut
	 * 
	 * @param chaine la chaîne à convertir
	 * 
	 * @return la collection d'attributs
	 */
	public static Set<Attribut> convertirSetAttributs(String chaine) {
		Set<Attribut> res = new TreeSet<>();

		String chaineNetoyee = Parseur.nettoyerNomAttribut(chaine).replace("<", "").replace(">", "").replace("}", "")
				.replace("{", "").replace(";", "").replace("->", "");

		for (String nomAtt : Arrays.asList(chaineNetoyee.split(","))) {
			try {
				res.add(new Attribut(nomAtt));
			} catch (NomVideException e) {
				// Nothing
			}
		}

		return res;
	}

	/**
	 * Méthode permettant de convertir une chaîne représentant un ensemble de
	 * dépendance en liste de dépendances sous forme de liste de chaîne de
	 * caractères
	 * 
	 * @param chaine la chaîne de caractères représentant l'ensemble de dépenance
	 * 
	 * @return liste de dépendances sous forme de liste de chaîne de caractères
	 * 
	 * @throws EnsembleDependancesInvalideException exception levée si la chaine
	 *                                              représentant l'ensemble de
	 *                                              dépendances est invalide
	 */
	public static List<String> parserListeDependances(String chaine) throws EnsembleDependancesInvalideException {
		String chaineNetoyee = Parseur.nettoyerNomAttribut(chaine);

		if (chaineNetoyee.isEmpty())
			throw new EnsembleDependancesInvalideException("L'ensemble saisi ne commence pas par {");
		if (chaineNetoyee.charAt(0) != '{')
			throw new EnsembleDependancesInvalideException("L'ensemble saisi ne commence pas par {");
		if (chaineNetoyee.charAt(chaineNetoyee.length() - 1) != '}')
			throw new EnsembleDependancesInvalideException("L'ensemble saisi ne fini pas par }");

		chaineNetoyee = chaineNetoyee.replace("{", "").replace("}", "").replace(";}", "").replace("; ", ";")
				.replace(" ;", ";").replace(", ", ",").replace(" ,", ",").replace(" -", "-").replace("- ", "-")
				.replace("> ", ">").replace("->", " -> ").replace(",", ", ");

		return Arrays.asList(chaineNetoyee.split(";"));
	}

	/**
	 * Méthode permettant de convertir une chaîne représentant une dépendance en une
	 * liste contenant de deux liste d'attributs représentant la dépendance
	 * 
	 * @param chaine la chaîne de caractères représentant une dépendance
	 * 
	 * @return une liste contenant deux liste d'attributs représentant la dépendance
	 * 
	 * @throws EnsembleDependancesInvalideException exception levée si la chaine
	 *                                              représentant l'ensemble de
	 *                                              dépendances est invalide
	 */
	public static List<Set<Attribut>> parserDependance(String chaine) throws EnsembleDependancesInvalideException {
		List<Set<Attribut>> resultat = new ArrayList<>(2);

		String chaineNetoyee = Parseur.nettoyerNomAttribut(chaine);

		if (chaineNetoyee.isEmpty())
			throw new EnsembleDependancesInvalideException("La dépendance est vide");

		chaineNetoyee = chaineNetoyee.replace("{", "").replace("}", "").replace(";}", "").replace("; ", ";")
				.replace(" ;", ";").replace(", ", ",").replace(" ,", ",").replace(" -", "-").replace("- ", "-")
				.replace("> ", ">");

		String parts[] = chaineNetoyee.split("->");

		resultat.add(Parseur.convertirSetAttributs(parts[0]));
		resultat.add(Parseur.convertirSetAttributs(parts[1]));

		return resultat;
	}

	/**
	 * Méthode permettant de convertir une chaîne en un Graphe représentant
	 * l'ensemble de dépendances saisie
	 * 
	 * @param attributsDispo les attributs disponible pour l'ensemble de dépendances
	 * @param chaine         la chaîne à convertir
	 *
	 * @throws AttributNonPresentException          exception lévée si l'utilisateur
	 *                                              à entrée un attribut non
	 *                                              disponible
	 * @throws EnsembleDependancesInvalideException xception lévée si l'utilisateur
	 *                                              à entrée un ensemble de
	 *                                              dépendances invalide
	 * 
	 * @return le Graphe représentant l'ensemble de dépendance
	 */
	public static Graphe convertirEnGraphe(Set<Attribut> attributsDispo, String chaine)
			throws AttributNonPresentException, EnsembleDependancesInvalideException {

		Graphe g = new Graphe(attributsDispo);

		String chaineNetoyee = Parseur.nettoyerNomAttribut(chaine);

		if (chaineNetoyee.isEmpty())
			throw new EnsembleDependancesInvalideException("L'ensemble saisi ne commence pas par {");
		if (chaineNetoyee.charAt(0) != '{')
			throw new EnsembleDependancesInvalideException("L'ensemble saisi ne commence pas par {");
		if (chaineNetoyee.charAt(chaineNetoyee.length() - 1) != '}')
			throw new EnsembleDependancesInvalideException("L'ensemble saisi ne fini pas par }");

		chaineNetoyee = chaineNetoyee.replace("{", "").replace("}", "").replace(";}", "").replace("; ", ";")
				.replace(" ;", ";").replace(", ", ",").replace(" ,", ",").replace(" -", "-").replace("- ", "-")
				.replace("> ", ">");

		if (!chaineNetoyee.isEmpty()) {

			List<String> listeDep = Arrays.asList(chaineNetoyee.split(";"));

			for (String dep : listeDep) {
				String[] parties = dep.split("->");

				if (parties.length < 2)
					parties = dep.split("- >");

				if (parties.length > 1) {
					try {
						g.ajouteArc(Parseur.convertirSetAttributs(parties[0]),
								Parseur.convertirSetAttributs(parties[1]));
					} catch (IllegalArgumentException | EnsembleAttributsVideException e) {
						// Nothing {a -> b,c; a-> c; c -> d,a}
					}

				} else {
					// Si il manque une partie de la dépendance
					throw new EnsembleDependancesInvalideException(
							"La dépendance " + parties[0] + " ne contient pas de -> ");
				}
			}
		}

		return g;
	}

}
