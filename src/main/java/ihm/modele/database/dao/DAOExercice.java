package ihm.modele.database.dao;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

import com.google.gson.JsonParseException;

import ihm.modele.database.configuration.TableConfiguration;
import ihm.modele.database.sessions.SessionDatabase;
import ihm.modele.exceptions.ConnexionException;
import ihm.modele.exceptions.ExerciceInexistantException;
import ihm.modele.utilisateur.ConfigurationLoader;
import ihm.modele.utilisateur.Exercice;

/**
 * Classe permettant de manipuler les comptes dans la base de données
 * 
 * @author Thibaut PICHON
 * @version 0.1
 */
public class DAOExercice {

	private SessionDatabase session;
	private TableConfiguration tableConfiguration;

	/**
	 * Constructeur de la classe
	 * 
	 * @throws JsonParseException exception levée si le fichier de configuration est
	 *                            mal écrit
	 * @throws IOException        exception levée si un problème de lecture du
	 *                            fichier de configuration est rencontré
	 * @throws ConnexionException exception levée lorsqu'il est impossible de se
	 *                            connecter à la base de données
	 */
	public DAOExercice() throws JsonParseException, IOException, ConnexionException {
		try {
			this.session = ConfigurationLoader.chargerSessionDatabaseConfiguration("settings/database.json");
			this.tableConfiguration = ConfigurationLoader.chargerConfigTables("settings/database.json")
					.get("Exercices");
		} catch (SQLException e) {
			if (e.getErrorCode() == 0) {
				throw new ConnexionException();
			} else {
				System.out.println(e.getErrorCode());
				System.out.println(e.getMessage());
			}
		}
	}

	/**
	 * Méthode permettant de créer un exercice
	 * 
	 * @param exo l'exercice à créer
	 */
	public void creer(Exercice exo) {
		Connection conn = this.session.getConnection();

		try {
			PreparedStatement stmt = conn.prepareStatement("INSERT INTO " + this.tableConfiguration.getTable() + " ("
					+ this.tableConfiguration.getAttribut("relation") + ", "
					+ this.tableConfiguration.getAttribut("tuples") + ") VALUES(?, ?)");
			stmt.setString(1, exo.serializerRelation());
			stmt.setString(2, exo.serializerTuples());
			stmt.execute();

			stmt = conn.prepareStatement("Select id FROM " + this.tableConfiguration.getTable() + " ORDER BY "
					+ this.tableConfiguration.getAttribut("id") + " DESC LIMIT 1");
			stmt.execute();
			ResultSet result = stmt.getResultSet();
			result.next();

			exo.setID(result.getInt(this.tableConfiguration.getAttribut("id")));
		} catch (SQLException e) {
			if (e.getErrorCode() == 1146) {
				this.creerTable();
			} else {
				System.out.println(e.getErrorCode());
				System.out.println(e.getMessage());
			}
		}

	}

	/**
	 * Méthode permettant de supprimer un exo de la base de données
	 * 
	 * @param exo l'exercice à supprimer
	 */
	public void supprimer(Exercice exo) {
		Connection conn = this.session.getConnection();

		try {
			PreparedStatement stmt = conn.prepareStatement("DELETE FROM " + this.tableConfiguration.getTable()
					+ " WHERE " + this.tableConfiguration.getAttribut("id") + " = ?");
			stmt.setInt(1, exo.getID());
			stmt.execute();
		} catch (SQLException e) {
			if (e.getErrorCode() == 1146) {
				this.creerTable();
			} else {
				System.out.println(e.getErrorCode());
				System.out.println(e.getMessage());
			}
		}
	}

	/**
	 * Méthode permettant de récupérer la liste des exercices stockés dans la base
	 * de données
	 * 
	 * @return la liste des exercices stockés dans la base de données
	 */
	public List<Exercice> donnerTous() {
		List<Exercice> exercices = new LinkedList<>();
		Connection conn = this.session.getConnection();

		try {
			Statement stmt = conn.createStatement();
			stmt.execute("SELECT * FROM " + this.tableConfiguration.getTable());

			ResultSet result = stmt.getResultSet();
			while (result.next()) {
				Exercice exo = new Exercice(result.getInt(this.tableConfiguration.getAttribut("id")));
				exo.deserializerRelation(result.getString(this.tableConfiguration.getAttribut("relation")));
				exo.deserializerTuples(result.getString(this.tableConfiguration.getAttribut("tuples")));
				exercices.add(exo);
			}
		} catch (SQLException e) {
			if (e.getErrorCode() == 1146) {
				this.creerTable();
			} else {
				System.out.println(e.getErrorCode());
				System.out.println(e.getMessage());
			}
		}

		return exercices;
	}

	/**
	 * Méthode permettant de récupérer un exercice dans la base de données grâce à
	 * son ID
	 * 
	 * @param id l'ID de l'exercice
	 * 
	 * @return l'exercice correspondant à l'ID
	 * 
	 * @throws ExerciceInexistantException exception levée si l'exercice n'existe
	 *                                     pas
	 * @throws IllegalArgumentException    exception levée si l'ID est incorrect (ID
	 *                                     inférieur à 0)
	 */
	public Exercice donnerExerciceParID(int id) throws ExerciceInexistantException, IllegalArgumentException {
		if (id < 0)
			throw new IllegalArgumentException("ID de l'exercice inférieur à 0");

		Connection conn = this.session.getConnection();

		try {
			PreparedStatement stmt = conn.prepareStatement("SELECT " + this.tableConfiguration.getAttribut("id") + ", "
					+ this.tableConfiguration.getAttribut("relation") + ", "
					+ this.tableConfiguration.getAttribut("tuples") + ", COUNT(*) FROM "
					+ this.tableConfiguration.getTable() + " WHERE " + this.tableConfiguration.getAttribut("id")
					+ " = ?");
			stmt.setInt(1, id);
			stmt.execute();

			ResultSet result = stmt.getResultSet();
			result.next();

			if (result.getInt(4) > 0) {
				Exercice exo = new Exercice(result.getInt(this.tableConfiguration.getAttribut("id")));
				exo.deserializerRelation(result.getString(this.tableConfiguration.getAttribut("relation")));
				exo.deserializerTuples(result.getString(this.tableConfiguration.getAttribut("tuples")));
				return exo;
			} else {
				throw new ExerciceInexistantException();
			}
		} catch (SQLException e) {
			if (e.getErrorCode() == 1146) {
				this.creerTable();
			} else {
				System.out.println(e.getErrorCode());
				System.out.println(e.getMessage());
			}

			throw new ExerciceInexistantException();
		}
	}

	/**
	 * Méthode permettant de créer la table exercices
	 */
	private void creerTable() {
		Connection con = this.session.getConnection();

		Statement stmt;
		try {
			stmt = con.createStatement();
			stmt.execute("DROP TABLE IF EXISTS " + this.tableConfiguration.getTable());
			stmt.execute("CREATE TABLE " + this.tableConfiguration.getTable() + " (\n  "
					+ this.tableConfiguration.getAttribut("id") + " int(11) NOT NULL AUTO_INCREMENT,\n  "
					+ this.tableConfiguration.getAttribut("relation") + " text NOT NULL,\n  "
					+ this.tableConfiguration.getAttribut("tuples")
					+ " text NOT NULL,\n   CONSTRAINT PRIMARY KEY ("
					+ this.tableConfiguration.getAttribut("id")
					+ ")\n) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci");
		} catch (SQLException e) {
			System.out.println(e.getErrorCode());
			System.out.println(e.getMessage());
		}
	}

	@Override
	public void finalize() {
		try {
			this.session.closeConnexion();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
