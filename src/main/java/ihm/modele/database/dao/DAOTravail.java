package ihm.modele.database.dao;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

import com.google.gson.JsonParseException;

import ihm.modele.bd.Relation;
import ihm.modele.database.configuration.TableConfiguration;
import ihm.modele.database.sessions.SessionDatabase;
import ihm.modele.exceptions.ConnexionException;
import ihm.modele.exceptions.ExerciceInexistantException;
import ihm.modele.utilisateur.Compte;
import ihm.modele.utilisateur.ConfigurationLoader;
import ihm.modele.utilisateur.Etape;
import ihm.modele.utilisateur.Exercice;

/**
 * Classe permettant de manipuler les comptes dans la base de données
 * 
 * @author Thibaut PICHON
 * @version 0.1
 */
public class DAOTravail {

	private SessionDatabase session;
	private TableConfiguration tableConfiguration;
	private TableConfiguration confTableComptes, confTableExos;

	/**
	 * Constructeur de la classe
	 * 
	 * @throws JsonParseException exception levée si le fichier de configuration est
	 *                            mal écrit
	 * @throws IOException        exception levée si un problème de lecture du
	 *                            fichier de configuration est rencontré
	 * @throws ConnexionException exception levée lorsqu'il est impossible de se
	 *                            connecter à la base de données
	 */
	public DAOTravail() throws JsonParseException, IOException, ConnexionException {
		try {
			this.session = ConfigurationLoader.chargerSessionDatabaseConfiguration("settings/database.json");
			Map<String, TableConfiguration> tablesConf = ConfigurationLoader
					.chargerConfigTables("settings/database.json");
			this.tableConfiguration = tablesConf.get("Travail");
			this.confTableComptes = tablesConf.get("Comptes");
			this.confTableExos = tablesConf.get("Exercices");
		} catch (SQLException e) {
			if (e.getErrorCode() == 0) {
				throw new ConnexionException();
			} else {
				System.out.println(e.getErrorCode());
				System.out.println(e.getMessage());
			}
		}
	}

	/**
	 * Méthode permettant d'assigner qu'un utilisateur à travaillr sur un exercice
	 * 
	 * @param compte le compte ayant travailler sur l'exercice
	 * @param exo    l'exercice surlequel il a travaillé
	 */
	public void creer(Compte compte, Exercice exo) {
		Connection conn = this.session.getConnection();

		try {
			PreparedStatement stmt = conn.prepareStatement("INSERT INTO " + this.tableConfiguration.getTable() + " ("
					+ this.tableConfiguration.getAttribut("id_compte") + ", "
					+ this.tableConfiguration.getAttribut("id_exercice") + ", "
					+ this.tableConfiguration.getAttribut("relationSaisie") + ", "
					+ this.tableConfiguration.getAttribut("relation") + ", "
					+ this.tableConfiguration.getAttribut("couvMin") + ", "
					+ this.tableConfiguration.getAttribut("fermTran") + ", "
					+ this.tableConfiguration.getAttribut("fermEns") + ", " + this.tableConfiguration.getAttribut("cle")
					+ ", " + this.tableConfiguration.getAttribut("formeNormale") + ", "
					+ this.tableConfiguration.getAttribut("decomposition") + ", "
					+ this.tableConfiguration.getAttribut("synthese") + ", "
					+ this.tableConfiguration.getAttribut("perteDonnees") + ", "
					+ this.tableConfiguration.getAttribut("perteDependances")
					+ ") VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
			stmt.setInt(1, compte.getID());
			stmt.setInt(2, exo.getID());
			stmt.setString(3, exo.serializerRelation());
			stmt.setInt(4, compte.donnerEtatEtape(Etape.Relation));
			stmt.setInt(5, compte.donnerEtatEtape(Etape.CouvMin));
			stmt.setInt(6, compte.donnerEtatEtape(Etape.FermTran));
			stmt.setInt(7, compte.donnerEtatEtape(Etape.FermEns));
			stmt.setInt(8, compte.donnerEtatEtape(Etape.Cle));
			stmt.setInt(9, compte.donnerEtatEtape(Etape.FormeNormale));
			stmt.setInt(10, compte.donnerEtatEtape(Etape.Decomposition));
			stmt.setInt(11, compte.donnerEtatEtape(Etape.Synthese));
			stmt.setInt(12, compte.donnerEtatEtape(Etape.PertesDonnees));
			stmt.setInt(13, compte.donnerEtatEtape(Etape.PertesDependances));
			stmt.execute();
		} catch (SQLException e) {
			if (e.getErrorCode() == 1146 || e.getErrorCode() == 1054) {
				this.creerTable();
				this.creer(compte, exo);
			} else {
				System.out.println(e.getErrorCode());
				System.out.println(e.getMessage());
			}
		}
	}

	/**
	 * Méthode permettant de récupérer la liste des exercices réalisés par un
	 * étudiant
	 * 
	 * @param compte le compte de l'étudiant
	 * 
	 * @return la liste des exercices
	 */
	public Map<Integer, Exercice> donnerExosCompte(Compte compte) {
		Map<Integer, Exercice> res = new HashMap<>();

		try {
			Connection conn = this.session.getConnection();
			DAOExercice daoexo = new DAOExercice();

			PreparedStatement stmt = conn.prepareStatement("SELECT " + this.tableConfiguration.getAttribut("id") + ", "
					+ this.tableConfiguration.getAttribut("id_exercice") + " FROM " + this.tableConfiguration.getTable()
					+ " WHERE " + this.tableConfiguration.getAttribut("id_compte") + " = ?");
			stmt.setInt(1, compte.getID());
			ResultSet resultSet = stmt.executeQuery();

			while (resultSet.next()) {
				res.put(resultSet.getInt(1), daoexo.donnerExerciceParID(resultSet.getInt(2)));
			}
		} catch (JsonParseException e) {
			// NOTHING
		} catch (IOException e) {
			// NOTHING
		} catch (IllegalArgumentException e) {
			// NOTHING
		} catch (ExerciceInexistantException e) {
			// NOTHING
		} catch (SQLException e) {
			if (e.getErrorCode() == 1146) {
				this.creerTable();
			} else {
				System.out.println(e.getErrorCode());
				System.out.println(e.getMessage());
			}
		} catch (ConnexionException e) {
			e.printStackTrace();
		}

		return res;
	}

	/**
	 * Méthode permettant de récupérer les stats d'un travail sur un exercice
	 * 
	 * @param id l'id du travail/tâche
	 * 
	 * @return une map associant à chaque étape le nombre d'essai ratté
	 */
	public Map<Etape, Integer> donnerStatsTaches(int id) {
		Map<Etape, Integer> res = new HashMap<>();

		// Les stats sont initialisés à 0
		for (Etape etape : Etape.values()) {
			res.put(etape, 0);
		}

		try {
			Connection conn = this.session.getConnection();
			PreparedStatement stmt = conn.prepareStatement("SELECT * FROM " + this.tableConfiguration.getTable()
					+ " WHERE " + this.tableConfiguration.getAttribut("id") + " = ?");
			stmt.setInt(1, id);
			ResultSet resultSet = stmt.executeQuery();

			if (resultSet.next()) {
				res.put(Etape.Relation, resultSet.getInt(this.tableConfiguration.getAttribut("relation")));
				res.put(Etape.CouvMin, resultSet.getInt(this.tableConfiguration.getAttribut("couvMin")));
				res.put(Etape.FermTran, resultSet.getInt(this.tableConfiguration.getAttribut("fermTran")));
				res.put(Etape.FermEns, resultSet.getInt(this.tableConfiguration.getAttribut("fermEns")));
				res.put(Etape.Cle, resultSet.getInt(this.tableConfiguration.getAttribut("cle")));
				res.put(Etape.FormeNormale, resultSet.getInt(this.tableConfiguration.getAttribut("formeNormale")));
				res.put(Etape.Decomposition, resultSet.getInt(this.tableConfiguration.getAttribut("decomposition")));
				res.put(Etape.Synthese, resultSet.getInt(this.tableConfiguration.getAttribut("synthese")));
				res.put(Etape.PertesDonnees, resultSet.getInt(this.tableConfiguration.getAttribut("perteDonnees")));
				res.put(Etape.PertesDependances,
						resultSet.getInt(this.tableConfiguration.getAttribut("perteDependances")));
			}
		} catch (JsonParseException e) {
			// NOTHING
		} catch (IllegalArgumentException e) {
			// NOTHING
		} catch (SQLException e) {
			if (e.getErrorCode() == 1146) {
				this.creerTable();
			} else {
				System.out.println(e.getErrorCode());
				System.out.println(e.getMessage());
			}
		}

		return res;
	}

	/**
	 * Méthode permettant de récupérer la relation saisie lors d'un travail sur un
	 * exercice
	 * 
	 * @param id l'id du travail/tâche
	 * 
	 * @return la relation saisie
	 */
	public Relation donnerRelationTaches(int id) {
		try {
			Connection conn = this.session.getConnection();
			PreparedStatement stmt = conn
					.prepareStatement("SELECT " + this.tableConfiguration.getAttribut("relationSaisie") + " FROM "
							+ this.tableConfiguration.getTable() + " WHERE " + this.tableConfiguration.getAttribut("id")
							+ " = ?");
			stmt.setInt(1, id);
			ResultSet resultSet = stmt.executeQuery();

			if (resultSet.next()) {
				Exercice exo = new Exercice();
				exo.deserializerRelation(resultSet.getString(1));
				return exo.getRelation();
			}
		} catch (JsonParseException e) {
			// NOTHING
		} catch (IllegalArgumentException e) {
			// NOTHING
		} catch (SQLException e) {
			if (e.getErrorCode() == 1146) {
				this.creerTable();
			} else {
				System.out.println(e.getErrorCode());
				System.out.println(e.getMessage());
			}
		}

		return null;
	}

	/**
	 * Méthode permettant de créer la table travail
	 */
	private void creerTable() {
		Connection con = this.session.getConnection();

		try {
			Statement stmt = con.createStatement();
			stmt.execute("DROP TABLE IF EXISTS " + this.tableConfiguration.getTable());
			stmt.execute("CREATE TABLE " + this.tableConfiguration.getTable() + " (\n "
					+ this.tableConfiguration.getAttribut("id") + " int(11) NOT NULL AUTO_INCREMENT,\n  "
					+ this.tableConfiguration.getAttribut("id_compte") + " int(11) NOT NULL,\n  "
					+ this.tableConfiguration.getAttribut("id_exercice") + " int(11) NOT NULL,\n  "
					+ this.tableConfiguration.getAttribut("relationSaisie")
					+ " text NOT NULL,\n  " + this.tableConfiguration.getAttribut("relation")
					+ " int(11) NOT NULL,\n  " + this.tableConfiguration.getAttribut("couvMin")
					+ " int(11) NOT NULL,\n  " + this.tableConfiguration.getAttribut("fermTran")
					+ " int(11) NOT NULL,\n  " + this.tableConfiguration.getAttribut("fermEns")
					+ " int(11) NOT NULL,\n  " + this.tableConfiguration.getAttribut("cle") + " int(11) NOT NULL,\n  "
					+ this.tableConfiguration.getAttribut("formeNormale") + " int(11) NOT NULL,\n  "
					+ this.tableConfiguration.getAttribut("decomposition") + " int(11) NOT NULL,\n  "
					+ this.tableConfiguration.getAttribut("synthese") + " int(11) NOT NULL,\n  "
					+ this.tableConfiguration.getAttribut("perteDonnees") + " int(11) NOT NULL,\n  "
					+ this.tableConfiguration.getAttribut("perteDependances")
					+ " int(11) NOT NULL,\n CONSTRAINT PRIMARY KEY (\n" + this.tableConfiguration.getAttribut("id")
					+ "),\n  CONSTRAINT FK_Compte FOREIGN KEY (" + this.tableConfiguration.getAttribut("id_compte")
					+ ") REFERENCES " + this.confTableComptes.getTable() + " ("
					+ this.confTableComptes.getAttribut("id") + "),\n  CONSTRAINT FK_Exercice FOREIGN KEY ("
					+ this.tableConfiguration.getAttribut("id_exercice") + ") REFERENCES "
					+ this.confTableExos.getTable() + " (" + this.confTableExos.getAttribut("id")
					+ ")\n) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci");
		} catch (SQLException e) {
			System.out.println(e.getErrorCode());
			System.out.println(e.getMessage());
		}
	}

	@Override
	public void finalize() {
		try {
			this.session.closeConnexion();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
