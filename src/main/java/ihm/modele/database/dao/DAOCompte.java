package ihm.modele.database.dao;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.google.gson.JsonParseException;

import ihm.modele.database.configuration.TableConfiguration;
import ihm.modele.database.sessions.SessionDatabase;
import ihm.modele.exceptions.CompteExistantException;
import ihm.modele.exceptions.ConnexionException;
import ihm.modele.utilisateur.Compte;
import ihm.modele.utilisateur.ConfigurationLoader;

/**
 * Classe permettant de manipuler les comptes dans la base de données
 * 
 * @author Thibaut PICHON
 * @version 0.1
 */
public class DAOCompte {

	private SessionDatabase session;
	private TableConfiguration tableConfiguration;

	/**
	 * Constructeur de la classe
	 * 
	 * @throws JsonParseException exception levée si le fichier de configuration est
	 *                            mal écrit
	 * @throws IOException        exception levée si un problème de lecture du
	 *                            fichier de configuration est rencontré
	 * @throws ConnexionException exception levée lorsqu'il est impossible de se
	 *                            connecter à la base de données
	 */
	public DAOCompte() throws JsonParseException, IOException, ConnexionException {
		try {
			this.session = ConfigurationLoader.chargerSessionDatabaseConfiguration("settings/database.json");
			this.tableConfiguration = ConfigurationLoader.chargerConfigTables("settings/database.json").get("Comptes");
		} catch (SQLException e) {
			if (e.getErrorCode() == 0) {
				throw new ConnexionException();
			} else {
				System.out.println(e.getErrorCode());
				System.out.println(e.getMessage());
			}
		}
	}

	/**
	 * Méthode permettant de créer un compte
	 * 
	 * @param compte le compte créer
	 * 
	 * @throws CompteExistantException exception levée si le compte existe déjà
	 */
	public void creer(Compte compte) throws CompteExistantException {
		Connection conn = this.session.getConnection();

		try {
			PreparedStatement stmt = conn.prepareStatement("SELECT COUNT(*) FROM " + this.tableConfiguration.getTable()
					+ " WHERE " + this.tableConfiguration.getAttribut("identifiant") + " = ?");
			stmt.setString(1, compte.getIdentifiant());
			ResultSet res = stmt.executeQuery();
			res.next();

			if (res.getInt(1) > 0)
				throw new CompteExistantException();

			stmt = conn.prepareStatement("INSERT INTO " + this.tableConfiguration.getTable() + " ("
					+ this.tableConfiguration.getAttribut("identifiant") + ", "
					+ this.tableConfiguration.getAttribut("motDePasse") + ") VALUES(?, ?)");
			stmt.setString(1, compte.getIdentifiant());
			stmt.setString(2, compte.getMotDePasse());
			stmt.execute();
		} catch (SQLException e) {
			if (e.getErrorCode() == 1146) {
				this.creerTable();
				this.creer(compte);
			} else {
				System.out.println(e.getErrorCode());
				System.out.println(e.getMessage());
			}
		}
	}

	/**
	 * Méthode permettant de mettre à jour le compte
	 * 
	 * @param compte le compte à mettre à jour
	 */
	public void miseAJour(Compte compte) {
		Connection conn = this.session.getConnection();

		try {
			PreparedStatement stmt = conn.prepareStatement("UPDATE " + this.tableConfiguration.getTable() + " SET "
					+ this.tableConfiguration.getAttribut("identifiant") + " = ?, "
					+ this.tableConfiguration.getAttribut("motDePasse") + " = ? WHERE "
					+ this.tableConfiguration.getAttribut("id") + " = ?");
			stmt.setString(1, compte.getIdentifiant());
			stmt.setString(2, compte.getMotDePasse());
			stmt.setInt(3, compte.getID());
			stmt.execute();
		} catch (SQLException e) {
			if (e.getErrorCode() == 1146) {
				this.creerTable();
			} else {
				System.out.println(e.getErrorCode());
				System.out.println(e.getMessage());
			}
		}
	}

	/**
	 * Méthode permettant de supprimer un compte de la base de données
	 * 
	 * @param compte le compte à supprimer
	 */
	public void supprimer(Compte compte) {
		Connection conn = this.session.getConnection();

		try {
			PreparedStatement stmt = conn.prepareStatement("DELETE FROM " + this.tableConfiguration.getTable()
					+ " WHERE " + this.tableConfiguration.getAttribut("id") + " = ?");
			stmt.setInt(1, compte.getID());
			stmt.execute();
		} catch (SQLException e) {
			if (e.getErrorCode() == 1146) {
				this.creerTable();
			} else {
				System.out.println(e.getErrorCode());
				System.out.println(e.getMessage());
			}
		}
	}

	/**
	 * Méthode pour tester l'authentification d'un compte
	 * 
	 * @param compte le compte avec son identifiant et son mot de passe
	 * 
	 * @return vrai si les informations d'authentification sont valides, faux sinon
	 */
	public boolean authentifier(Compte compte) {
		Connection conn = this.session.getConnection();

		try {
			PreparedStatement stmt = conn.prepareStatement("SELECT " + this.tableConfiguration.getAttribut("id")
					+ ", COUNT(*) FROM " + this.tableConfiguration.getTable() + " WHERE "
					+ this.tableConfiguration.getAttribut("identifiant") + " = ? AND "
					+ this.tableConfiguration.getAttribut("motDePasse") + " = ?");
			stmt.setString(1, compte.getIdentifiant());
			stmt.setString(2, compte.getMotDePasse());
			ResultSet res = stmt.executeQuery();
			res.next();

			if (res.getInt(2) < 1) {
				return false;
			} else {
				// Mise à jourd de l'ID du compte
				compte.setID(res.getInt(1));
				return true;
			}
		} catch (SQLException e) {
			if (e.getErrorCode() == 1146) {
				this.creerTable();
			} else {
				System.out.println(e.getErrorCode());
				System.out.println(e.getMessage());
			}
			return false;
		}
	}

	/**
	 * Méthode permettant de créer la table comptes
	 */
	private void creerTable() {
		Connection con = this.session.getConnection();

		Statement stmt;
		try {
			stmt = con.createStatement();
			stmt.execute("DROP TABLE IF EXISTS " + this.tableConfiguration.getTable());
			stmt.execute("CREATE TABLE " + this.tableConfiguration.getTable() + " (\n  "
					+ this.tableConfiguration.getAttribut("id") + " int(11) NOT NULL AUTO_INCREMENT,\n  "
					+ this.tableConfiguration.getAttribut("identifiant")
					+ " varchar(255) NOT NULL,\n" + "  "
					+ this.tableConfiguration.getAttribut("motDePasse")
					+ " varchar(255) NOT NULL,\n  CONSTRAINT PRIMARY KEY ("
					+ this.tableConfiguration.getAttribut("id") + ")\n"
					+ ") ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci");
		} catch (SQLException e) {
			System.out.println(e.getErrorCode());
			System.out.println(e.getMessage());
		}
	}

	@Override
	public void finalize() {
		try {
			this.session.closeConnexion();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
