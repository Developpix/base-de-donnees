package ihm.modele.database.configuration;

import java.util.Map;

/**
 * Classe permettant de sauvegarder la configuration d'une base de données
 * 
 * @author Thibaut PICHON
 * @version 0.1
 */
public class DatabaseConfiguration {
	private Map<String, String> connexion;
	private Map<String, TableConfiguration> tables;

	/**
	 * Accesseur permettant de récupérer la configuration de la connexion
	 * 
	 * @return la configuration de la connexion
	 */
	public Map<String, String> getConnexion() {
		return connexion;
	}

	/**
	 * Modifieur permettant de définir la configuration de la connexion
	 * 
	 * @param connexion la configuration de la connexion
	 */
	public void setConnexion(Map<String, String> connexion) {
		this.connexion = connexion;
	}

	/**
	 * Accesseur permettant de récupérer la configuration des tables dans la base de
	 * données
	 * 
	 * @return la configuration des tables dans la base de données
	 */
	public Map<String, TableConfiguration> getTables() {
		return tables;
	}

	/**
	 * Modifieur permettant de définir la configuration des tables dans la base de
	 * données
	 * 
	 * @param tables la configuration des tables dans la base de données
	 */
	public void setTables(Map<String, TableConfiguration> tables) {
		this.tables = tables;
	}
}