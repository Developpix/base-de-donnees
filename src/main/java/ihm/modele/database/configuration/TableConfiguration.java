package ihm.modele.database.configuration;

import java.util.Map;

/**
 * Classe permettant de récupérer la configuration d'une table
 * 
 * @author Thibaut PICHON
 * @version 0.1
 */
public class TableConfiguration {

	private String table;
	private Map<String, String> attributs;

	/**
	 * Accesseur permettant de récupérer le nom de la table
	 * 
	 * @return le nom de la table
	 */
	public String getTable() {
		return table.replaceAll("/[^A-Za-z0-9_-]/", "");
	}

	/**
	 * Modifieur permettant de définir le nom de la table
	 * 
	 * @param table le nom de la table
	 */
	public void setTable(String table) {
		this.table = table;
	}

	/**
	 * Accesseur permettant de récupérer la configuration des attributs dans la
	 * table
	 * 
	 * @return la configuration des attributs dans la table
	 */
	public Map<String, String> getAttributs() {
		return attributs;
	}

	/**
	 * Modifieur permettant de définir la configuration des attributs dans la table
	 * 
	 * @param attributs la configuration des attributs dans la table
	 */
	public void setAttributs(Map<String, String> attributs) {
		this.attributs = attributs;
	}

	/**
	 * Méthode permettant de récupérer le nom de la colonne stockant l'attribut
	 * 
	 * @param attribut l'attribut
	 * 
	 * @return le nom de la colonne stockant l'attribut
	 */
	public String getAttribut(String attribut) {
		return this.attributs.get(attribut).replaceAll("/[^A-Za-z0-9_-]/", "");
	}
}