package ihm.modele.database.sessions;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * Interface décrivant une session à une database
 * 
 * @author Thibaut PICHON
 * @version 0.1
 */
public interface SessionDatabase {
	/**
	 * Accesseur permettant d'obtenir une connexion à la base de données
	 * 
	 * @return la connexion avec la base de données
	 */
	public Connection getConnection();

	/**
	 * Méthode permettant de fermer la connexion
	 * 
	 * @throws SQLException exception levé lorsqu'un problème avec la base de
	 *                      données est rencontré
	 */
	public void closeConnexion() throws SQLException;
}
