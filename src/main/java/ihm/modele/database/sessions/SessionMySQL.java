package ihm.modele.database.sessions;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Classe permettant de créer un session avec une base de données MySQL
 * 
 * @author Thibaut PICHON
 * @version 0.1
 */
public class SessionMySQL implements SessionDatabase {

	private Connection connexion;

	/**
	 * Constructeur pour créer une session à une base de données MySQL
	 * 
	 * @param hote         l'adresse IP ou le nom d'hôte de la base de données
	 * @param port         le port utilisé pour se connecter à la base de données
	 * @param databaseName la nom de la base de données utilisée
	 * @param user         le nom d'utilisateur pour se connecter
	 * @param passwd       le mot de passe de l'utilisateur
	 * 
	 * @throws SQLException exception levée si il y a un problème pour se connecter
	 *                      à la base de données
	 */
	public SessionMySQL(String hote, int port, String databaseName, String user, String passwd) throws SQLException {
		DriverManager.registerDriver(new com.mysql.cj.jdbc.Driver());
		this.connexion = DriverManager.getConnection("jdbc:mysql://" + hote + ":" + port + "/" + databaseName
				+ "?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC",
				user, passwd);
	}

	@Override
	public Connection getConnection() {
		return this.connexion;
	}

	@Override
	public void closeConnexion() throws SQLException {
		this.connexion.close();
	}

}
