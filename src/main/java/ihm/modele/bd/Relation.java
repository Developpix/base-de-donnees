package ihm.modele.bd;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

/**
 * Classe permettant de créer une relation
 * 
 * @author AUBIN Florian
 * @author JOBARD Mathis
 * @author PICHON Thibaut
 * @version 0.0.1
 */
public class Relation {

	private Graphe graphe;
	private Set<Attribut> attributs;

	/**
	 * Contructeur permettant de créer une relation
	 * 
	 * @param ensembleDependances l'ensemble de dépendances de la relation
	 * @param ensembleAttributs   l'ensemble d'attributs de la relation
	 */
	public Relation(Graphe ensembleDependances, Set<Attribut> ensembleAttributs) {

		this.graphe = ensembleDependances;
		this.attributs = ensembleAttributs;

	}

	/**
	 * Accesseurs pour l'attribut graphe correspondant à l'ensemble de dépendances
	 * 
	 * @return le graphe correspondant à l'ensemble de dépendances
	 */
	public Graphe getGraphe() {
		return this.graphe;
	}

	/**
	 * Accesseurs pour l'attribut attributs correpondant à l'ensemble d'attributs de
	 * la relation
	 * 
	 * @return l'ensemble d'attributs de la relation
	 */
	public Set<Attribut> getAttributs() {
		return this.attributs;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((attributs == null) ? 0 : attributs.hashCode());
		result = prime * result + ((graphe == null) ? 0 : graphe.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Relation other = (Relation) obj;
		if (attributs == null) {
			if (other.attributs != null)
				return false;
		} else if (!attributs.equals(other.attributs))
			return false;
		if (graphe == null) {
			if (other.graphe != null)
				return false;
		} else if (!this.graphe.equals(other.getGraphe()))
			return false;
		return true;
	}

	/**
	 * Méthode calculant la couverture minimale de l'ensemble de dépendances
	 * fonctionnelles de la relation
	 * 
	 * @return Graphe la couverture minimale
	 */
	public Graphe couvertureMinimale() {
		return this.graphe.couvertureMinimale();
	}

	/**
	 * Méthode permettant de faire la fermeture d'un ensemble
	 * 
	 * @param liste une liste d'attribut
	 * 
	 * @return la fermeture de l'ensemble
	 */
	public Set<Attribut> fermetureEnsemble(Set<Attribut> liste) {

		Set<Attribut> resultat = new TreeSet<>(liste);
		Graphe fermeture = this.couvertureMinimale().fermetureTransitive();
		for (Set<Attribut> sg : fermeture.donneCles()) {
			if (liste.containsAll(sg)) {
				for (Set<Attribut> sd : fermeture.successeurs(sg)) {
					resultat.addAll(sd);
				}
			}
		}
		return resultat;
	}

	/**
	 * Méthode permettant de retourner la fermeture transitive d'un graphe
	 * 
	 * @return la fermeture transitive d'un graphe
	 */
	public Graphe fermetureTransitive() {
		return graphe.fermetureTransitive();
	}

	/**
	 * Méthode permettant de décomposer une relation en plusieurs avec l'algorithme
	 * de synthèse
	 * 
	 * @param cle la clé à utiliser
	 * 
	 * @return la décomposition de la relation
	 */
	public List<Relation> algorithmeSynthese(Set<Attribut> cle) {

		List<Relation> listeRelations = new LinkedList<Relation>();

		try {
			Graphe couvMin = this.couvertureMinimale();

			for (Set<Attribut> sg : couvMin.donneCles()) {

				Set<Attribut> atts = new HashSet<>();
				atts.addAll(sg);

				for (Set<Attribut> sd : couvMin.successeurs(sg)) {
					atts.addAll(sd);
				}

				Graphe gBis = new Graphe(atts);

				for (Set<Attribut> sd : couvMin.successeurs(sg)) {
					gBis.ajouteArc(sg, sd);
				}

				listeRelations.add(new Relation(gBis, atts));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		boolean clePresente = false;

		for (Relation rel : listeRelations) {
			if (rel.getAttributs().containsAll(cle))
				clePresente = true;
		}

		if (!clePresente) {
			try {
				listeRelations.add(new Relation(new Graphe(cle), cle));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return listeRelations;

	}

	/**
	 * Méthode pour vérifier qu'il n'y a pas de pertes de données
	 * 
	 * @param liste liste de relations
	 * @return vrai si il n'y a pas de pertes de données et faux sinon
	 */
	public boolean verifPasPerteDonnees(List<Relation> liste) {

		Set<Attribut> att = new TreeSet<>();

		for (Relation r : liste) {
			att.addAll(r.getAttributs());
		}

		return att.equals(this.attributs);

	}

	/**
	 * Méthode pour vérifier qu'il n'y a pas de pertes de dépendances
	 * 
	 * @param liste liste de relations
	 * 
	 * @return vrai si il n'y a pas de pertes de dépendances et faux sinon
	 */
	public boolean verifPasPerteDependances(List<Relation> liste) {

		Set<Attribut> atts = new TreeSet<>();

		for (Relation r : liste) {
			atts.addAll(r.getAttributs());
		}

		try {

			Graphe g = new Graphe(atts);

			for (Relation r : liste) {
				for (Set<Attribut> sg : r.getGraphe().donneCles()) {
					for (Set<Attribut> sd : r.getGraphe().successeurs(sg)) {
						g.ajouteArc(sg, sd);
					}
				}
			}

			return g.couvertureMinimale().fermetureTransitive().equals(this.couvertureMinimale().fermetureTransitive());

		} catch (Exception e) {
			e.printStackTrace();

			return false;
		}
	}

	/**
	 * Méthode qui trouve la clé de la relation
	 * 
	 * @return la clé de la relation
	 */
	public Set<Attribut> rechercheCle() {

		Set<Attribut> cle = null;

		cle = new TreeSet<>(this.attributs);

		for (Attribut att : this.attributs) {
			Set<Attribut> tmp = new TreeSet<>(cle);
			tmp.remove(att);

			if (this.fermetureEnsemble(tmp).containsAll(this.attributs))
				cle = tmp;
		}

		return cle;
	}

	/**
	 * Méthode permettant de récuperer la forme normale de la relation
	 * 
	 * @param cle la clé à utiliser
	 * 
	 * @return la forme normale de la relation
	 */
	public FormeNormale rechercheformeNormale(Set<Attribut> cle) {
		return this.nf2(cle)
				? (this.nf3(cle) ? (this.nfbc3() ? FormeNormale.NFBC3 : FormeNormale.NF3) : FormeNormale.NF2)
				: FormeNormale.NF1;
	}

	private boolean nf2(Set<Attribut> cle) {
		try {
			if (cle.size() > 1) {
				Set<Attribut> nonCle = new HashSet<Attribut>(this.attributs);
				nonCle.removeAll(cle);

				Graphe tmp = Graphe.dependancesSimple(this.graphe).fermetureTransitive();

				for (Set<Attribut> sg : tmp.donneCles()) {
					for (Set<Attribut> sd : tmp.successeurs(sg)) {
						if (cle.containsAll(sg) && cle.size() != sg.size() && !tmp.estArc(cle, sd)) {
							return false;
						}
					}
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return true;
	}

	/**
	 * Méthode permettant de vérifier si la relation est en 3NF
	 * 
	 * @param cle la clé à utiliser
	 * 
	 * @return vrai si elle est en 3NF, faux sinon
	 */
	private boolean nf3(Set<Attribut> cle) {

		try {
			Set<Attribut> nonCle = new HashSet<Attribut>(this.attributs);
			nonCle.removeAll(cle);

			Graphe tmp = Graphe.dependancesSimple(this.graphe);

			Set<Attribut> tmpAtt;

			for (Attribut att : nonCle) {
				tmpAtt = new HashSet<>();
				tmpAtt.add(att);

				if (!tmp.estArc(cle, tmpAtt)) {
					return false;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return true;
	}

	private boolean nfbc3() {
		// Parcours des attributs sources de dépendances
		for (Set<Attribut> sg : this.graphe.donneCles()) {
			// Si ce n'est pas une clé candidate on retourne faux
			if (!this.fermetureEnsemble(sg).equals(this.attributs))
				return false;
		}

		// On retourne vrai si toute les sources de dépendances sont des clés candidates
		return true;
	}

	/**
	 * Méthode retourner une chaîne de caractères représentant la relation
	 * 
	 * @return la chaîne représentant la relation
	 */
	@Override
	public String toString() {

		String str = this.attributs.toString().replace("[", "<").replace("]", ">");
		str += this.graphe;

		return str;
	}

}
