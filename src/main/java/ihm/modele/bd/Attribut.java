package ihm.modele.bd;

import ihm.modele.exceptions.NomVideException;
import ihm.modele.utilisateur.Parseur;

/**
 * Classe permettant de créer un attribut
 * 
 * @author PICHON Thibaut
 * @version 0.0.1
 */
public class Attribut implements Comparable<Attribut> {

	private String nom;

	/**
	 * Constructeur de la classe Attribut
	 * 
	 * @param nom le nom de l'attribut
	 * 
	 * @throws NomVideException si le nom est vide
	 */
	public Attribut(String nom) throws NomVideException {

		if (nom == null)
			throw new IllegalArgumentException();

		this.nom = Parseur.nettoyerNomAttribut(nom).replace("{", "").replace("}", "").replace(";", "").replace(",", "")
				.replace("->", "").replace("<", "").replace(">", "");

		if (this.nom.isEmpty())
			throw new NomVideException();
	}

	/**
	 * Méthode permettant d'obtenir un hash code pour l'attribut
	 * 
	 * @return un hash code de l'attribut
	 */
	@Override
	public int hashCode() {
		return this.nom.hashCode();
	}

	/**
	 * Méthode permettant de tester si deux attribut sont égaux
	 * 
	 * @param obj l'objet à comparer
	 * 
	 * @return true si ils sont égaux, false sinon
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Attribut other = (Attribut) obj;
		if (nom == null) {
			if (other.nom != null)
				return false;
		} else if (!nom.equals(other.nom))
			return false;
		return true;
	}

	/**
	 * Méthode comparer deux attributs suivant leurs noms (lexicographique)
	 * 
	 * @param att2 l'attribut à comparer
	 * 
	 * @return -1, 0 ou 1 suivant si l'attribut est inférieur, égaux ou supérieur au
	 *         deuxième attribut
	 */
	@Override
	public int compareTo(Attribut att2) {

		if (this.equals(att2))
			return 0;

		return this.toString().compareTo(att2.toString());
	}

	/**
	 * Modifieur du nom de l'attribut
	 * 
	 * @param nom le nouveau de l'attribut
	 * @throws NomVideException         si le nom est vide
	 * @throws IllegalArgumentException si le nom est null
	 */
	public void setNom(String nom) throws NomVideException, IllegalArgumentException {
		if (nom == null)
			throw new IllegalArgumentException();

		if (nom.isEmpty())
			throw new NomVideException();

		this.nom = nom;
	}

	/**
	 * Méthode permettant de convertir un attribut en chaîne de caractères
	 * 
	 * @return l'attribut sous forme de chaîne de caractères
	 */
	@Override
	public String toString() {
		return this.nom;
	}
}
