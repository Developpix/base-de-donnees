package ihm.modele.bd;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import javax.swing.table.AbstractTableModel;

/**
 * Classe permettant de créer un table contenant des tuples
 * 
 * @author Thibaut PICHON
 * @version 0.1
 */
public class Table extends AbstractTableModel {

	/**
	 * Serial version UID
	 */
	private static final long serialVersionUID = 1L;

	private Relation relation;
	private Map<Attribut, List<String>> tuples;

	/**
	 * Constructeur de la classe pour créer une table vide
	 */
	public Table() {
		super();

		this.tuples = new HashMap<>();
	}

	/**
	 * Constructeur de la classe
	 * 
	 * @param attributs les attributs de la table
	 */
	public Table(Set<Attribut> attributs) {
		super();

		this.tuples = new HashMap<>();

		// Initialisation des listes des valeurs des attributs
		for (Attribut att : attributs) {
			this.tuples.put(att, new LinkedList<>());
		}
	}

	/**
	 * Accesseur permettant de récupérer la liste des tuples de la table
	 * 
	 * @return la liste des tuples
	 */
	public Map<Attribut, List<String>> getTuples() {
		return this.tuples;
	}

	/**
	 * Accesseur permettant de récupérer la liste des attributs de la table
	 * 
	 * @return la liste des attributs de la table
	 */
	public Set<Attribut> getAttributs() {
		return this.tuples.keySet();
	}

	/**
	 * Méthode permettant de récupérer le nombre de tuples dans la table
	 * 
	 * @return le nombre de tuples dans la table
	 */
	public int nombreTuples() {
		return this.tuples.values().iterator().next().size();
	}

	/**
	 * Méthode permettant d'ajouter un attribut dans la table
	 * 
	 * @param att l'attribut à ajouter
	 */
	public void ajouterAttribut(Attribut att) {
		this.tuples.put(att, new LinkedList<>());
		
		// On supprime tous les tuples
		for (Attribut attDedans : this.tuples.keySet()) {
			this.tuples.get(attDedans).clear();
		}
		
		this.fireTableStructureChanged();
		this.fireTableDataChanged();
	}

	/**
	 * Méthode permettant de supprimer un attribut de la table
	 * 
	 * @param att l'attribut à supprimer
	 */
	public void supprimerAttribut(Attribut att) {
		this.tuples.remove(att);
		
		this.fireTableStructureChanged();
	}

	/**
	 * Méthode permettant d'ajouter un tuple
	 * 
	 * @param tuple une Map contenant la liste des valeurs de chaque attribut
	 */
	public void ajouterTuple(Map<Attribut, String> tuple) {
		// Si les attributs du tuple sont identiques à ceux de la table
		if (this.tuples.keySet().equals(tuple.keySet())) {
			// Ajout des valeurs des attributs du tuple dans un tuple de la table
			for (Attribut att : tuple.keySet()) {
				this.tuples.get(att).add(tuple.get(att));
			}

			this.relation = null;

			this.fireTableDataChanged();
		}
	}

	/**
	 * Méthode permettant de supprimé un tuple dans la table
	 * 
	 * @param index l'index du tuple dans la table
	 * @param tuple le tuple à mettre à jour
	 */
	public void majTuple(int index, Map<Attribut, String> tuple) {
		// Si l'index est valide et ques les attributs du tuple sont les mêmes que ceux
		// de la table
		if (index >= 0 && index < this.tuples.values().iterator().next().size()
				&& this.tuples.keySet().equals(tuple.keySet())) {
			// Modification des valeurs des attributs du tuple
			for (Attribut att : this.tuples.keySet()) {
				this.tuples.get(att).add(index, tuple.get(att));
			}

			this.relation = null;

			this.fireTableDataChanged();
		}
	}

	/**
	 * Méthode permettant de supprimé un tuple dans la table
	 * 
	 * @param index l'index du tuple dans la table
	 */
	public void supprimerTuple(int index) {
		// Si l'index est valide
		if (index >= 0 && index < this.tuples.values().iterator().next().size()) {
			// Suppression des valeurs des attributs du tuple
			for (Attribut att : this.tuples.keySet()) {
				this.tuples.get(att).remove(index);
			}

			this.relation = null;

			this.fireTableDataChanged();
		}
	}

	/**
	 * Méthode permettant de récupérer le nombre de tuples dans la table
	 * 
	 * @param index l'index (numéro de la ligne) du tuple
	 * 
	 * @return le nombre de tuples présent dans la table
	 */
	public Map<Attribut, String> getTuple(int index) {
		Map<Attribut, String> tuple = new TreeMap<>();

		// Si l'index est valide
		if (index >= 0 && index < this.tuples.values().iterator().next().size()) {
			// Ajout des valeurs des attributs dans le tuple
			for (Attribut att : this.tuples.keySet()) {
				tuple.put(att, this.tuples.get(att).get(index));
			}
		}

		return tuple;
	}

	/**
	 * Méthode permettant de récupérer le nombre de tuples dans la table
	 * 
	 * @return le nombre de tuples présent dans la table
	 */
	public int taille() {
		return this.tuples.values().iterator().next().size();
	}

	/**
	 * Méthode permettant de récupérer la relation associé à une table
	 * 
	 * @return la relation associé à une table
	 */
	public Relation getRelation() {
		if (this.relation == null) {
			List<Set<Attribut>> combinaisons = new LinkedList<>();
			/*
			 * On genère l'ensemble de toutes les combinaisons d'attributs de taille 1 à
			 * n-1, sans prendre en compte l'ordre des attributs
			 */
			for (int i = 1; i < this.tuples.keySet().size(); i++) {
				this.generateCombinaisons(new ArrayList<>(this.tuples.keySet()), i, 0, combinaisons,
						new TreeSet<Attribut>());
			}

			/*
			 * On créer une map associant chaque combinaison d'attributs à la liste des
			 * attributs non présent dans la combinaison, ainsi qu'un clone de la map
			 * (tmpMaps) afin de supprimé les dépendances puisque maps sera utilisé pour
			 * parcourir l'ensemble des dépendances possibles
			 */
			Map<Set<Attribut>, Set<Attribut>> maps = new HashMap<>();
			Map<Set<Attribut>, Set<Attribut>> tmpMaps = new HashMap<>();
			for (Set<Attribut> combinaison : combinaisons) {
				Set<Attribut> attributsNonPresent = new TreeSet<>();

				for (Attribut att : this.tuples.keySet()) {
					if (!combinaison.contains(att))
						attributsNonPresent.add(att);
				}

				maps.put(combinaison, attributsNonPresent);

				/*
				 * Création d'un clone de la map avec un clones des attributs non présent dans
				 * la combinaisons pour le parcours des combinaisons (plus tard dans l'algo) qui
				 * supprimera des attributs dans la collection des attributs non présent dans la
				 * combinaison
				 */
				tmpMaps.put(combinaison, new TreeSet<>(attributsNonPresent));
			}

			for (Set<Attribut> combinaison : combinaisons) {
				/*
				 * On créer une map pour associé chaque valeurs des attributs de la combinaison
				 * à la liste des valeurs des autres attributs (pas présent dans la combinaison)
				 */
				Map<List<String>, Map<Attribut, String>> mapsC = new HashMap<>();
				// TODO utilisation d'une Map<List<String>, Map<Attribut, String>> pour
				// identifier l'attribut avec une valeur différente

				for (int i = 0; i < this.tuples.values().iterator().next().size(); i++) {
					List<String> combinaisonValeurs = new LinkedList<>();

					for (Attribut attInCombinaison : combinaison) {
						combinaisonValeurs.add(this.tuples.get(attInCombinaison).get(i));
					}

					/*
					 * Si la combinaison des valeurs des attributs présent dans la combinaison
					 * d'attributs a déjà été trouvé dans d'autres tuples pour chaque attribut non
					 * présent dans la combinaison je regarde les valeurs de ces attributs dans le
					 * tuple et compare les valeurs avec celles déjà trouvé dans les autres tuples
					 * ayant les mêmes valeurs des attributs présent dans la combinaison
					 * d'attributs, si on a trouvé une valeur pour l'attribut différent avant alors
					 * la dépendance est invalide on la retire de tmpMaps
					 */
					if (mapsC.keySet().contains(combinaisonValeurs)) {
						for (Attribut attributNonPresent : tmpMaps.get(combinaison)) {
							if (!mapsC.get(combinaisonValeurs).get(attributNonPresent)
									.equals(this.tuples.get(attributNonPresent).get(i)))
								maps.get(combinaison).remove(attributNonPresent);
						}
					} else {
						/*
						 * Sinon si on a pas trouvé le couple de valeurs des attributs présent dans la
						 * combinaison alors on ajoute la combinaison de valeur dans listeC (cf expliqué
						 * précèdement), on ajoute aussi les valeurs des autres attributs dans mapsC
						 */
						if (i < this.tuples.values().iterator().next().size() - 1) {
							Map<Attribut, String> tmpTuple = new HashMap<>();

							for (Attribut attributNonPresent : tmpMaps.get(combinaison)) {
								tmpTuple.put(attributNonPresent, this.tuples.get(attributNonPresent).get(i));
							}

							mapsC.put(combinaisonValeurs, tmpTuple);
						}
					}
				}
			}

			// Création du graphe à partir de la map contenant l'ensemble des dépendances
			Graphe g = new Graphe(this.tuples.keySet());
			for (Set<Attribut> sg : maps.keySet()) {
				Set<Attribut> sd = maps.get(sg);

				// Si il y a des attributs dans la partie droite
				if (!sd.isEmpty()) {
					try {
						g.ajouteArc(sg, sd);
					} catch (Exception e) {
						/* NOTHING */ e.printStackTrace();
					}
				}
			}

			this.relation = new Relation(g, this.tuples.keySet());
		}

		return this.relation;
	}

	/**
	 * Méthode permettant de générer une liste de combinaisons d'attributs
	 * 
	 * @param liste       la liste des attributs disponibles
	 * @param longueur    la taille des combinaisons a généré (recommandé nb
	 *                    attributs - 1)
	 * @param indexDepart l'index de départ (recommandé 0)
	 * @param res         une List<List<Attribut>> permettant de stocker la liste
	 *                    des combinaisons
	 * @param tmp         un Set<Attribut> permettant de stocker la combinaison en
	 *                    cours de réalisation
	 */
	private void generateCombinaisons(List<Attribut> liste, int longueur, int indexDepart, List<Set<Attribut>> res,
			Set<Attribut> tmp) {
		if (longueur == 0) {
			// Stocke un clone de la combinaison dans le résultat
			res.add(new TreeSet<>(tmp));
		} else {
			for (int i = indexDepart; i <= liste.size() - longueur; i++) {
				// On créer un clone du tmp auquel on rajoute l'attribut choisi
				Set<Attribut> tmp2 = new TreeSet<>(tmp);
				tmp2.add(liste.get(i));
				generateCombinaisons(liste, longueur - 1, i + 1, res, tmp2);
			}
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((tuples == null) ? 0 : tuples.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Table other = (Table) obj;
		if (tuples == null) {
			if (other.tuples != null)
				return false;
		} else if (!tuples.equals(other.tuples))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Table [tuples=" + tuples + "]";
	}

	@Override
	public int getColumnCount() {
		if (this.tuples.size() < 1)
			return 1;

		return this.tuples.size();
	}

	@Override
	public int getRowCount() {
		if (this.tuples.size() < 1)
			return 0;

		return this.tuples.values().iterator().next().size();
	}

	@Override
	public Object getValueAt(int ligne, int colonne) {
		if (this.tuples.size() < 1)
			return "";

		List<Attribut> columNames = new LinkedList<>(this.tuples.keySet());

		return this.tuples.get(columNames.get(colonne)).get(ligne);
	}

	@Override
	public String getColumnName(int col) {
		if (this.tuples.size() < 1)
			return "";

		List<Attribut> columNames = new LinkedList<>(this.tuples.keySet());

		return columNames.get(col).toString();
	}
}