package ihm.modele.bd;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import ihm.modele.exceptions.AttributNonPresentException;
import ihm.modele.exceptions.EnsembleAttributsVideException;

/**
 * Classe permettant de créer un graphe avec des attributs comme noeuds
 * 
 * @author PICHON Thibaut
 * @version 0.0.1
 */
public class Graphe {

	private Map<Set<Attribut>, Set<Set<Attribut>>> listeAdjacence;
	private Set<Attribut> ensembleAttributs;

	/**
	 * Contructeur de la classe Graphe permettant de créer un graphe
	 * 
	 * @param ensembleAttributs l'ensemble d'attributs sans doublons
	 * 
	 * @throws IllegalArgumentException Exception levée si l'ensemble d'attributs
	 *                                  est nulle
	 */
	public Graphe(Set<Attribut> ensembleAttributs) throws IllegalArgumentException {
		if (ensembleAttributs == null)
			throw new IllegalArgumentException();

		this.listeAdjacence = new HashMap<>();
		this.ensembleAttributs = ensembleAttributs;
	}

	/**
	 * Contructeur de la classe Graphe permettant de clonner un graphe déjà existant
	 * 
	 * @param initial le graphe à cloner
	 * 
	 * @throws IllegalArgumentException Exception levée si le Graphe est nulle
	 */
	public Graphe(Graphe initial) throws IllegalArgumentException {
		if (initial == null)
			throw new IllegalArgumentException("Erreur le Graphe nulle");

		this.listeAdjacence = new HashMap<>();

		for (Set<Attribut> s : initial.donneCles()) {
			this.listeAdjacence.put(s, new HashSet<>(initial.successeurs(s)));
		}

		this.ensembleAttributs = initial.getEnsembleAttributs();
	}

	/**
	 * Accesseur permettant de récuperer la liste d'adjacence du graphe
	 * 
	 * Utiliser uniquement pour les tests
	 * 
	 * @return la liste d'adjacence du graphe
	 */
	protected Map<Set<Attribut>, Set<Set<Attribut>>> getListeAdjacence() {
		return this.listeAdjacence;
	}

	/**
	 * Accesseur permettant de récuperer la liste d'attributs du graphe
	 * 
	 * @return la liste d'attributs du graphe
	 */
	public Set<Attribut> getEnsembleAttributs() {
		return this.ensembleAttributs;
	}

	/**
	 * Méthode permettant de savoir si un arc est présent dans le graphe donc si il
	 * y a une dépendance att1 implique att2
	 * 
	 * @param att1 l'ensemble d'attribut à gauche de la dépendance
	 * @param att2 l'ensemble d'attribut à droite de la dépendance
	 * 
	 * @throws AttributNonPresentException exception levée si l'attribut n'est pas
	 *                                     présent dans le graphe
	 * 
	 * @return vrai si l'arc(dépendance) est présente, faux sinon
	 * 
	 * @throws EnsembleAttributsVideException Exception émise si l'ensemble
	 *                                        d'attributs est vide
	 * @throws IllegalArgumentException       Exception levée si l'ensemble
	 *                                        d'attributs est nulle
	 */
	public boolean estArc(Set<Attribut> att1, Set<Attribut> att2)
			throws AttributNonPresentException, IllegalArgumentException, EnsembleAttributsVideException {
		if (att1 == null || att2 == null)
			throw new IllegalArgumentException();

		if (att1.size() <= 0 || att2.size() <= 0)
			throw new EnsembleAttributsVideException();

		if (!this.ensembleAttributs.containsAll(att1) || !this.ensembleAttributs.containsAll(att2))
			throw new AttributNonPresentException();

		// Si le sommet n'est pas présent
		if (this.listeAdjacence.containsKey(att1))
			return this.listeAdjacence.get(att1).contains(att2);

		return false;
	}

	/**
	 * Méthode permettant de rajouter un arc dans le graphe, donc rajouter la
	 * dépendance att1 implique att2
	 * 
	 * @param att1 l'ensemble d'attribut à gauche de la dépendance
	 * @param att2 l'ensemble d'attribut à droite de la dépendance
	 * 
	 * @throws AttributNonPresentException    exception levée si l'attribut n'est
	 *                                        pas présent dans le graphe
	 * @throws EnsembleAttributsVideException Exception émise si l'ensemble
	 *                                        d'attributs est vide
	 * @throws IllegalArgumentException       Exception levée si l'ensemble
	 *                                        d'attributs est nulle
	 */
	public void ajouteArc(Set<Attribut> att1, Set<Attribut> att2)
			throws AttributNonPresentException, IllegalArgumentException, EnsembleAttributsVideException {
		if (att1 == null || att2 == null)
			throw new IllegalArgumentException();

		if (att1.size() <= 0 || att2.size() <= 0)
			throw new EnsembleAttributsVideException();

		if (!this.ensembleAttributs.containsAll(att1) || !this.ensembleAttributs.containsAll(att2))
			throw new AttributNonPresentException();

		/*
		 * Si la clé n'existe pas encore on la créer et on créer la liste des
		 * successeurs
		 */
		if (!this.listeAdjacence.containsKey(att1))
			this.listeAdjacence.put(att1, new HashSet<Set<Attribut>>());

		this.listeAdjacence.get(att1).add(att2);
	}

	/**
	 * Méthode permettant de supprimer un arc dans le graphe, donc supprimer la
	 * dépendance att1 impplique att2
	 * 
	 * @param att1 l'ensemble d'attribut à gauche de la dépendance
	 * @param att2 l'ensemble d'attribut à droite de la dépendance
	 * 
	 * @throws AttributNonPresentException    exception levée si l'attribut n'est
	 *                                        pas présent dans le graphe
	 * @throws EnsembleAttributsVideException Exception émise si l'ensemble
	 *                                        d'attributs est vide
	 * @throws IllegalArgumentException       Exception levée si l'ensemble
	 *                                        d'attributs est nulle
	 */
	public void supprimerArc(Set<Attribut> att1, Set<Attribut> att2)
			throws AttributNonPresentException, IllegalArgumentException, EnsembleAttributsVideException {
		if (att1 == null || att2 == null)
			throw new IllegalArgumentException();

		if (att1.size() <= 0 || att2.size() <= 0)
			throw new EnsembleAttributsVideException();

		if (!this.ensembleAttributs.containsAll(att1) || !this.ensembleAttributs.containsAll(att2))
			throw new AttributNonPresentException();

		if (this.listeAdjacence.containsKey(att1)) {
			this.listeAdjacence.get(att1).remove(att2);

			if (this.listeAdjacence.get(att1).size() == 0)
				this.listeAdjacence.remove(att1);
		}
	}

	/**
	 * Accesseur permettant d'obtenir la liste des clés de la liste d'adjacence du
	 * graphe
	 * 
	 * @return la liste des clés (sommet ayant des successeurs)
	 */
	public Set<Set<Attribut>> donneCles() {
		return this.listeAdjacence.keySet();
	}

	/**
	 * Méthode retournant la liste des succeurs d'un Sommet
	 * 
	 * @param sommet la liste des attributs à gauche dans les dépendances
	 * 
	 * @return la liste des parties gauches(liste de Attribut) des dépendances ayant
	 *         la partie droite
	 * 
	 * @throws IllegalArgumentException exception levée si le Sommet est nulle
	 */
	public Set<Set<Attribut>> successeurs(Set<Attribut> sommet) throws IllegalArgumentException {

		if (sommet == null)
			throw new IllegalArgumentException("Le sommet est nulle");

		if (this.listeAdjacence.keySet().contains(sommet))
			return this.listeAdjacence.get(sommet);
		return new HashSet<>();
	}

	/**
	 * Méthode permettant de récupérer la liste des descendants d'un sommet
	 * 
	 * @param sommet le sommet
	 * 
	 * @return la liste de ses descendants
	 */
	private Set<Set<Attribut>> descendants(Set<Attribut> sommet, Set<Set<Attribut>> liste) {
		Set<Set<Attribut>> descendants = new HashSet<>();
		descendants.addAll(liste);
		
		// Ajout des successeurs au descendants
		for (Set<Attribut> successeur : this.successeurs(sommet)) {
			descendants.add(successeur);
		}
		
		// Calcul des attributs actuellement disponible
		Set<Attribut> dispo = new TreeSet<>();
		for(Set<Attribut> descendant : descendants) {
			dispo.addAll(descendant);
		}
		
		// Ajout de tous les sommets contant tous les attributs aux descendants
		for (Set<Attribut> sg : this.donneCles()) {
			if (dispo.containsAll(sg))
				descendants.add(sg);
		}
		
		// Récupération de tous les descendants de tous les sommets
		Set<Set<Attribut>> tmp = new HashSet<>(descendants);
		for (Set<Attribut> descendant : descendants) {
			if (!liste.contains(descendant))
				tmp.addAll(this.descendants(descendant, descendants));
		}
		
		return tmp;
	}

	/**
	 * Méthode permettant de récupérer la liste des descendants d'un sommet
	 * 
	 * @param sommet le sommet
	 * 
	 * @return la liste de ses descendants
	 */
	public Set<Set<Attribut>> descendants(Set<Attribut> sommet) {
		Set<Set<Attribut>> liste = new HashSet<>();
		liste.add(sommet);
		
		return this.descendants(sommet, liste);
	}

	/**
	 * Méthode permettant d'obtenir la fermeture transitive du Graphe donc de
	 * l'ensemble de dépendances
	 * 
	 * @return la fermeture transitive du graphe
	 */
	public Graphe fermetureTransitive() {
		Graphe fermeture = new Graphe(this);

		for (Set<Attribut> sg : this.listeAdjacence.keySet()) {

			Set<Attribut> attsImpliques = new TreeSet<>();

			for (Set<Attribut> descendant : this.descendants(sg)) {

				for (Attribut attD : descendant) {
					if (!sg.contains(attD))
						attsImpliques.add(attD);
				}
			}

			for (Set<Attribut> sd : this.successeurs(sg)) {
				attsImpliques.removeAll(sd);
			}

			try {
				fermeture.ajouteArc(sg, attsImpliques);
			} catch (Exception e) {
				// NOTHING
			}
		}

		return fermeture;
	}

	/**
	 * Méthode qui retourne le sous-graphe induit par une liste d'attributs
	 * 
	 * @param attributsInduit l'ensemble d'attributs du sous-graphe induit
	 * 
	 * @return le sous-graphe induit par l'ensemble d'attributs
	 * 
	 * @throws IllegalArgumentException       si l'ensemble d'attributs est null
	 * @throws EnsembleAttributsVideException si l'ensemble d'attributs est vide
	 * @throws AttributNonPresentException    si l'ensemble d'attributs contient des
	 *                                        attributs non présents dans le graphe
	 */
	public Graphe grapheInduit(Set<Attribut> attributsInduit)
			throws IllegalArgumentException, EnsembleAttributsVideException, AttributNonPresentException {

		if (attributsInduit == null)
			throw new IllegalArgumentException();

		if (attributsInduit.size() == 0)
			throw new EnsembleAttributsVideException();

		if (!this.ensembleAttributs.containsAll(attributsInduit))
			throw new AttributNonPresentException();

		Graphe induit = new Graphe(attributsInduit);

		for (Set<Attribut> sg : this.listeAdjacence.keySet()) {
			if (attributsInduit.containsAll(sg)) {
				for (Set<Attribut> sd : this.listeAdjacence.get(sg)) {

					Set<Attribut> atts2 = new TreeSet<>();

					for (Attribut att : sd) {
						if (attributsInduit.contains(att))
							atts2.add(att);
					}

					if (atts2.size() > 0) {
						try {
							induit.ajouteArc(sg, atts2);
						} catch (AttributNonPresentException e) {
							// Normalement jamais atteint
							e.printStackTrace();
						}
					}
				}
			}
		}

		return induit;
	}

	/**
	 * Méthode permettant de vérifier si le Graphe (ensemble de dépendances)
	 * contient des dépendances non simples
	 * 
	 * @return true si le Graphe (ensemble de dépendances) contient des dépendances
	 *         non simples, faux sinon
	 */
	public boolean estSimple() {

		for (Set<Attribut> sg : this.donneCles()) { // récup sommet gauche
			for (Set<Attribut> sd : this.successeurs(sg)) { // récup sommet droit du sommet gauche concerné
				if (sd.size() > 1)
					return false;
			}
		}

		return true;
	}

	/**
	 * Méthode permettant de vérifier si le Graphe (ensemble de dépendances)
	 * contient des dépendances avec des attributs superflus
	 * 
	 * @return true si le Graphe (ensemble de dépendances) contient des dépendances
	 *         avec des attributs superflus, faux sinon
	 */
	public boolean sansSuperflu() {
		for (Set<Attribut> sg : this.donneCles()) {
			for (Set<Attribut> sd : this.successeurs(sg)) {
				for (Set<Attribut> sg2 : this.donneCles()) {
					if (!sg.equals(sg2)) {
						for (Set<Attribut> sd2 : this.successeurs(sg2)) {
							if (sd.equals(sd2) && (sg.containsAll(sg2) || sg2.containsAll(sg)))
								return false;
						}
					}
				}
			}
		}

		return true;
	}

	/**
	 * Méthode calculant la couverture minimale de l'ensemble de dépendances
	 * fonctionnelles
	 * 
	 * @return Graphe la couverture minimale
	 */
	public Graphe couvertureMinimale() {
		return Graphe.supprimerRedondances(
				Graphe.supprimerDependanceReflexive(Graphe.supprimerAttributsSuperflu(Graphe.dependancesSimple(this))));
	}

	public static Graphe dependancesSimple(Graphe g) {
		Graphe gSimple = null;

		try {
			gSimple = new Graphe(g.getEnsembleAttributs());
		} catch (Exception e1) {
		}

		for (Set<Attribut> sg : g.donneCles()) { // récup sommet gauche
			for (Set<Attribut> sd : g.successeurs(sg)) { // récup sommet droit du sommet gauche concerné
				if (sd.size() > 1) {
					for (Attribut att : sd) { // parcourt de tout les attributs
						Set<Attribut> attD = new TreeSet<>();
						attD.add(att);
						try {
							gSimple.ajouteArc(sg, attD); // ajout de l'arc dans le graph retour
						} catch (Exception e) {
							// NOTHING
						}
					}
				} else {
					try {
						gSimple.ajouteArc(sg, sd); // si exemple A -> B (un seul attribut) alors insérer l'arc
					} catch (Exception e) {
						// NOTHING
					}
				}
			}
		}

		return gSimple;
	}

	public static Graphe supprimerAttributsSuperflu(Graphe g) {
		Graphe gSansSuperflu = null;

		try {
			gSansSuperflu = new Graphe(g); // clonage du graphe

			for (Set<Attribut> sg : g.donneCles()) {

				for (Set<Attribut> sd : g.successeurs(sg)) {

					for (Set<Attribut> sg2 : g.donneCles()) {

						if (!sg.equals(sg2)) {

							for (Set<Attribut> sd2 : g.successeurs(sg2)) {

								if (sd.equals(sd2)) {

									if (sg.containsAll(sg2)) {
										gSansSuperflu.supprimerArc(sg, sd);
									} else if (sg2.containsAll(sg)) {
										gSansSuperflu.supprimerArc(sg2, sd);
									}

								}
							}
						}
					}
				}
			}
		} catch (

		Exception e1) {
		}

		return gSansSuperflu;
	}

	/**
	 * Méthode permettant de supprimer les dépendances réflexives d'un graphe
	 * 
	 * @param g le graphe
	 * 
	 * @return le graphe sans les dépendances réflexifs
	 */
	public static Graphe supprimerDependanceReflexive(Graphe g) {
		Graphe gSansReflexif = null;

		gSansReflexif = new Graphe(g); // clonage du graphe

		for (Set<Attribut> sg : g.donneCles()) {

			for (Set<Attribut> sd : g.successeurs(sg)) {

				if (sg.containsAll(sd)) {
					try {
						gSansReflexif.supprimerArc(sg, sd);
					} catch (Exception e) {
						// NOTHING
					}
				}
			}
		}

		return gSansReflexif;
	}

	/**
	 * Méthode de classe permettant de supprimer les redondances d'un graphe
	 * 
	 * @param g le graphe
	 * 
	 * @return le graphe sans les dépendances redondantes
	 */
	public static Graphe supprimerRedondances(Graphe g) {
		Graphe gSansRedondances = null;

		try {
			gSansRedondances = new Graphe(g);
		} catch (Exception e1) {
		}

		for (Set<Attribut> sg : g.donneCles()) {
			for (Set<Attribut> sd : g.successeurs(sg)) {
				Graphe bis = new Graphe(gSansRedondances);
				try {
					bis.supprimerArc(sg, sd);
				} catch (Exception e) {
					e.printStackTrace();
				}
				if (Graphe.dependancesSimple(bis.fermetureTransitive())
						.equals(Graphe.dependancesSimple(gSansRedondances.fermetureTransitive()))) {
					gSansRedondances = bis;
				}
			}

		}

		return gSansRedondances;

	}

	/**
	 * Méthode permettant de vérifier si le Graphe (ensemble de dépendances)
	 * contient des redondances
	 * 
	 * @return true si le Graphe (ensemble de dépendances) contient des redondances,
	 *         faux sinon
	 */
	public boolean contientRedondances() {
		for (Set<Attribut> sg : this.donneCles()) {
			for (Set<Attribut> sd : this.successeurs(sg)) {
				Graphe bis = new Graphe(this);
				try {
					bis.supprimerArc(sg, sd);
				} catch (Exception e) {
					e.printStackTrace();
				}
				if (bis.fermetureTransitive().equals(this.fermetureTransitive()))
					return true;
			}
		}

		return false;
	}

	/**
	 * Méthode calculant le hasCode du Graphe
	 * 
	 * @return le hashCode du Graphe
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((ensembleAttributs == null) ? 0 : ensembleAttributs.hashCode());
		result = prime * result + ((listeAdjacence == null) ? 0 : listeAdjacence.hashCode());
		return result;
	}

	/**
	 * Méthode testant si deux Graphe sont identiques, même ensemble d'attributs et
	 * même arcs et sommets
	 * 
	 * @return vrai si les Graphes sont identiques, faux sinon
	 */
	@Override
	public boolean equals(Object obj) {

		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;

		Graphe other = (Graphe) obj;

		if (this.ensembleAttributs == null) {
			if (other.getEnsembleAttributs() != null)
				return false;
			else
				return true;
		} else if (!this.ensembleAttributs.equals(other.getEnsembleAttributs()))
			return false;

		if (this.listeAdjacence == null) {
			if (other.getEnsembleAttributs() != null)
				return false;
			else
				return true;
		} else {
			if (this.listeAdjacence.keySet().size() != other.donneCles().size())
				return false;

			for (Set<Attribut> s : this.listeAdjacence.keySet()) {
				try {
					if (this.successeurs(s).size() != other.successeurs(s).size())
						return false;
					if (!this.successeurs(s).containsAll(other.successeurs(s))) {
						return false;
					}
				} catch (Exception e) {
					// On ne fait rien
				}
			}

			return true;
		}
	}

	@Override
	public String toString() {
		String str = "";

		for (Set<Attribut> sg : this.listeAdjacence.keySet()) {
			for (Set<Attribut> sd : this.listeAdjacence.get(sg)) {
				if (!str.trim().isEmpty())
					str += "; ";
				String strSG = sg.toString();
				String strSD = sd.toString();
				str += (strSG.substring(1, strSG.length() - 1) + "->" + strSD.substring(1, strSD.length() - 1))
						.replace(" ,", ",").replace(", ", ",");
			}
		}

		return "{" + str + "}";
	}

}