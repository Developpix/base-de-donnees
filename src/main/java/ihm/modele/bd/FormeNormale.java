package ihm.modele.bd;

/**
 * Enumération représentant la forme normale d'une relation
 * 
 * @author PICHON Thibaut
 * @version 1.0
 */
public enum FormeNormale {

	/**
	 * La Relation est en 1NF
	 */
	NF1("1NF"),

	/**
	 * La Relation est en 2NF
	 */
	NF2("2NF"),

	/**
	 * La Relation est en 3NF
	 */
	NF3("3NF"),

	/**
	 * La Relation est en 3NFBC
	 */
	NFBC3("3NFBC");

	private String name;

	FormeNormale(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return this.name;
	}
}
