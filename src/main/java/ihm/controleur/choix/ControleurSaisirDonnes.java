package ihm.controleur.choix;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import ihm.modele.bd.Table;
import ihm.modele.utilisateur.Compte;
import ihm.vue.VueChoix;
import ihm.vue.VueSaisieRelation;
import ihm.vue.VueSaisieTuples;

/**
 * Classe permettant de créer un controleur permettant de gérer le début de la
 * saisie des données (relation ou tuples)
 * 
 * @author Thibaut PICHON
 * @version 0.1
 */
public class ControleurSaisirDonnes implements ActionListener {

	private Compte modele;
	private VueChoix vue;

	/**
	 * Constructeur de la classe
	 * 
	 * @param vueChoix la vue
	 * @param modele   le modèle
	 */
	public ControleurSaisirDonnes(VueChoix vueChoix, Compte modele) {
		this.modele = modele;
		this.vue = vueChoix;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		this.vue.setVisible(false);
		this.vue.dispose();

		if (this.vue.donneChoixGenerateurSaisie().equals("Relation")) {
			new VueSaisieRelation(this.modele);
		} else {
			this.modele.getExerciceEnCours().setTuples(new Table());

			new VueSaisieTuples(this.modele);
		}
	}
}
