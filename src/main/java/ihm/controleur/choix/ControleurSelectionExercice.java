package ihm.controleur.choix;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import com.google.gson.JsonParseException;

import ihm.modele.database.dao.DAOExercice;
import ihm.modele.exceptions.ConnexionException;
import ihm.modele.exceptions.ExerciceInexistantException;
import ihm.vue.VueChoix;

/**
 * Classe permettant de créer un controleur permettant de gérer la selection
 * d'un exercice
 * 
 * @author Thibaut PICHON
 * @version 0.1
 */
public class ControleurSelectionExercice implements ActionListener {

	private VueChoix vue;

	/**
	 * Constructeur de la classe
	 * 
	 * @param vueChoix la vue
	 */
	public ControleurSelectionExercice(VueChoix vueChoix) {
		this.vue = vueChoix;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		int idExo = this.vue.donneExerciceSelectionne();

		try {
			DAOExercice daoExo = new DAOExercice();

			this.vue.miseAJourAffichageExo(daoExo.donnerExerciceParID(idExo));
		} catch (JsonParseException e) {
			// NOTHING
		} catch (IOException e) {
			// NOTHING
		} catch (IllegalArgumentException e) {
			// NOTHING
		} catch (ExerciceInexistantException e) {
			// NOTHING
		} catch (ConnexionException e1) {
			this.vue.afficherMessageErreur("Problème de connexion", "Impossible de se connecter à la base de données");
		}
	}
}
