package ihm.controleur.choix;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import com.google.gson.JsonParseException;

import ihm.modele.database.dao.DAOExercice;
import ihm.modele.exceptions.ConnexionException;
import ihm.modele.exceptions.ExerciceInexistantException;
import ihm.modele.utilisateur.Compte;
import ihm.modele.utilisateur.Etape;
import ihm.modele.utilisateur.Exercice;
import ihm.vue.VueChoix;
import ihm.vue.VueRecherche;
import ihm.vue.VueSaisieRelation;

/**
 * Classe permettant de créer un controleur permettant de générer le
 * commencement d'un exercice par l'utilisateur
 * 
 * @author Thibaut PICHON
 * @version 0.1
 */
public class ControleurCommencerExercice implements ActionListener {

	private Compte modele;
	private VueChoix vue;

	/**
	 * Constructeur de la classe
	 * 
	 * @param vueChoix la vue
	 * @param modele le modèle
	 */
	public ControleurCommencerExercice(VueChoix vueChoix, Compte modele) {
		this.modele = modele;
		this.vue = vueChoix;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		int idExo = this.vue.donneExerciceSelectionne();

		if (idExo >= 0) {
			try {
				DAOExercice daoExo = new DAOExercice();
				Exercice exo = daoExo.donnerExerciceParID(idExo);
				this.modele.setExercice(exo);

				this.vue.setVisible(false);
				this.vue.dispose();

				if (exo.getTuples() != null)
					new VueSaisieRelation(this.modele);
				else {
					this.modele.definirEtatEtape(Etape.Relation, 0);
					new VueRecherche(this.modele);
				}
			} catch (IllegalArgumentException | ExerciceInexistantException e1) {
				e1.printStackTrace();
			} catch (JsonParseException e1) {
				e1.printStackTrace();
			} catch (IOException e1) {
				e1.printStackTrace();
			} catch (ConnexionException e1) {
				this.vue.afficherMessageErreur("Problème de connexion", "Impossible de se connecter à la base de données");
			}

		}
	}
}
