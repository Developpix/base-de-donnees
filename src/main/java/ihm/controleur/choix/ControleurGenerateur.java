package ihm.controleur.choix;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import ihm.modele.utilisateur.Compte;
import ihm.vue.VueChoix;
import ihm.vue.VueGenerateur;

/**
 * Classe permettant de créer un controleur permettant de gérer la transition
 * vers le générateur
 * 
 * @author Thibaut PICHON
 * @version 0.1
 */
public class ControleurGenerateur implements ActionListener {

	private Compte modele;
	private VueChoix vue;

	/**
	 * Constructeur de la classe
	 * 
	 * @param vueChoix la vue
	 * @param modele   le modèle
	 */
	public ControleurGenerateur(VueChoix vueChoix, Compte modele) {
		this.modele = modele;
		this.vue = vueChoix;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		this.vue.setVisible(false);
		this.vue.dispose();

		new VueGenerateur(this.modele, this.vue.donneChoixGenerateurSaisie().equals("Tuples"));
	}
}
