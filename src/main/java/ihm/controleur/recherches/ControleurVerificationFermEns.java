package ihm.controleur.recherches;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Set;

import ihm.modele.bd.Attribut;
import ihm.modele.utilisateur.Compte;
import ihm.modele.utilisateur.Etape;
import ihm.modele.utilisateur.Exercice;
import ihm.modele.utilisateur.Parseur;
import ihm.vue.VueRecherche;

/**
 * Classe permettant de créer un controleur gérant la vérifiant la fermeture
 * d'un ensemble d'attributs saisie par l'utilisateur
 * 
 * @author Thibaut PICHON
 * @version 0.1
 */
public class ControleurVerificationFermEns implements ActionListener {

	private Compte modele;
	private VueRecherche vue;

	/**
	 * Contructeur de la classe
	 * 
	 * @param modele le Graphe pour la fermeture de l'ensemble
	 * @param vue    la vue
	 */
	public ControleurVerificationFermEns(Compte modele, VueRecherche vue) {
		this.modele = modele;
		this.vue = vue;
	}

	@Override
	public void actionPerformed(ActionEvent event) {
		String ensNetoyee = Parseur.nettoyerNomAttribut(this.vue.donneEnsembleAttsPourFermEns());
		String fermEnsNetoyee = Parseur.nettoyerNomAttribut(this.vue.donneFermetureEns());

		// On stocke l'exercice dans une variable
		Exercice exoEnCours = this.modele.getExerciceEnCours();

		if (ensNetoyee.charAt(0) != '{') {
			this.vue.afficherMessage("Ensemble d'attributs saisi invalide",
					"L'ensemble d'attributs saisi ne commence pas par {");
		} else if (ensNetoyee.charAt(ensNetoyee.length() - 1) != '}') {
			this.vue.afficherMessage("Ensemble d'attributs saisi invalide",
					"L'ensemble d'attributs saisi ne se termine pas par }");
		} else if (fermEnsNetoyee.charAt(0) != '{') {
			this.vue.afficherMessage("Ensemble d'attributs saisi invalide",
					"L'ensemble d'attributs saisi ne commence pas par {");
		} else if (fermEnsNetoyee.charAt(fermEnsNetoyee.length() - 1) != '}') {
			this.vue.afficherMessage("Ensemble d'attributs saisi invalide",
					"L'ensemble d'attributs saisi ne se termine pas par }");
		} else { // a->b; a->c; b->c
			Set<Attribut> ens = Parseur.convertirSetAttributs(ensNetoyee);
			Set<Attribut> attendu = exoEnCours.getRelation().fermetureEnsemble(ens);
			Set<Attribut> fermEns = Parseur.convertirSetAttributs(fermEnsNetoyee);

			int i = this.modele.donnerEtatEtape(Etape.FermEns);
			
			if (attendu.equals(fermEns)) {
				this.modele.definirEtatEtape(Etape.FermEns, i);
				this.vue.validerSaisieFermEns();
			} else {
				if (i < 2)
					this.vue.validerSaisieFermEns(
							"La fermeture de l'ensemble est invalide (" + (2 - i) + " tentatives restantes)");
				else
					this.vue.validerSaisieFermEns("E⁺ = " + attendu.toString().replace("[", "{").replace("]", "}"));

				if (i < 3)
					this.modele.definirEtatEtape(Etape.FermEns, i + 1);
			}

			this.vue.miseAJourProgression();
		}

	}
}
