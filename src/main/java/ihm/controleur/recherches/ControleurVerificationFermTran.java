package ihm.controleur.recherches;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import ihm.modele.bd.Graphe;
import ihm.modele.exceptions.AttributNonPresentException;
import ihm.modele.exceptions.EnsembleDependancesInvalideException;
import ihm.modele.utilisateur.Compte;
import ihm.modele.utilisateur.Etape;
import ihm.modele.utilisateur.Exercice;
import ihm.modele.utilisateur.Parseur;
import ihm.vue.VueRecherche;

/**
 * Classe permettant de créer un controleur gérant la vérifiant la fermeture
 * transitive saisie par l'utilisateur
 * 
 * @author Thibaut PICHON
 * @version 0.1
 */
public class ControleurVerificationFermTran implements ActionListener {

	private Compte modele;
	private VueRecherche vue;

	/**
	 * Contructeur de la classe
	 * 
	 * @param modele le Graphe stockant la fermeture transitive
	 * @param vue    la vue
	 */
	public ControleurVerificationFermTran(Compte modele, VueRecherche vue) {
		this.modele = modele;
		this.vue = vue;
	}

	@Override
	public void actionPerformed(ActionEvent event) {
		String nomEnsemble = "(ensemble de dépendances de base)";

		// On stocke l'exercice dans une variable
		Exercice exoEnCours = this.modele.getExerciceEnCours();

		try {
			Graphe attendu = Parseur
					.convertirEnGraphe(exoEnCours.getRelation().getAttributs(), this.vue.donneEnsemblePourFermTran())
					.fermetureTransitive();

			nomEnsemble = "(fermeture transitive)";

			int i = this.modele.donnerEtatEtape(Etape.FermTran);

			if (Parseur.convertirEnGraphe(exoEnCours.getRelation().getAttributs(), this.vue.donneFermTran())
					.equals(attendu)) {
				this.modele.definirEtatEtape(Etape.FermTran, i);
				this.vue.validerSaisieFermTran();
			} else {
				if (i < 2)
					this.vue.validerSaisieFermTran(
							"La fermeture transitive est invalide (" + (2 - i) + " tentatives restantes)");
				else
					this.vue.validerSaisieFermTran("F⁺ = " + attendu.toString());

				if (i < 3)
					this.modele.definirEtatEtape(Etape.FermTran, i + 1);
			}

			this.vue.miseAJourProgression();
		} catch (AttributNonPresentException e) {
			this.vue.afficherMessage("Attributs non présent dans la relation", "Ensemble de dépendances " + nomEnsemble
					+ " contient des attributs non présent dans la relation de base");
		} catch (EnsembleDependancesInvalideException e) {
			this.vue.afficherMessage("Ensemble de dépendances saisie invalide", nomEnsemble + e.getMessage());
		}

	}
}
