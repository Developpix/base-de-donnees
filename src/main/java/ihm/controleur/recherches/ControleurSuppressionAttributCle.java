package ihm.controleur.recherches;

import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import ihm.modele.bd.Attribut;
import ihm.modele.utilisateur.Compte;
import ihm.modele.utilisateur.Exercice;
import ihm.vue.VueRecherche;

/**
 * Classe permettant de créer un controleur gérant la suppression d'un attribut
 * de la clé saisie par l'utilisateur
 * 
 * @author PICHON Thibaut
 * @version 0.1
 */
public class ControleurSuppressionAttributCle implements ListSelectionListener {

	private Compte modele;
	private VueRecherche vue;

	/**
	 * Constructeur de la classe
	 * 
	 * @param modele le modèle
	 * @param vue    la vue
	 */
	public ControleurSuppressionAttributCle(Compte modele, VueRecherche vue) {
		this.modele = modele;
		this.vue = vue;
	}

	@Override
	public void valueChanged(ListSelectionEvent e) {
		Attribut att = this.vue.donneAttributCleSelectionne();

		// On stocke l'exercice dans une variable
		Exercice exoEnCours = this.modele.getExerciceEnCours();

		if (att != null) {
			exoEnCours.reinitialiserCle();
			exoEnCours.getCle().remove(att);

			this.vue.miseAJourSaisieCle();
			this.vue.miseAJourAffichageValidationCle();
		}

	}
}
