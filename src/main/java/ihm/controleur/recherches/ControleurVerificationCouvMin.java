package ihm.controleur.recherches;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import ihm.modele.bd.Graphe;
import ihm.modele.exceptions.AttributNonPresentException;
import ihm.modele.exceptions.EnsembleDependancesInvalideException;
import ihm.modele.utilisateur.Compte;
import ihm.modele.utilisateur.Etape;
import ihm.modele.utilisateur.Exercice;
import ihm.modele.utilisateur.Parseur;
import ihm.vue.VueRecherche;

/**
 * Classe permettant de créer un controleur gérant la vérifiant la couverture
 * minimale saisie par l'utilisateur
 * 
 * @author Thibaut PICHON
 * @version 0.1
 */
public class ControleurVerificationCouvMin implements ActionListener {

	private Compte modele;
	private VueRecherche vue;

	/**
	 * Contructeur de la classe
	 * 
	 * @param modele le Graphe stockant la couverture minimale
	 * @param vue    la vue
	 */
	public ControleurVerificationCouvMin(Compte modele, VueRecherche vue) {
		this.modele = modele;
		this.vue = vue;
	}

	@Override
	public void actionPerformed(ActionEvent event) {
		// On stocke l'exercice dans une variable
		Exercice exoEnCours = this.modele.getExerciceEnCours();

		try {
			Graphe attendu = exoEnCours.getRelation().couvertureMinimale();

			Graphe couvMin = Parseur.convertirEnGraphe(exoEnCours.getRelation().getAttributs(),
					this.vue.donneCouvMin());

			int i = this.modele.donnerEtatEtape(Etape.CouvMin);

			if (couvMin.estSimple() && couvMin.sansSuperflu() && !couvMin.contientRedondances() && exoEnCours
					.getRelation().couvertureMinimale().fermetureTransitive().equals(couvMin.fermetureTransitive())) {
				this.vue.validerSaisieCouvMin();
				this.modele.definirEtatEtape(Etape.CouvMin, i);
				exoEnCours.setCouvMin(couvMin);
			} else {
				if (i < 2) {
					this.vue.validerSaisieCouvMin(
							"La couverture minimale est invalide (" + (2 - i) + " tentatives restantes)");
				} else {
					this.vue.validerSaisieCouvMin("Fmin = " + attendu.toString());
					exoEnCours.setCouvMin(attendu);
				}
				
				if (i < 3)
					this.modele.definirEtatEtape(Etape.CouvMin, i + 1);
			}

			// Si la clé et la couverture minimale sont définis
			if (!exoEnCours.getCle().isEmpty() && exoEnCours.getCouvMin() != null)
				this.vue.activerSuivant(true);

			this.vue.miseAJourProgression();
		} catch (AttributNonPresentException e) {
			this.vue.afficherMessage("Attributs non présent dans la relation",
					"Ensemble de dépendances contient des attributs non présent dans la relation de base");
		} catch (EnsembleDependancesInvalideException e) {
			this.vue.afficherMessage("Ensemble de dépendances saisie invalide", e.getMessage());
		}

	}
}
