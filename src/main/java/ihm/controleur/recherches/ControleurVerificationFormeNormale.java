package ihm.controleur.recherches;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import ihm.modele.bd.FormeNormale;
import ihm.modele.utilisateur.Compte;
import ihm.modele.utilisateur.Etape;
import ihm.modele.utilisateur.Exercice;
import ihm.vue.VueRecherche;

/**
 * Classe permettant de créer un controleur permettant de valider la forme
 * normale saisie
 * 
 * @author PICHON Thibaut
 * @version 0.1
 */
public class ControleurVerificationFormeNormale implements ActionListener {

	private Compte modele;
	private VueRecherche vue;

	/**
	 * Constructeur de la classe
	 * 
	 * @param modele le modèle
	 * @param vue    la vue
	 */
	public ControleurVerificationFormeNormale(Compte modele, VueRecherche vue) {
		this.modele = modele;
		this.vue = vue;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		// On stocke l'exercice dans une variable
		Exercice exoEnCours = this.modele.getExerciceEnCours();

		if (!exoEnCours.getCle().isEmpty()) {
			FormeNormale attendu = exoEnCours.getRelation().rechercheformeNormale(exoEnCours.getCle());

			int i = this.modele.donnerEtatEtape(Etape.FormeNormale);
			
			if (this.vue.donneNF().equals(attendu)) {
				this.modele.definirEtatEtape(Etape.FormeNormale, i);
				this.vue.validerSaisieFormeNormale();
			} else {
				if (i < 2)
					this.vue.validerSaisieFormeNormale(
							"La forme normale est invalide (" + (2 - i) + " tentatives restantes)");
				else
					this.vue.validerSaisieFormeNormale(attendu.toString());
				
				if (i < 3)
					this.modele.definirEtatEtape(Etape.FormeNormale, i + 1);
			}

			this.vue.miseAJourProgression();
		} else {
			this.vue.afficherMessage("Impossible calculer de vérifier normale",
					"La clé est nécessaire pour vérifier la forme normale");
		}
	}
}
