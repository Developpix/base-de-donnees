package ihm.controleur.recherches;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Set;
import java.util.TreeSet;

import ihm.modele.bd.Attribut;
import ihm.modele.utilisateur.Compte;
import ihm.modele.utilisateur.Etape;
import ihm.modele.utilisateur.Exercice;
import ihm.vue.VueRecherche;

/**
 * Classe permettant de créer un controleur gérant la vérification de la clé
 * saisie
 * 
 * @author PICHON Thibaut
 * @version 0.1
 */
public class ControleurVerificationCle implements ActionListener {

	private Compte modele;
	private VueRecherche vue;

	/**
	 * Constructeur de la classe
	 * 
	 * @param modele le modèle
	 * @param vue    la vue
	 */
	public ControleurVerificationCle(Compte modele, VueRecherche vue) {
		this.modele = modele;
		this.vue = vue;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		// On stocke l'exercice dans une variable
		Exercice exoEnCours = this.modele.getExerciceEnCours();

		Set<Attribut> attsRelation = exoEnCours.getRelation().getAttributs();
		Set<Attribut> cleSaisie = exoEnCours.getCle();

		int i = this.modele.donnerEtatEtape(Etape.Cle);

		// Si la clé saisie permet de retrouver tous les attributs
		if (exoEnCours.getRelation().fermetureEnsemble(cleSaisie).equals(attsRelation)) {
			// On retire tous les attributs supperflu de la clé saisie
			Set<Attribut> cleNetoyee = new TreeSet<>(cleSaisie);
			for (Attribut att : cleSaisie) {
				Set<Attribut> tmp = new TreeSet<>(cleNetoyee);
				tmp.remove(att);

				if (exoEnCours.getRelation().fermetureEnsemble(tmp).equals(attsRelation))
					cleNetoyee.remove(att);
			}

			// Si la clé saisie ne contient pas d'attributs superflus on affiche correct
			if (cleSaisie.equals(cleNetoyee) && i < 3) {
				this.modele.definirEtatEtape(Etape.Cle, i);
				this.vue.validerSaisieCle();
			} else {
				if (i < 2)
					this.vue.validerSaisieCle("La clé est invalide (" + (2 - i) + " tentatives restantes)");
				else if (exoEnCours.getCle().isEmpty()) {
					// Sinon on affiche la clé sans les attributs superflus
					this.vue.validerSaisieCle("Cle = " + cleNetoyee.toString());
					exoEnCours.setCle(cleNetoyee);
				}

				if (i < 3)
					this.modele.definirEtatEtape(Etape.Cle, i + 1);
			}
		} else {
			if (i < 2)
				this.vue.validerSaisieCle("La clé est invalide (" + (2 - i) + " tentatives restantes)");
			else if (exoEnCours.getCle().isEmpty()) {
				// Sinon on affiche la clé sans les attributs superflus
				this.vue.validerSaisieCle("Cle = " + exoEnCours.getRelation().rechercheCle().toString());
				exoEnCours.setCle(exoEnCours.getRelation().rechercheCle());
			}

			if (i < 3)
				this.modele.definirEtatEtape(Etape.Cle, i + 1);
		}

		// Si la clé et la couverture minimale sont définis
		if (!exoEnCours.getCle().isEmpty() && exoEnCours.getCouvMin() != null)
			this.vue.activerSuivant(true);

		this.vue.miseAJourProgression();
	}
}
