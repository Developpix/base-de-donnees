package ihm.controleur.transitions;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JFrame;

import com.google.gson.JsonParseException;

import ihm.modele.database.dao.DAOTravail;
import ihm.modele.exceptions.ConnexionException;
import ihm.modele.utilisateur.Compte;
import ihm.vue.VueGestionCompte;

/**
 * Classe permettant de créer un controleur permettant d'aller à la vue
 * d'authentification
 * 
 * @author Jobard Mathis
 * @version 0.1
 */
public class ControleurAllerGestionDuCompte implements ActionListener {

	private Compte modele;
	private JFrame vue;

	/**
	 * Constructeur de la classe
	 * 
	 * @param modele le modèle
	 * @param vue    la vue
	 */
	public ControleurAllerGestionDuCompte(Compte modele, JFrame vue) {
		this.modele = modele;
		this.vue = vue;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		this.vue.setVisible(false);
		this.vue.dispose();

		try {
			DAOTravail daoTravail = new DAOTravail();
			List<Integer> taches = new LinkedList<>(daoTravail.donnerExosCompte(this.modele).keySet());

			Integer[] listeTaches = new Integer[taches.size()];
			for (int i = 0; i < taches.size(); i++) {
				listeTaches[i] = taches.get(i);
			}
			
			new VueGestionCompte(this.modele, listeTaches);
		} catch (JsonParseException | IOException e) {
			e.printStackTrace();
		} catch (ConnexionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
