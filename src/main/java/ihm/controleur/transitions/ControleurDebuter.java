package ihm.controleur.transitions;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.List;

import com.google.gson.JsonParseException;

import ihm.modele.database.dao.DAOExercice;
import ihm.modele.exceptions.ConnexionException;
import ihm.modele.utilisateur.Compte;
import ihm.modele.utilisateur.Exercice;
import ihm.vue.VueAuthentification;
import ihm.vue.VueBienvenue;
import ihm.vue.VueChoix;

/**
 * Classe permettant de créer un controleur permettant d'aller à la vue
 * d'authentification
 * 
 * @author Jobard Mathis
 * @version 0.1
 */
public class ControleurDebuter implements ActionListener {

	private Compte modele;
	private VueBienvenue vue;

	/**
	 * Constructeur de la classe
	 * 
	 * @param modele le modèle
	 * @param vue    la vue
	 */
	public ControleurDebuter(Compte modele, VueBienvenue vue) {
		this.modele = modele;
		this.vue = vue;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		this.vue.setVisible(false);
		this.vue.dispose();

		if (this.modele == null)
			new VueAuthentification();
		else {
			this.modele.nouvelleExercice();
			Integer[] listeExos = new Integer[0];

			try {
				DAOExercice daoExos = new DAOExercice();
				List<Exercice> exos = daoExos.donnerTous();

				listeExos = new Integer[exos.size()];
				for (int i = 0; i < exos.size(); i++) {
					listeExos[i] = exos.get(i).getID();
				}
			} catch (JsonParseException e) {
				// NOTHING
			} catch (IOException e) {
				// NOTHING
			} catch (ConnexionException e) {
				this.vue.afficherMessage("Problème de connexion", "Impossible de se connecter à la base de données");
			}

			new VueChoix(this.modele, listeExos);
		}
	}
}
