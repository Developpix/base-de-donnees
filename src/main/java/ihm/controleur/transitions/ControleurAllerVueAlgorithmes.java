package ihm.controleur.transitions;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;

import ihm.modele.utilisateur.Compte;
import ihm.vue.VueAlgorithmes;

/**
 * Classe permettant de créer un controleur permettant d'aller à la vue des
 * algorithmes
 * 
 * @author PICHON Thibaut
 * @version 0.1
 */
public class ControleurAllerVueAlgorithmes implements ActionListener {

	private Compte modele;
	private JFrame vue;

	/**
	 * Constructeur de la classe
	 * 
	 * @param modele le modèle
	 * @param vue    la vue
	 */
	public ControleurAllerVueAlgorithmes(Compte modele, JFrame vue) {
		this.modele = modele;
		this.vue = vue;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		this.vue.setVisible(false);
		this.vue.dispose();

		new VueAlgorithmes(this.modele);
	}
}
