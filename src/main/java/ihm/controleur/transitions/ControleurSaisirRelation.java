package ihm.controleur.transitions;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;

import ihm.modele.utilisateur.Compte;
import ihm.vue.VueSaisieRelation;

/**
 * Classe permettant de créer un controleur permettant de gérer le lancement de
 * la saisie d'une relation
 * 
 * @author Thibaut PICHON
 * @version 0.1
 */
public class ControleurSaisirRelation implements ActionListener {

	private Compte modele;
	private JFrame vue;

	/**
	 * Constructeur de la classe
	 * 
	 * @param vue    la vue
	 * @param modele le modèle
	 */
	public ControleurSaisirRelation(JFrame vue, Compte modele) {
		this.modele = modele;
		this.vue = vue;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		this.vue.setVisible(false);
		this.vue.dispose();

		new VueSaisieRelation(this.modele);
	}
}
