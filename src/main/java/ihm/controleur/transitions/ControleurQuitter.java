package ihm.controleur.transitions;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;

/**
 * Classe permettant de créer le controleur redirigeant vers la vue de bienvenue
 * 
 * @author PICHON Thibaut
 * @version 1.0
 */
public class ControleurQuitter implements ActionListener {

	private JFrame vue;

	/**
	 * Constructeur du controleur
	 * 
	 * @param vue la vue en cours
	 */
	public ControleurQuitter(JFrame vue) {
		this.vue = vue;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		this.vue.setVisible(false);
		this.vue.dispose();
	}
}