package ihm.controleur.transitions;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.List;

import com.google.gson.JsonParseException;

import ihm.modele.database.dao.DAOCompte;
import ihm.modele.database.dao.DAOExercice;
import ihm.modele.database.dao.DAOTravail;
import ihm.modele.exceptions.ConnexionException;
import ihm.modele.utilisateur.Compte;
import ihm.modele.utilisateur.Exercice;
import ihm.vue.VueAuthentification;
import ihm.vue.VueChoix;

/**
 * Controleur pour se connecter à l'application
 * 
 * @author Mathis Jobard
 * @version 0.1
 */
public class ControleurConnexion implements ActionListener {

	private VueAuthentification vue;

	/**
	 * Constructeur de la classe
	 * 
	 * @param vue la vue
	 */
	public ControleurConnexion(VueAuthentification vue) {
		this.vue = vue;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		if (!this.vue.donneLogin().isEmpty() && !this.vue.donnePassword().isEmpty()) {
			Compte compte = new Compte(this.vue.donneLogin(), this.vue.donnePassword());

			try {
				DAOCompte daocompte = new DAOCompte();

				if (daocompte.authentifier(compte)) {
					this.vue.setVisible(false);
					this.vue.dispose();

					compte.nouvelleExercice();

					DAOExercice daoExos = new DAOExercice();
					List<Exercice> exos = daoExos.donnerTous();

					Integer[] listeExos = new Integer[exos.size()];
					for (int i = 0; i < exos.size(); i++) {
						listeExos[i] = exos.get(i).getID();
					}

					// Chargement des exercices réalisés
					DAOTravail daoTravail = new DAOTravail();
					compte.setExosRealises(daoTravail.donnerExosCompte(compte));

					new VueChoix(compte, listeExos);
				} else {
					this.vue.afficherMessageErreur("Erreur", "Compte non existant");
				}
			} catch (JsonParseException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (ConnexionException e) {
				this.vue.afficherMessageErreur("Problème de connexion", "Impossible de se connecter à la base de données");
			}
		} else {
			this.vue.afficherMessageErreur("Erreur", "Champs vides");
		}
	}
}