package ihm.controleur.transitions;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.List;

import javax.swing.JFrame;

import com.google.gson.JsonParseException;

import ihm.modele.database.dao.DAOExercice;
import ihm.modele.exceptions.ConnexionException;
import ihm.modele.utilisateur.Compte;
import ihm.modele.utilisateur.Exercice;
import ihm.vue.VueChoix;

/**
 * Classe permettant de créer le controleur redirigeant vers la vue de bienvenue
 * 
 * @author PICHON Thibaut
 * @version 1.0
 */
public class ControleurAllerVueChoix implements ActionListener {

	private Compte modele;
	private JFrame vue;

	/**
	 * Constructeur du controleur
	 * 
	 * @param modele le modèle
	 * @param vue    la vue en cours
	 */
	public ControleurAllerVueChoix(Compte modele, JFrame vue) {
		this.modele = modele;
		this.vue = vue;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		this.vue.setVisible(false);
		this.vue.dispose();

		try {
			DAOExercice daoExos = new DAOExercice();
			List<Exercice> exos = daoExos.donnerTous();

			Integer[] listeExos = new Integer[exos.size()];
			for (int i = 0; i < exos.size(); i++) {
				listeExos[i] = exos.get(i).getID();
			}

			this.modele.nouvelleExercice();

			new VueChoix(this.modele, listeExos);
		} catch (JsonParseException | IOException e) {
			e.printStackTrace();
		} catch (ConnexionException e) {
			// NOTHING
		}
	}
}
