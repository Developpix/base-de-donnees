package ihm.controleur.transitions;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;

import ihm.modele.utilisateur.Compte;
import ihm.vue.VueSaisieRelation;

/**
 * Classe permettant de créer le controleur redirigeant vers la vue de saisie
 * des données
 * 
 * @author PICHON Thibaut
 * @version 1.0
 */
public class ControleurAllerVueSaisie implements ActionListener {

	private Compte modele;
	private JFrame vue;

	/**
	 * Constructeur du controleur
	 * 
	 * @param modele le modèle
	 * @param vue    la vue en cours
	 */
	public ControleurAllerVueSaisie(Compte modele, JFrame vue) {
		this.modele = modele;
		this.vue = vue;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		this.vue.setVisible(false);
		this.vue.dispose();

		new VueSaisieRelation(this.modele);
	}
}
