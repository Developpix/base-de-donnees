package ihm.controleur.transitions;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;

import ihm.modele.utilisateur.Compte;
import ihm.vue.VueUllman;

/**
 * Classe permettant de créer un controleur permettant de rejoindre la vue pour
 * le théorème d'Ullman
 * 
 * @author PICHON Thibaut
 * @version 0.1
 */
public class ControleurAllerVueUllman implements ActionListener {

	private Compte modele;
	private JFrame vue;

	/**
	 * Constructeur de la classe
	 * 
	 * @param modele le modèle
	 * @param vue    la vue
	 */
	public ControleurAllerVueUllman(Compte modele, JFrame vue) {
		this.modele = modele;
		this.vue = vue;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		this.vue.setVisible(false);
		this.vue.dispose();

		new VueUllman(this.modele);
	}
}
