package ihm.controleur.gestionCompte;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

import com.google.common.hash.Hashing;
import com.google.gson.JsonParseException;

import ihm.modele.database.dao.DAOCompte;
import ihm.modele.exceptions.ConnexionException;
import ihm.modele.utilisateur.Compte;
import ihm.vue.VueGestionCompte;

/**
 * Controleur pour se connecter à l'application
 * 
 * @author Mathis Jobard
 * @version 0.1
 */
public class ControleurModifPassword implements ActionListener {

	private VueGestionCompte vue;
	private Compte modele;

	/**
	 * Constructeur de la classe
	 * 
	 * @param vue    la vue
	 * @param compte le compte de l'utilisateur (modèle)
	 */
	public ControleurModifPassword(VueGestionCompte vue, Compte compte) {
		this.vue = vue;
		this.modele = compte;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		if (!this.vue.donneAncienMdp().isEmpty() && !this.vue.donneNouveaumdp().isEmpty()
				&& !this.vue.donneNouveaumdp2().isEmpty()) {
			if (Hashing.sha512().hashString(this.vue.donneAncienMdp(), StandardCharsets.UTF_8).toString()
					.equals(this.modele.getMotDePasse())) {
				if (this.vue.donneNouveaumdp().equals(this.vue.donneNouveaumdp2())) {
					this.modele.setMotDePasse(this.vue.donneNouveaumdp());

					try {
						DAOCompte daocompte = new DAOCompte();
						daocompte.miseAJour(modele);
						this.vue.afficherMessageInformatif("Info", "Mise à jour effectuée");
					} catch (JsonParseException | IOException e) {
						e.printStackTrace();
					} catch (ConnexionException e) {
						this.vue.afficherMessageErreur("Problème de connexion",
								"Impossible de se connecter à la base de données");
					}

				} else {
					this.vue.afficherMessageErreur("Erreur", "Problème avec la saisie du nouveau mot de passe");
				}
			} else {
				this.vue.afficherMessageErreur("Erreur", "L'ancien mot de passe n'est pas correct");
			}
		} else {
			this.vue.afficherMessageErreur("Erreur", "Champs vides");
		}
	}
}