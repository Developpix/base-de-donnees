package ihm.controleur.gestionCompte;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import com.google.gson.JsonParseException;

import ihm.modele.database.dao.DAOTravail;
import ihm.modele.exceptions.ConnexionException;
import ihm.modele.utilisateur.Compte;
import ihm.vue.VueGestionCompte;

/**
 * Classe permettant de créer un controleur permettant de gérer la selection
 * d'un exercice
 * 
 * @author Thibaut PICHON
 * @version 0.1
 */
public class ControleurAffichageExercice implements ActionListener {

	private VueGestionCompte vue;
	private Compte modele;

	/**
	 * Constructeur de la classe
	 * 
	 * @param modele   le modèle
	 * @param vue la vue
	 */
	public ControleurAffichageExercice(VueGestionCompte vue, Compte modele) {
		this.modele = modele;
		this.vue = vue;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		int idTravail = this.vue.donneChoixExercice();

		try {
			DAOTravail daoTravail = new DAOTravail();

			this.vue.miseAJourAffichageExo(this.modele.getExosRealises().get(idTravail),
					daoTravail.donnerRelationTaches(idTravail), daoTravail.donnerStatsTaches(idTravail));
		} catch (JsonParseException e) {
			// NOTHING
		} catch (IOException e) {
			// NOTHING
		} catch (IllegalArgumentException e) {
			// NOTHING
		} catch (ConnexionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
