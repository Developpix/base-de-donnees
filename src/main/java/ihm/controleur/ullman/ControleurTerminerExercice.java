package ihm.controleur.ullman;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import com.google.gson.JsonParseException;

import ihm.modele.database.dao.DAOExercice;
import ihm.modele.database.dao.DAOTravail;
import ihm.modele.exceptions.ConnexionException;
import ihm.modele.utilisateur.Compte;
import ihm.modele.utilisateur.Exercice;
import ihm.vue.VueBienvenue;
import ihm.vue.VueUllman;

/**
 * Classe permettant de créer un controleur permettant de gérer la fin d'un
 * exercice par l'utilisateur
 * 
 * @author Thibaut PICHON
 * @version 0.1
 */
public class ControleurTerminerExercice implements ActionListener {

	private Compte modele;
	private VueUllman vue;

	/**
	 * Constructeur de la classe
	 * 
	 * @param vueUllman la vue
	 * @param modele    le modèle
	 */
	public ControleurTerminerExercice(VueUllman vueUllman, Compte modele) {
		this.modele = modele;
		this.vue = vueUllman;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		Exercice exo = this.modele.getExerciceEnCours();

		try {
			// Si l'exercice ne provient pas de la base de données on le sauvegarde dedans
			if (exo.getID() == -1) {
				DAOExercice daoExo = new DAOExercice();
				daoExo.creer(exo);
			}

			DAOTravail daoTravail = new DAOTravail();
			daoTravail.creer(this.modele, exo);

			this.vue.setVisible(false);
			this.vue.dispose();
			
			this.modele.setExosRealises(daoTravail.donnerExosCompte(this.modele));
			new VueBienvenue(this.modele);
		} catch (JsonParseException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ConnexionException e) {
			this.vue.afficherMessage("Problème de connexion", "Impossible de se connecter à la base de données");
		}
	}
}
