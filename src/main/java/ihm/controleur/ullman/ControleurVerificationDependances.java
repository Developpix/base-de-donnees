package ihm.controleur.ullman;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Set;

import ihm.modele.bd.Attribut;
import ihm.modele.bd.Graphe;
import ihm.modele.bd.Relation;
import ihm.modele.exceptions.AttributNonPresentException;
import ihm.modele.exceptions.EnsembleDependancesInvalideException;
import ihm.modele.utilisateur.Compte;
import ihm.modele.utilisateur.Etape;
import ihm.modele.utilisateur.Parseur;
import ihm.vue.VueUllman;

/**
 * Classe permettant de créer un controleur gérant la vérification des attributs
 * saisies
 * 
 * @author Jobard Mathis
 * @version 0.1
 */
public class ControleurVerificationDependances implements ActionListener {

	private Compte modele;
	private VueUllman vue;

	/**
	 * Constructeur de la classe
	 * 
	 * @param vue    la vue
	 * @param modele le modèle
	 */
	public ControleurVerificationDependances(VueUllman vue, Compte modele) {
		this.modele = modele;
		this.vue = vue;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {

		String attsSaisisNettoyes = Parseur.nettoyerNomAttribut(this.vue.donneAttributs());
		String attsSaisisNettoyesR1 = Parseur.nettoyerNomAttribut(this.vue.donneAttributsR1());
		String attsSaisisNettoyesR2 = Parseur.nettoyerNomAttribut(this.vue.donneAttributsR2());

		if (attsSaisisNettoyes.charAt(0) != '<' || attsSaisisNettoyesR1.charAt(0) != '<'
				|| attsSaisisNettoyesR2.charAt(0) != '<') {
			this.vue.afficherMessage("Ensemble d'attributs saisi invalide",
					"L'ensemble d'attributs saisi ne commence pas par <");
		} else if (attsSaisisNettoyes.charAt(attsSaisisNettoyes.length() - 1) != '>'
				|| attsSaisisNettoyesR1.charAt(attsSaisisNettoyesR1.length() - 1) != '>'
				|| attsSaisisNettoyesR2.charAt(attsSaisisNettoyesR2.length() - 1) != '>') {
			this.vue.afficherMessage("Ensemble d'attributs saisi invalide",
					"L'ensemble d'attributs saisi ne se termine pas par >");
		} else if (attsSaisisNettoyes.length() < 3 || attsSaisisNettoyesR1.length() < 3
				|| attsSaisisNettoyesR2.length() < 3) {
			this.vue.afficherMessage("Ensemble d'attributs vide",
					"Des attributs sont nécessaires pour pouvoir travailler");
		} else {

			try {

				Set<Attribut> atts = Parseur.convertirSetAttributs(attsSaisisNettoyes);
				Set<Attribut> attsR1 = Parseur.convertirSetAttributs(attsSaisisNettoyesR1);
				Set<Attribut> attsR2 = Parseur.convertirSetAttributs(attsSaisisNettoyesR2);

				Graphe gInitial = Parseur.convertirEnGraphe(atts, this.vue.donneEnsembleDependances());

				Relation r = new Relation(gInitial, atts);

				Graphe gR1 = Parseur.convertirEnGraphe(attsR1, this.vue.donneEnsembleDependancesR1());

				Relation r1 = new Relation(gR1, attsR1);

				Graphe gR2 = Parseur.convertirEnGraphe(attsR2, this.vue.donneEnsembleDependancesR2());

				Relation r2 = new Relation(gR2, attsR2);

				ArrayList<Relation> listerelations = new ArrayList<>(2);
				listerelations.add(r1);
				listerelations.add(r2);

				int i = this.modele.donnerEtatEtape(Etape.PertesDependances);

				if ((r.verifPasPerteDependances(listerelations) && this.vue.choixDependances() == "Pas Pertes")
						|| (!r.verifPasPerteDependances(listerelations) && vue.choixDependances() == "Pertes")) {
					this.modele.definirEtatEtape(Etape.PertesDependances, i);
					this.vue.validerSaisieDependances();
				} else {
					this.modele.definirEtatEtape(Etape.PertesDependances, i + 1);
					this.vue.validerSaisieDependances(
							(r.verifPasPerteDependances(listerelations) ? "Pas de pertes de dépendances"
									: "Pertes de dépendances"));
				}

			} catch (AttributNonPresentException e) {
				this.vue.afficherMessage("Rajouter le(s) attribut(s) manquants",
						"Ensemble de dépendances contient des attributs non disponibles");
			} catch (EnsembleDependancesInvalideException e) {
				this.vue.afficherMessage("Ensemble de dépendances saisie invalide", e.getMessage());
			}
		}
	}
}
