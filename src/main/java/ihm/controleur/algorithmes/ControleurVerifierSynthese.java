package ihm.controleur.algorithmes;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import ihm.modele.bd.Relation;
import ihm.modele.utilisateur.Compte;
import ihm.modele.utilisateur.Etape;
import ihm.modele.utilisateur.Exercice;
import ihm.vue.VueAlgorithmes;

/**
 * Classe permettant de créer un controleur permettant de vérifier le résultat
 * de l'algorithme de synthèse saisi par l'utilisateur
 * 
 * @author Thibaut PICHON
 * @version 0.1
 */
public class ControleurVerifierSynthese implements ActionListener {

	private Compte modele;
	private VueAlgorithmes vue;

	/**
	 * Constructeur de la classe
	 * 
	 * @param modele le modèle
	 * @param vue    la vue
	 */
	public ControleurVerifierSynthese(Compte modele, VueAlgorithmes vue) {
		this.modele = modele;
		this.vue = vue;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		// On stocke l'exercice en cours dans une variable
		Exercice exoEnCours = this.modele.getExerciceEnCours();

		int i = this.modele.donnerEtatEtape(Etape.Synthese);

		if (exoEnCours.getRelation().algorithmeSynthese(exoEnCours.getCle()).size() == exoEnCours.getDecompoSynthese()
				.values().size()) {
			boolean correct = true;

			for (Relation r : exoEnCours.getRelation().algorithmeSynthese(exoEnCours.getCle())) {
				if (!exoEnCours.getDecompoSynthese().containsValue(r))
					correct = false;

			}

			if (correct) {
				this.modele.definirEtatEtape(Etape.Synthese, i);
				this.vue.validerSynthese();
			} else {
				if (i < 2)
					this.vue.validerSynthese("La décomposition avec l'algorithme de Synthèse est invalide (" + (2 - i)
							+ " tentatives restantes)");
				else
					this.vue.validerSynthese(exoEnCours.getRelation().algorithmeSynthese(exoEnCours.getCle()).toString()
							.replace("}, ", "}\n").replace("[", "").replace("]", ""));

				if (i < 3)
					this.modele.definirEtatEtape(Etape.Synthese, i + 1);
			}
		} else {
			if (i < 2)
				this.vue.validerSynthese("La décomposition avec l'algorithme de Synthèse est invalide (" + (2 - i)
						+ " tentatives restantes)");
			else
				this.vue.validerSynthese(exoEnCours.getRelation().algorithmeSynthese(exoEnCours.getCle()).toString()
						.replace("}, ", "}\n").replace("[", "").replace("]", ""));

			if (i < 3)
				this.modele.definirEtatEtape(Etape.Synthese, i + 1);
		}

		this.vue.miseAJourProgression();
	}
}
