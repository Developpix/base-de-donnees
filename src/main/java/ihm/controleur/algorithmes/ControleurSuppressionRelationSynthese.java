package ihm.controleur.algorithmes;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import ihm.modele.utilisateur.Compte;
import ihm.vue.VueAlgorithmes;

/**
 * Classe permettant de créer un controleur gérant la suppression d'une relation
 * dans la décomposition suivant l'algo de décomposition
 * 
 * @author PICHON Thibaut
 * @version 0.1
 *
 */
public class ControleurSuppressionRelationSynthese implements ActionListener {

	private Compte modele;
	private VueAlgorithmes vue;

	/**
	 * Constructeur de la classe
	 * 
	 * @param modele le modèle
	 * @param vue    la vue
	 */
	public ControleurSuppressionRelationSynthese(Compte modele, VueAlgorithmes vue) {
		this.modele = modele;
		this.vue = vue;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		if (this.vue.donneRelationSyntheseSelectionne() != null) {
			this.modele.getExerciceEnCours().getDecompoSynthese()
					.remove(this.vue.donneRelationSyntheseSelectionne().split(" ")[0]);
			this.vue.miseAJourListeSynthese();
		}
	}
}
