package ihm.controleur.algorithmes;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import ihm.modele.bd.Attribut;
import ihm.modele.bd.Graphe;
import ihm.modele.bd.Relation;
import ihm.modele.exceptions.AttributNonPresentException;
import ihm.modele.exceptions.EnsembleAttributsVideException;
import ihm.modele.exceptions.EnsembleDependancesInvalideException;
import ihm.modele.utilisateur.Compte;
import ihm.modele.utilisateur.Etape;
import ihm.modele.utilisateur.Exercice;
import ihm.modele.utilisateur.Parseur;
import ihm.vue.VueAlgorithmes;

/**
 * Controleur pour vérifier la décomposition en suivant l'algo de décomposition
 * 
 * @author Thibaut PICHON
 * @version 0.1
 *
 */
public class ControleurVerificationDecompo implements ActionListener {

	private Compte modele;
	private VueAlgorithmes vue;

	/**
	 * Constructeur de la classe
	 * 
	 * @param modele le modèle
	 * @param vue    la vue
	 */
	public ControleurVerificationDecompo(Compte modele, VueAlgorithmes vue) {
		this.modele = modele;
		this.vue = vue;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		// On stocke l'exercice en cours dans une variable
		Exercice exoEnCours = this.modele.getExerciceEnCours();

		try {
			List<Set<Attribut>> parties = Parseur.parserDependance(this.vue.donnerDependanceDecompoSelectionne());
			List<Set<Attribut>> attsAttendus = new ArrayList<>(2);
			attsAttendus.add(new TreeSet<>());
			attsAttendus.add(new TreeSet<>());

			attsAttendus.get(0).addAll(parties.get(0));
			attsAttendus.get(0).addAll(parties.get(1));

			attsAttendus.get(1).addAll(exoEnCours.getRelationRestante().getAttributs());
			attsAttendus.get(1).removeAll(parties.get(1));

			String attsR1 = this.vue.donnerAttributsR1(), attsR2 = this.vue.donnerAttributsR2();
			String depsR1 = this.vue.donnerDependancesR1(), depsR2 = this.vue.donnerDependancesR2();

			////////// Vérification que les saisies sont valides //////////
			if (attsR1.charAt(0) != '<') {
				this.vue.afficherMessage("Ensemble d'attributs de R1 saisi invalide",
						"L'ensemble d'attributs saisi ne commence pas par <");
			} else if (attsR1.charAt(attsR1.length() - 1) != '>') {
				this.vue.afficherMessage("Ensemble d'attributs de R1 saisi invalide",
						"L'ensemble d'attributs saisi ne se termine pas par >");
			} else if (attsR2.charAt(0) != '<') {
				this.vue.afficherMessage("Ensemble d'attributs de R2 saisi invalide",
						"L'ensemble d'attributs saisi ne commence pas par <");
			} else if (attsR2.charAt(attsR2.length() - 1) != '>') {
				this.vue.afficherMessage("Ensemble d'attributs de R2 saisi invalide",
						"L'ensemble d'attributs saisi ne se termine pas par >");
			}
			if (depsR1.charAt(0) != '{') {
				this.vue.afficherMessage("Ensemble de dépendances de R1 saisi invalide",
						"L'ensemble de dépendances saisi ne commence pas par {");
			} else if (depsR1.charAt(depsR1.length() - 1) != '}') {
				this.vue.afficherMessage("Ensemble de dépendances de R1 saisi invalide",
						"L'ensemble de dépendances saisi ne se termine pas par {");
			} else if (depsR2.charAt(0) != '{') {
				this.vue.afficherMessage("Ensemble de dépendances de R2 saisi invalide",
						"L'ensemble de dépendances saisi ne commence pas par {");
			} else if (depsR2.charAt(depsR2.length() - 1) != '}') {
				this.vue.afficherMessage("Ensemble de dépendances de R2 saisi invalide",
						"L'ensemble de dépendances saisi ne se termine pas par }");
			} else if (exoEnCours.getRelation().fermetureEnsemble(parties.get(0))
					.containsAll(exoEnCours.getRelation().getAttributs())) {
				// Si X de la dépendance X --> Y, contient une clé de R
				boolean resteDeps = false;

				// On parcours les partie gauche des dépendances
				for (Set<Attribut> partieGauche : exoEnCours.getRelationRestante().getGraphe().donneCles()) {
					// Si la partie gauche ne contient pas une clé de R alors il reste une
					// dépendance utilisable
					if (!exoEnCours.getRelation().fermetureEnsemble(partieGauche)
							.containsAll(exoEnCours.getRelation().getAttributs())) {
						System.out.println(partieGauche);
						resteDeps = true;
						break;
					}
				}

				// Il ne reste aucune dépendance utilisable
				if (!resteDeps) {
					this.vue.miseAJourValidationDecompo(exoEnCours.getRelationRestante().toString());
					this.modele.definirEtatEtape(Etape.Decomposition, this.modele.donnerEtatEtape(Etape.Decomposition) + 1);
					exoEnCours.getDecompoDecomposition().add(exoEnCours.getRelationRestante());
					exoEnCours.setRelationRestante(null);
				} else {
					this.vue.afficherMessage("Dépendance X --> Y invalide",
							"X contient une clé de la relation initiale");
				}

				this.vue.miseAJourListeRelationsDecompo();
			} else {
				Set<Attribut> attributsR1 = Parseur.convertirSetAttributs(Parseur.nettoyerNomAttribut(attsR1));
				Set<Attribut> attributsR2 = Parseur.convertirSetAttributs(Parseur.nettoyerNomAttribut(attsR2));

				// Si l'utilisateur a saisi les bons attributs pour R1 et r2
				if (attributsR1.equals(attsAttendus.get(0)) && attributsR2.equals(attsAttendus.get(1))) {
					try {
						Graphe attendu1, attendu2, saisie1, saisie2;
						attendu1 = exoEnCours.getRelation().getGraphe().grapheInduit(attributsR1);
						attendu2 = exoEnCours.getRelation().getGraphe().grapheInduit(attributsR2);

						saisie1 = Parseur.convertirEnGraphe(attributsR1, this.vue.donnerDependancesR1());
						saisie2 = Parseur.convertirEnGraphe(attributsR2, this.vue.donnerDependancesR2());

						if (attendu1.equals(saisie1) && attendu2.equals(saisie2)) {
							// Si l'ensemble de dépendance de R1 et R2 est valide
							exoEnCours.getDecompoDecomposition().add(new Relation(attendu1, attributsR1));

							exoEnCours.setRelationRestante(new Relation(attendu2, attributsR2));

							this.vue.miseAJourValidationDecompo();
							this.modele.definirEtatEtape(Etape.Decomposition, 0);
						} else {
							// Sinon si les dépendances de R1 et/ou R2 sont incorrects
							Relation r1 = new Relation(attendu1, attsAttendus.get(0));
							Relation r2 = new Relation(attendu2, attsAttendus.get(1));

							exoEnCours.getDecompoDecomposition().add(r1);
							exoEnCours.setRelationRestante(r2);

							this.vue.miseAJourValidationDecompo(r1.toString() + "\n\n" + r2.toString());
							this.modele.definirEtatEtape(Etape.Decomposition, this.modele.donnerEtatEtape(Etape.Decomposition) + 1);
						}
					} catch (IllegalArgumentException | EnsembleAttributsVideException
							| AttributNonPresentException e) {
						/* NOTHING */ }
				} else {
					// Si il n'a pas saisi les bons attributs pour R1 et/ou R2
					try {
						Relation r1 = new Relation(
								exoEnCours.getRelationRestante().getGraphe().grapheInduit(attsAttendus.get(0)),
								attsAttendus.get(0));

						exoEnCours.getDecompoDecomposition().add(r1);
						Relation r2 = new Relation(
								exoEnCours.getRelationRestante().getGraphe().grapheInduit(attsAttendus.get(1)),
								attsAttendus.get(1));

						exoEnCours.setRelationRestante(r2);

						this.vue.miseAJourValidationDecompo(r1.toString() + "\n\n" + r2.toString());
						this.modele.definirEtatEtape(Etape.Decomposition, this.modele.donnerEtatEtape(Etape.Decomposition) + 1);
					} catch (IllegalArgumentException | EnsembleAttributsVideException
							| AttributNonPresentException e) {
						/* NOTHING */ }
				}

				boolean resteDeps = false;

				// On parcours les partie gauche des dépendances
				for (Set<Attribut> partieGauche : exoEnCours.getRelationRestante().getGraphe().donneCles()) {
					// Si la partie gauche ne contient pas une clé de R alors il reste une
					// dépendance utilisable
					if (!exoEnCours.getRelation().fermetureEnsemble(partieGauche)
							.containsAll(exoEnCours.getRelation().getAttributs())) {
						System.out.println(partieGauche);
						resteDeps = true;
						break;
					}
				}

				// Il ne reste aucune dépendance utilisable
				if (!resteDeps) {
					exoEnCours.getDecompoDecomposition().add(exoEnCours.getRelationRestante());
					exoEnCours.setRelationRestante(null);
				}

				this.vue.miseAJourListeRelationsDecompo();
			}
		} catch (EnsembleDependancesInvalideException e) {
			/* NOTHING */ }
	}
}
