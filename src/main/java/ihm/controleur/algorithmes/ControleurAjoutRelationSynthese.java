package ihm.controleur.algorithmes;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Set;

import ihm.modele.bd.Attribut;
import ihm.modele.bd.Graphe;
import ihm.modele.bd.Relation;
import ihm.modele.exceptions.AttributNonPresentException;
import ihm.modele.exceptions.EnsembleDependancesInvalideException;
import ihm.modele.utilisateur.Compte;
import ihm.modele.utilisateur.Parseur;
import ihm.vue.VueAlgorithmes;

/**
 * Classe permettant de créer un controleur gérant l'ajout d'une relation dans
 * la décomposition suivant l'algorithme de synthèse
 * 
 * @author PICHON Thibaut
 * @version 0.1
 *
 */
public class ControleurAjoutRelationSynthese implements ActionListener {

	private Compte modele;
	private VueAlgorithmes vue;

	/**
	 * Constructeur de la classe
	 * 
	 * @param modele le modèle
	 * @param vue    la vue
	 */
	public ControleurAjoutRelationSynthese(Compte modele, VueAlgorithmes vue) {
		this.modele = modele;
		this.vue = vue;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		String attsSaisisNetoyes = Parseur.nettoyerNomAttribut(this.vue.donneAttributsSynthese());

		if (attsSaisisNetoyes.charAt(0) != '<') {
			this.vue.afficherMessage("Ensemble d'attributs saisi invalide",
					"L'ensemble d'attributs saisi ne commence pas par <");
		} else if (attsSaisisNetoyes.charAt(attsSaisisNetoyes.length() - 1) != '>') {
			this.vue.afficherMessage("Ensemble d'attributs saisi invalide",
					"L'ensemble d'attributs saisi ne se termine pas par >");
		} else if (attsSaisisNetoyes.length() < 3) {
			this.vue.afficherMessage("Ensemble d'attributs vide",
					"Des attributs sont nécessaires pour pouvoir travailler");
		} else if (this.vue.donneNomRelationSynthese().trim().isEmpty()) {
			this.vue.afficherMessage("Nom de la relation incorrect",
					"Le nom de la relation est vide, veuillez saisir un nom");
		} else {
			try {

				Set<Attribut> atts = Parseur.convertirSetAttributs(attsSaisisNetoyes);

				Graphe g = Parseur.convertirEnGraphe(atts, this.vue.donneDependancesSynthese());

				this.modele.getExerciceEnCours().getDecompoSynthese().put(this.vue.donneNomRelationSynthese(),
						new Relation(g, atts));

				this.vue.miseAJourListeSynthese();
			} catch (AttributNonPresentException e) {
				this.vue.afficherMessage("Rajouter le(s) attribut(s) manquants",
						"Ensemble de dépendances contient des attributs non disponibles");
			} catch (EnsembleDependancesInvalideException e) {
				this.vue.afficherMessage("Ensemble de dépendances saisie invalide", e.getMessage());
			}
		}
	}
}
