package ihm.controleur.saisies;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Set;

import ihm.modele.bd.Attribut;
import ihm.modele.bd.Graphe;
import ihm.modele.bd.Relation;
import ihm.modele.exceptions.AttributNonPresentException;
import ihm.modele.exceptions.EnsembleDependancesInvalideException;
import ihm.modele.utilisateur.Compte;
import ihm.modele.utilisateur.Etape;
import ihm.modele.utilisateur.Exercice;
import ihm.modele.utilisateur.Parseur;
import ihm.vue.VueRecherche;
import ihm.vue.VueSaisieRelation;

/**
 * Classe permettant de créer le controleur redirigeant vers la vue des
 * recherches préliminaires
 * 
 * @author PICHON Thibaut
 * @version 1.0
 */
public class ControleurCreerRelation implements ActionListener {

	private Compte modele;
	private VueSaisieRelation vue;

	/**
	 * Constructeur du controleur
	 * 
	 * @param modele le modèle
	 * @param vue    la vue en cours
	 */
	public ControleurCreerRelation(Compte modele, VueSaisieRelation vue) {
		this.modele = modele;
		this.vue = vue;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		String attsSaisisNetoyes = Parseur.nettoyerNomAttribut(this.vue.donneAttributs());

		// On stocke l'exercice dans une variable
		Exercice exoEnCours = this.modele.getExerciceEnCours();

		int i = this.modele.donnerEtatEtape(Etape.Relation);

		if (attsSaisisNetoyes.charAt(0) != '<') {
			this.vue.afficherMessage("Ensemble d'attributs saisi invalide",
					"L'ensemble d'attributs saisi ne commence pas par <");
		} else if (attsSaisisNetoyes.charAt(attsSaisisNetoyes.length() - 1) != '>') {
			this.vue.afficherMessage("Ensemble d'attributs saisi invalide",
					"L'ensemble d'attributs saisi ne se termine pas par >");
		} else if (attsSaisisNetoyes.length() < 3) {
			this.vue.afficherMessage("Ensemble d'attributs vide",
					"Des attributs sont nécessaires pour pouvoir travailler");
		} else {
			try {

				Set<Attribut> atts = Parseur.convertirSetAttributs(attsSaisisNetoyes);

				Graphe g = Parseur.convertirEnGraphe(atts, this.vue.donneEnsembleDependances());

				if (exoEnCours.getTuples() != null) {
					if (!exoEnCours.getTuples().getAttributs().equals(atts)) {
						if (i < 2)
							this.vue.afficherMessage("Relation invalide (attributs)",
									"Les attributs de la relation saisie ne correspondent pas à ceux des tuples affichés ("
											+ (2 - i) + " tentatives restantes)");
						else
							this.vue.definirRelation(exoEnCours.getTuples().getRelation());

						if (i < 3)
							this.modele.definirEtatEtape(Etape.Relation, i + 1);
						return;
					}

					Graphe g2 = exoEnCours.getTuples().getRelation().getGraphe();

					for (Set<Attribut> sg : g.donneCles()) {
						if (!g2.donneCles().contains(sg) || !g2.successeurs(sg).containsAll(g.successeurs(sg))) {
							if (i < 2)
								this.vue.afficherMessage("Relation invalide (dépendances)",
										"La relation saisie ne correspond pas aux tuples affichés (" + (2 - i)
												+ " tentatives restantes)");
							else
								this.vue.definirRelation(exoEnCours.getTuples().getRelation());

							if (i < 3)
								this.modele.definirEtatEtape(Etape.Relation, i + 1);
							return;
						}
					}
				}

				exoEnCours.setRelation(new Relation(g, atts));

				this.vue.setVisible(false);
				this.vue.dispose();

				this.modele.definirEtatEtape(Etape.Relation, i);

				new VueRecherche(this.modele);
			} catch (AttributNonPresentException e) {
				this.modele.definirEtatEtape(Etape.Relation, this.modele.donnerEtatEtape(Etape.Relation) + 1);
				this.vue.afficherMessage("Rajouter le(s) attribut(s) manquants",
						"Ensemble de dépendances contient des attributs non disponibles");
			} catch (EnsembleDependancesInvalideException e) {
				this.modele.definirEtatEtape(Etape.Relation, this.modele.donnerEtatEtape(Etape.Relation) + 1);
				this.vue.afficherMessage("Ensemble de dépendances saisie invalide", e.getMessage());
			}
		}

	}
}
