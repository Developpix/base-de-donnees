package ihm.controleur.saisies;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import ihm.modele.utilisateur.Compte;
import ihm.vue.VueSaisieTuples;

/**
 * Classe permettant de créer un controleur permettant de gérer l'ajout d'un
 * tuple dans la table des tuples
 * 
 * @author Thibaut PICHON
 * @version 0.1
 */
public class ControleurAjoutTuple implements ActionListener {

	private Compte modele;
	private VueSaisieTuples vue;

	/**
	 * Constructeur de la classe
	 * 
	 * @param vueSaisieTuples la vue
	 * @param modele          le modèle
	 */
	public ControleurAjoutTuple(VueSaisieTuples vueSaisieTuples, Compte modele) {
		this.modele = modele;
		this.vue = vueSaisieTuples;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		this.modele.getExerciceEnCours().getTuples().ajouterTuple(this.vue.donneTuple());

		this.vue.miseAJourGestionAttributs();
		this.vue.miseAJourGestionTuples();

		this.vue.activerSuppressionTuples(true);
	}
}
