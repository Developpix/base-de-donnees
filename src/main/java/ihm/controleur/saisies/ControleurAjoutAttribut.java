package ihm.controleur.saisies;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import ihm.modele.bd.Attribut;
import ihm.modele.exceptions.NomVideException;
import ihm.modele.utilisateur.Compte;
import ihm.modele.utilisateur.Exercice;
import ihm.vue.VueSaisieTuples;

/**
 * Classe permettant de créer un controleur permettant de gérer l'ajout d'un
 * attribut dans la table des tuples
 * 
 * @author Thibaut PICHON
 * @version 0.1
 */
public class ControleurAjoutAttribut implements ActionListener {

	private Compte modele;
	private VueSaisieTuples vue;

	/**
	 * Constructeur de la classe
	 * 
	 * @param vueSaisieTuples la vue
	 * @param modele          le modèle
	 */
	public ControleurAjoutAttribut(VueSaisieTuples vueSaisieTuples, Compte modele) {
		this.modele = modele;
		this.vue = vueSaisieTuples;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		// On stocke l'exercice dans une variable
		Exercice exoEnCours = this.modele.getExerciceEnCours();
		
		try {
			exoEnCours.getTuples().ajouterAttribut(new Attribut(this.vue.donneNomAttribut()));

			this.vue.miseAJourGestionAttributs();
			this.vue.miseAJourGestionTuples();
			this.vue.activerSuivant(true);
		} catch (NomVideException e) {
			this.vue.afficherMessage("Nom attribut vide", "Le nom de l'attribut à ajouter ne doit pas être vide");
		}
	}
}
