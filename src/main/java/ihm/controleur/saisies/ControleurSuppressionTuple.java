package ihm.controleur.saisies;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import ihm.modele.utilisateur.Compte;
import ihm.modele.utilisateur.Exercice;
import ihm.vue.VueSaisieTuples;

/**
 * Classe permettant de créer un controleur permettant de gérer la suppression
 * d'un tuple dans la table
 * 
 * @author Thibaut PICHON
 * @version 0.1
 */
public class ControleurSuppressionTuple implements ActionListener {

	private Compte modele;
	private VueSaisieTuples vue;

	/**
	 * Constructeur de la classe
	 * 
	 * @param vueSaisieTuples la vue
	 * @param modele          le modèle
	 */
	public ControleurSuppressionTuple(VueSaisieTuples vueSaisieTuples, Compte modele) {
		this.modele = modele;
		this.vue = vueSaisieTuples;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		int[] tuples = this.vue.donnerTuplesSelectionnes();

		// On stocke l'exercice dans une variable
		Exercice exoEnCours = this.modele.getExerciceEnCours();

		for (int i = 0; i < tuples.length; i++) {
			exoEnCours.getTuples().supprimerTuple(tuples[i] - i);
		}

		if (exoEnCours.getTuples().getRowCount() < 1)
			this.vue.activerSuppressionTuples(false);
	}
}
