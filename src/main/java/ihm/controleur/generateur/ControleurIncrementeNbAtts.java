package ihm.controleur.generateur;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import ihm.vue.VueGenerateur;

/**
 * Classe permettant de créer un controleur permettant de gérer l'incrémentation
 * du nombre d'attributs pour le générateur
 * 
 * @author Thibaut PICHON
 * @version 0.1
 */
public class ControleurIncrementeNbAtts implements ActionListener {

	private VueGenerateur vue;

	/**
	 * Constructeur de la classe
	 * 
	 * @param vue la vue
	 */
	public ControleurIncrementeNbAtts(VueGenerateur vue) {
		this.vue = vue;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		if (this.vue.donneNbAtts() < 15)
			this.vue.setNbAtts(this.vue.donneNbAtts() + 1);
	}
}
