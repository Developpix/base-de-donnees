package ihm.controleur.generateur;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import ihm.vue.VueGenerateur;

/**
 * Classe permettant de créer un controleur gérer la decrémentation du nombre de
 * dépendances ou de tuples pour le générateur
 * 
 * @author Thibaut PICHON
 * @version 0.1
 */
public class ControleurDecrementeNbDepsTuples implements ActionListener {

	private VueGenerateur vue;
	private boolean genererTuples;

	/**
	 * Constructeur de la classe
	 * 
	 * @param vueGenerateur la vue
	 * @param genererTuples un bouléen pour informer si il s'agit d'une génération
	 *                      de tuples ou de relation
	 */
	public ControleurDecrementeNbDepsTuples(VueGenerateur vueGenerateur, boolean genererTuples) {
		this.vue = vueGenerateur;
		this.genererTuples = genererTuples;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		if ((this.vue.donneNbDepsTuples() > 0 && !this.genererTuples) || this.genererTuples)
			this.vue.setNbDepsTuples(this.vue.donneNbDepsTuples() - 1);

	}
}
