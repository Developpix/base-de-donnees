package ihm.controleur.generateur;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.TimeUnit;

import ihm.modele.bd.Relation;
import ihm.modele.bd.Table;
import ihm.modele.utilisateur.Compte;
import ihm.modele.utilisateur.GenerateurExercice;
import ihm.vue.VueGenerateur;
import ihm.vue.VueRecherche;
import ihm.vue.VueSaisieRelation;

/**
 * Classe permettant de créer un constructeur permettant de gérer le lancement
 * du générateur
 * 
 * @author Thibaut PICHON
 * @version 0.1
 */
public class ControleurGenerer implements ActionListener {

	private Compte modele;
	private VueGenerateur vue;
	private boolean genererTuples;
	protected Relation r;

	/**
	 * Constructeur de la classe
	 * 
	 * @param vueGenerateurTuples la vue
	 * @param modele              le modèle
	 * @param genererTuples       un bouléen pour informer si il faut générer des
	 *                            tuples ou une relation
	 */
	public ControleurGenerer(VueGenerateur vueGenerateurTuples, Compte modele, boolean genererTuples) {
		this.modele = modele;
		this.vue = vueGenerateurTuples;
		this.genererTuples = genererTuples;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		this.vue.setVisible(false);
		this.vue.dispose();

		if (this.genererTuples) {
			Table tuples = GenerateurExercice.genererTuples(this.vue.donneNbAtts(), this.vue.donneNbDepsTuples());
			this.modele.getExerciceEnCours().setTuples(tuples);
			new VueSaisieRelation(this.modele);
		} else {
			// Tant qu'on a pas de relation on execute la génération avec un timeout de 10	ms
			while (r == null) {
				// Création d'un service d'exécution de processus avec fork
				ExecutorService executor = new ForkJoinPool();
				// Execution du générateur dans un processus enfant
				executor.execute(new Runnable() {
					@Override
					public void run() {
						ControleurGenerer.this.r = GenerateurExercice.genererRelation(
								ControleurGenerer.this.vue.donneNbAtts(), ControleurGenerer.this.vue.donneNbDepsTuples());
					}
				});

				try {
					// Attente de la fin du processus ou d'un timeout de 10ms
					executor.awaitTermination(10, TimeUnit.MILLISECONDS);
				} catch (InterruptedException e1) {}
			}
			
			// On démarre la vue de recherche
			this.modele.getExerciceEnCours().setRelation(this.r);
			new VueRecherche(this.modele);
		}
	}
}
