package ihm.controleur.generateur;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import ihm.modele.utilisateur.GenerateurExercice;
import ihm.vue.VueGenerateur;

/**
 * Classe permettant de créer un controleur gérant l'incrémentation du nombre de
 * dépendances ou de tuples pour le générateur
 * 
 * @author Thibaut PICHON
 * @version 0.1
 */
public class ControleurIncrementeNbDepsTuples implements ActionListener {

	private VueGenerateur vue;
	private boolean genererTuples;

	/**
	 * Constructeur de la classe
	 * 
	 * @param vueGenerateur la vue
	 * @param genererTuples un booléen indiquant si on génére des tuples ou non (dépendances)
	 */
	public ControleurIncrementeNbDepsTuples(VueGenerateur vueGenerateur, boolean genererTuples) {
		this.vue = vueGenerateur;
		this.genererTuples = genererTuples;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		if ((this.vue.donneNbDepsTuples() < Math
				.min(GenerateurExercice.nombreDependancesPossibles(this.vue.donneNbAtts()), 20) && !this.genererTuples)
				|| (this.vue.donneNbDepsTuples() < 20 && this.genererTuples))
			this.vue.setNbDepsTuples(this.vue.donneNbDepsTuples() + 1);
	}
}
