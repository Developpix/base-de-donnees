package ihm.controleur.generateur;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import ihm.modele.utilisateur.GenerateurExercice;
import ihm.vue.VueGenerateur;

/**
 * Classe permettant de créer un controleur permettant de gérer la
 * decrémentation du nombre d'attributs pour le générateur
 * 
 * @author Thibaut PICHON
 * @version 0.1
 */
public class ControleurDecrementeNbAtts implements ActionListener {

	private VueGenerateur vue;

	/**
	 * Constructeur de la classe
	 * 
	 * @param vueGenerateur la vue
	 */
	public ControleurDecrementeNbAtts(VueGenerateur vueGenerateur) {
		this.vue = vueGenerateur;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		if (this.vue.donneNbAtts() > 1) {
			this.vue.setNbAtts(this.vue.donneNbAtts() - 1);
			this.vue.setNbDepsTuples((int) Math.min(GenerateurExercice.nombreDependancesPossibles(this.vue.donneNbAtts()),
					this.vue.donneNbDepsTuples()));
		}
	}
}
