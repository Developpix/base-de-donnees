#!/usr/bin/env bash

apt-get update
apt-get install -y wget

mkdir .public
cd .public

echo "Récupération ancien exécutables"
wget "https://developpix.gitlab.io/base-de-donnees/base-de-donnees.jar"
wget "https://developpix.gitlab.io/base-de-donnees/base-de-donnees-all.zip"

if [[ $1 =~ '[noBuild]' ]]
then
    echo "Récupération ancien badge de la compilation"
    wget "https://developpix.gitlab.io/base-de-donnees/build.svg"
fi

if [[ $1 =~ '[noTests]' ]] || [[ $1 =~ '[noTestAttribut]' ]]
then
    echo "Récupération ancien badge des tests de Attribut"
    wget "https://developpix.gitlab.io/base-de-donnees/testsAttribut.svg"
fi

if [[ $1 =~ '[noTests]' ]] || [[ $1 =~ '[noTestGraphe]' ]]
then
    echo "Récupération ancien badge des tests de Graphe"
    wget "https://developpix.gitlab.io/base-de-donnees/testsGraphe.svg"
fi

if [[ $1 =~ '[noTests]' ]] || [[ $1 =~ '[noTestRelation]' ]]
then
    echo "Récupération ancien badge des tests de Relation"
    wget "https://developpix.gitlab.io/base-de-donnees/testsRelation.svg"
fi

if [[ $1 =~ '[noTests]' ]] || [[ $1 =~ '[noTestTable]' ]]
then
    echo "Récupération ancien badge des tests de Table"
    wget "https://developpix.gitlab.io/base-de-donnees/testsTable.svg"
fi

if [[ $1 =~ '[noTests]' ]] || [[ $1 =~ '[noTestCompte]' ]]
then
    echo "Récupération ancien badge des tests de Compte"
    wget "https://developpix.gitlab.io/base-de-donnees/testsCompte.svg"
fi

if [[ $1 =~ '[noTests]' ]] || [[ $1 =~ '[noTestGenerateurExos]' ]]
then
    echo "Récupération ancien badge des tests du générateur d'exercices"
    wget "https://developpix.gitlab.io/base-de-donnees/testsGenerateurExos.svg"
fi

if [[ $1 =~ '[noTests]' ]] || [[ $1 =~ '[noTestConfigLoader]' ]]
then
    echo "Récupération ancien badge des tests du chargeur de configuration"
    wget "https://developpix.gitlab.io/base-de-donnees/testsConfigLoader.svg"
fi

echo "Préparation terminé"
