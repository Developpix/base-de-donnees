# Les commits

## Structure des messages
```
<type>(<portée>): <sujet>
<LIGNE VIDE>
<corps>
<LIGNE VIDE>
<pied de page>
```

## Type
Doit-être un (ou plusieurs séprarer par des virgules) des éléments de la liste ci-dessous:
* **build**: Modifications affectant le système de compilation et/ou les dépendances
du projet
* **ci**: Modifications de l'intégration continue
* **docs**: Modifications de la documentation seulement
* **feat**: Nouvelle fonctionnalité
* **fix**: Correction de bug
* **perf**: Modifications permettant de modifier les performances
* **refactor**: Modifications du code ne donnant pas lieu à une nouveauté
ou à une correction de bug
* **style**: Modifications esthétique du code ne modifiant par l'algorithme ou son
implémentation (espace, formattage, point-virgule, etc)
* **test**: Modifications des tests

## Portée
Doit-être un des éléments de la liste ci-dessous:
* **IHM** : Modifications affectant l'IHM
* **MODELE** : Modifications affectant le modèle
* **VIEW** : Modifications affectant les vues
* **CONTROLLER** : Modifications affectant les controlleurs
* **OUTILS** : Modifications en lien avec les outils utilisés (git, GitLab,
gestion du CI de GitLab en lui-même)

## Sujet
Brève description des modifications apporté. <br/>
Conseil :
* Ecrivez vos phrase à l'impératif
* Ne commencez pas par une lettre majuscule
* Ne finissez pas par un "."

Aucune idée de sujet c'est pas grave :
* Commencez par un verbe/nom décrivant votre action sur le projet (Implémentation, Correction, Ajout, Suppression, etc)
* Quoi ? Expliquer en quelques mots qu'est qui a été ajouter, supprimer,
corriger, etc
* Voilà, vous avez fini ;-)

## Corps
Décrivez en détail les modifications si nécessaire.
Vous pouvez commencer par des majuscules et finir par un point,
impératif conseiller mais pas obligatoire.

## Pied de page
Utilisez le pied de page pour les filtrages des commits style, fermer une
discussion, corriger un problème ("fix an issue").

### Drapeaux pour le pipeline
Les drapeaux sont mis dans le pied de page.
Liste des drapeaux:
* **[noBuild]** Ne pas lancer de build
* **[noTests]** Ne pas lancer les tests
* **[noTestAttribut]** Ne pas lancer les tests sur l'attribut
* **[noTestGraphe]** Ne pas lancer les tests du graphe
* **[noTestRelation]** Ne pas lancer les tests sur la relation
* **[noTestTable]** Ne pas lancer les tests sur la table
* **[noTestCompte]** Ne pas lancer les tests sur le comtpe
* **[noTestConfigLoader]** Ne pas lancer les test le chargeur de configuration de la base de données
* **[noTestGenerateurExos]** Ne pas lancer les test du générateur d'exos